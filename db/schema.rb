# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180302141814) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "admins", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.index ["email"], name: "index_admins_on_email", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_admins_on_reset_password_token", unique: true, using: :btree
  end

  create_table "calendar_advertiser_events", force: :cascade do |t|
    t.integer "advertiser_id"
    t.string  "name"
    t.date    "start_date"
    t.date    "end_date"
    t.string  "fr_name"
    t.string  "color"
    t.index ["advertiser_id"], name: "index_calendar_advertiser_events_on_advertiser_id", using: :btree
  end

  create_table "calendar_advertisers", force: :cascade do |t|
    t.string  "cover_advert"
    t.string  "left_advert_1"
    t.string  "left_advert_2"
    t.string  "left_advert_3"
    t.string  "right_advert"
    t.string  "name"
    t.integer "grid"
    t.string  "website"
    t.string  "phone"
    t.string  "fr_cover_advert"
    t.string  "fr_left_advert_1"
    t.string  "fr_left_advert_2"
    t.string  "fr_left_advert_3"
    t.boolean "hide_on_map",       default: false
    t.string  "logo"
    t.boolean "tiny_map",          default: false
    t.boolean "tiny_address",      default: false
    t.string  "tiny_address_logo"
  end

  create_table "calendar_category_filters", force: :cascade do |t|
    t.integer "category"
    t.text    "words",    default: [], array: true
  end

  create_table "calendar_holidays", force: :cascade do |t|
    t.integer  "region_config_id"
    t.string   "name"
    t.date     "date"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.string   "fr_name"
    t.index ["region_config_id"], name: "index_calendar_holidays_on_region_config_id", using: :btree
  end

  create_table "calendar_locations", force: :cascade do |t|
    t.string  "name"
    t.string  "street_address"
    t.float   "latitude"
    t.float   "longitude"
    t.string  "postal_code"
    t.string  "province"
    t.string  "city"
    t.string  "phone"
    t.string  "website"
    t.integer "advertiser_id"
    t.string  "opening_hours_first_line"
    t.string  "opening_hours_second_line"
    t.string  "opening_hours_third_line"
    t.boolean "tiny_map",                     default: false
    t.integer "category",                     default: 0
    t.integer "region_config_id"
    t.string  "fr_opening_hours_first_line"
    t.string  "fr_opening_hours_second_line"
    t.string  "fr_opening_hours_third_line"
    t.string  "fr_opening_hours_fourth_line"
    t.string  "opening_hours_fourth_line"
    t.index ["advertiser_id"], name: "index_calendar_locations_on_advertiser_id", using: :btree
  end

  create_table "calendar_months", force: :cascade do |t|
    t.integer "region_config_id"
    t.integer "month_number"
    t.integer "advertiser_id"
    t.index ["advertiser_id"], name: "index_calendar_months_on_advertiser_id", using: :btree
    t.index ["region_config_id"], name: "index_calendar_months_on_region_config_id", using: :btree
  end

  create_table "calendar_postal_code_covers", force: :cascade do |t|
    t.integer "region_config_id"
    t.string  "front"
    t.string  "fr_front"
    t.string  "codes",            default: [], array: true
    t.string  "name"
    t.index ["region_config_id"], name: "index_calendar_postal_code_covers_on_region_config_id", using: :btree
  end

  create_table "calendar_region_configs", force: :cascade do |t|
    t.string  "region"
    t.string  "front"
    t.string  "signature"
    t.string  "front_left"
    t.string  "last_page"
    t.string  "calendar"
    t.string  "codes",            default: [], array: true
    t.integer "status",           default: 4
    t.string  "waste_pickup_url"
    t.string  "anniversary"
    t.string  "stamp"
    t.string  "fr_front"
    t.string  "fr_front_left"
    t.string  "fr_last_page"
    t.string  "fr_anniversary"
    t.string  "neighbourhood"
    t.string  "fr_neighbourhood"
    t.string  "fr_stamp"
    t.index ["codes"], name: "index_calendar_region_configs_on_codes", using: :btree
  end

  create_table "calendar_waste_pickups", force: :cascade do |t|
    t.datetime "date"
    t.boolean  "green_bin",              default: false
    t.boolean  "garbage",                default: false
    t.boolean  "recycling",              default: false
    t.boolean  "yard_waste",             default: false
    t.boolean  "christmas_tree",         default: false
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.integer  "waste_disposal_zone_id"
  end

  create_table "event_store_events", force: :cascade do |t|
    t.string   "stream",     null: false
    t.string   "event_type", null: false
    t.string   "event_id",   null: false
    t.text     "metadata"
    t.text     "data",       null: false
    t.datetime "created_at", null: false
    t.index ["created_at"], name: "index_event_store_events_on_created_at", using: :btree
    t.index ["event_id"], name: "index_event_store_events_on_event_id", unique: true, using: :btree
    t.index ["event_type"], name: "index_event_store_events_on_event_type", using: :btree
    t.index ["stream"], name: "index_event_store_events_on_stream", using: :btree
  end

  create_table "invitations", force: :cascade do |t|
    t.string   "email",      null: false
    t.string   "token",      null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["token"], name: "index_invitations_on_token", using: :btree
  end

  create_table "report_movers", force: :cascade do |t|
    t.string   "file_name"
    t.integer  "parsed_count",             null: false
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.integer  "status",       default: 0
    t.string   "zip_path"
    t.string   "batch_id"
  end

  create_table "report_movers_calendars", force: :cascade do |t|
    t.integer  "report_movers_id"
    t.string   "path"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "number"
    t.index ["report_movers_id"], name: "index_report_movers_calendars_on_report_movers_id", using: :btree
  end

  create_table "report_movers_errors", force: :cascade do |t|
    t.integer  "report_movers_id"
    t.string   "row"
    t.string   "message"
    t.integer  "error_type"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["report_movers_id"], name: "index_report_movers_errors_on_report_movers_id", using: :btree
  end

  create_table "waste_disposal_zones", force: :cascade do |t|
    t.json     "coordinates"
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.integer  "region_config_id"
    t.string   "name"
    t.string   "description"
    t.string   "waste_types",      default: [],              array: true
    t.string   "related_to"
    t.date     "date_from"
    t.date     "date_to"
  end

end
