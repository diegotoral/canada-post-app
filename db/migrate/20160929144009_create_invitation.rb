class CreateInvitation < ActiveRecord::Migration[5.0]
  def change
    create_table :invitations do |t|
      t.string :email, null: false
      t.string :token, null: false
      t.index :token

      t.timestamps null: false
    end
  end
end
