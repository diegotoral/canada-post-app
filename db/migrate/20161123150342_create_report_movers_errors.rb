class CreateReportMoversErrors < ActiveRecord::Migration[5.0]
  def change
    create_table :report_movers_errors do |t|
      t.belongs_to :report_movers
      t.string :row
      t.string :message
      t.integer :error_type
    end
  end
end
