class CreateCalendarCategoryFilters < ActiveRecord::Migration[5.0]
  def change
    create_table :calendar_category_filters do |t|
      t.integer :category
      t.text :words, array: true, default: []
    end
  end
end
