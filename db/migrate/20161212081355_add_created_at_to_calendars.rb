class AddCreatedAtToCalendars < ActiveRecord::Migration[5.0]
  def change
    add_column :report_movers_calendars, :created_at, :datetime
    add_column :report_movers_calendars, :updated_at, :datetime
  end
end
