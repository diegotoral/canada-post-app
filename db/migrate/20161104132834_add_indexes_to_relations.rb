class AddIndexesToRelations < ActiveRecord::Migration[5.0]
  def change
    add_index :report_example_calendars, :report_id
    add_index :calendar_months, :region_config_id
    add_index :calendar_months, :advertiser_id
    add_index :calendar_advertiser_events, :advertiser_id
  end
end
