class AddFrStampToRegionConfigs < ActiveRecord::Migration[5.0]
  def change
    add_column :calendar_region_configs, :fr_stamp, :string
  end
end
