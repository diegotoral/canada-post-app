class AddHideOnMapFieldToAdvertiser < ActiveRecord::Migration[5.0]
  def change
    add_column :calendar_advertisers, :hide_on_map, :boolean, default: false
  end
end
