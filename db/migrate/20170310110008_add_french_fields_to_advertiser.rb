class AddFrenchFieldsToAdvertiser < ActiveRecord::Migration[5.0]
  def change
    add_column :calendar_advertisers, :fr_cover_advert, :string
    add_column :calendar_advertisers, :fr_left_advert_1, :string
    add_column :calendar_advertisers, :fr_left_advert_2, :string
    add_column :calendar_advertisers, :fr_left_advert_3, :string
  end
end
