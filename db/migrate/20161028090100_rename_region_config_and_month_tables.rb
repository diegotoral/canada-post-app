class RenameRegionConfigAndMonthTables < ActiveRecord::Migration[5.0]
  def change
    rename_table :region_configs, :calendar_region_configs
    rename_table :month_configs, :calendar_months
  end
end
