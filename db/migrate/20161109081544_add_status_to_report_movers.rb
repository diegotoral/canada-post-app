class AddStatusToReportMovers < ActiveRecord::Migration[5.0]
  def change
    add_column :report_movers, :status, :integer, default: 0
  end
end
