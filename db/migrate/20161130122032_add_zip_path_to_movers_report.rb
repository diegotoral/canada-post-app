class AddZipPathToMoversReport < ActiveRecord::Migration[5.0]
  def change
    add_column :report_movers, :zip_path, :string
  end
end
