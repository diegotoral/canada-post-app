class AddFrenchOpeningHoursToLocation < ActiveRecord::Migration[5.0]
  def change
    add_column :calendar_locations, :fr_opening_hours_mon_fri, :string
    add_column :calendar_locations, :fr_opening_hours_sun, :string
    add_column :calendar_locations, :fr_opening_hours_sat, :string
  end
end
