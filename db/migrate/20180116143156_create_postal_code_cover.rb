class CreatePostalCodeCover < ActiveRecord::Migration[5.0]
  def change
    create_table :calendar_postal_code_covers do |t|
      t.belongs_to :region_config
      t.string :front
      t.string :fr_front
      t.string :codes, array: true, default: []
      t.string :name
    end
  end
end
