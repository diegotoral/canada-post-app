class CreateCalendarAdvertiserEvents < ActiveRecord::Migration[5.0]
  def change
    create_table :calendar_advertiser_events do |t|
      t.integer :advertiser_id
      t.string  :name
      t.date    :start_date
      t.date    :end_date
    end
  end
end
