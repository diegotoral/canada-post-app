class CreateCalendarAdvertiser < ActiveRecord::Migration[5.0]
  def change
    create_table :calendar_advertisers do |t|
      t.string :cover_advert
      t.string :left_advert_1
      t.string :left_advert_2
      t.string :left_advert_3
      t.string :right_advert
      t.string :name
      t.integer :grid
    end
  end
end
