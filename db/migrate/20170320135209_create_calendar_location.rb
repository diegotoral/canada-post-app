class CreateCalendarLocation < ActiveRecord::Migration[5.0]
  def change
    create_table :calendar_locations do |t|
      t.string :name
      t.string :street_address
      t.float :latitude
      t.float :longitude
      t.string :postal_code
      t.string :province
      t.string :city
      t.string :phone
      t.string :website

      t.integer :advertiser_id
    end
    add_index :calendar_locations, :advertiser_id
  end
end
