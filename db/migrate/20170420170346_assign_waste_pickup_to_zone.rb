class AssignWastePickupToZone < ActiveRecord::Migration[5.0]
  def change
    add_column :calendar_waste_pickups, :waste_disposal_zone_id, :integer
    remove_column :calendar_waste_pickups, :region_config_id, :integer
  end
end
