class AddNameDescriptionAndRegionIdToWasteDisposalZone < ActiveRecord::Migration[5.0]
  def change
    add_column :waste_disposal_zones, :region_config_id, :integer
    add_column :waste_disposal_zones, :name, :string
    add_column :waste_disposal_zones, :description, :string
  end
end
