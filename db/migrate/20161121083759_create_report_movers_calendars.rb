class CreateReportMoversCalendars < ActiveRecord::Migration[5.0]
  def change
    create_table :report_movers_calendars do |t|
      t.belongs_to :report_movers
      t.string :path
    end
  end
end
