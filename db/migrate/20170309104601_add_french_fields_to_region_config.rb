class AddFrenchFieldsToRegionConfig < ActiveRecord::Migration[5.0]
  def change
    add_column :calendar_region_configs, :fr_front, :string
    add_column :calendar_region_configs, :fr_front_left, :string
    add_column :calendar_region_configs, :fr_last_page, :string
    add_column :calendar_region_configs, :fr_anniversary, :string

    add_column :calendar_region_configs, :neighbourhood, :string
    add_column :calendar_region_configs, :fr_neighbourhood, :string
  end
end
