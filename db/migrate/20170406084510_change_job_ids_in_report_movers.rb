class ChangeJobIdsInReportMovers < ActiveRecord::Migration[5.0]
  def change
    remove_column :report_movers, :job_ids, :string, array: true
    add_column :report_movers, :batch_id, :string
  end
end
