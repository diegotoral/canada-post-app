class CreateCalendarHolidays < ActiveRecord::Migration[5.0]
  def change
    create_table :calendar_holidays do |t|
      t.belongs_to :region_config
      t.string :name
      t.date :date
      t.timestamps
    end
  end
end
