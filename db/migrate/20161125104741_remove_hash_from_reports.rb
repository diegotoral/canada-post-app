class RemoveHashFromReports < ActiveRecord::Migration[5.0]
  def change
    remove_column :report_movers, :errors_hash, :json, null: false, default: {}
  end
end
