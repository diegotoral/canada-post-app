class AddColorToEvents < ActiveRecord::Migration[5.0]
  def change
    add_column :calendar_advertiser_events, :color, :string
  end
end
