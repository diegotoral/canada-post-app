class AddCalendarsPathsToReport < ActiveRecord::Migration[5.0]
  def change
    add_column :report_movers, :calendars, :string, array: true, default: []
  end
end
