class AddWasteTypesRelations < ActiveRecord::Migration[5.0]
  def change
    add_column :waste_disposal_zones, :related_to, :string
    add_column :waste_disposal_zones, :date_from, :date
    add_column :waste_disposal_zones, :date_to, :date
  end
end
