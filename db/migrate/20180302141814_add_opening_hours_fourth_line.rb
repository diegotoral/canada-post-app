class AddOpeningHoursFourthLine < ActiveRecord::Migration[5.0]
  def change
    add_column :calendar_locations, :fr_opening_hours_fourth_line, :string
    add_column :calendar_locations, :opening_hours_fourth_line, :string
  end
end
