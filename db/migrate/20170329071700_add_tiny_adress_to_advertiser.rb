class AddTinyAdressToAdvertiser < ActiveRecord::Migration[5.0]
  def change
    add_column :calendar_advertisers, :tiny_address, :boolean, default: false
    add_column :calendar_advertisers, :tiny_address_logo, :string
  end
end
