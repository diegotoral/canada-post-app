class RemoveCalendarsArrayFromReport < ActiveRecord::Migration[5.0]
  def change
    remove_column :report_movers, :calendars, :string, array: true
  end
end
