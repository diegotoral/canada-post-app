class AddAdvertiserToMonths < ActiveRecord::Migration[5.0]
  def change
    add_column :calendar_months, :advertiser_id, :integer
  end
end
