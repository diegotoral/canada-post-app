class AddWastePickupLinkToRegion < ActiveRecord::Migration[5.0]
  def change
    add_column :calendar_region_configs, :waste_pickup_url, :string
  end
end
