class AddStampToRegionConfig < ActiveRecord::Migration[5.0]
  def change
    add_column :calendar_region_configs, :stamp, :string
  end
end
