class CreateMonthConfigs < ActiveRecord::Migration[5.0]
  def change
    create_table :month_configs do |t|
      t.integer :region_config_id
      t.integer :month_number
      t.string  :cover_advert
      t.string  :small_advert
    end
  end
end
