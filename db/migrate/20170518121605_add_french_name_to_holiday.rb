class AddFrenchNameToHoliday < ActiveRecord::Migration[5.0]
  def change
    add_column :calendar_holidays, :fr_name, :string
  end
end
