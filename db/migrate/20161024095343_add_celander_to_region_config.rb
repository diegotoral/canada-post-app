class AddCelanderToRegionConfig < ActiveRecord::Migration[5.0]
  def change
    add_column :region_configs, :calendar, :string
  end
end
