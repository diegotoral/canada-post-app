class CreateCalendarWastePickup < ActiveRecord::Migration[5.0]
  def change
    create_table :calendar_waste_pickups do |t|
      t.belongs_to :region_config
      t.datetime :date
      t.boolean :green_bin, default: false
      t.boolean :garbage, default: false
      t.boolean :recycling, default: false
      t.boolean :yard_waste, default: false
      t.boolean :christmas_tree, default: false

      t.timestamps null: false
    end
  end
end
