class AddIndexOnRegionCodes < ActiveRecord::Migration[5.0]
  def change
    add_index :calendar_region_configs, :codes
  end
end
