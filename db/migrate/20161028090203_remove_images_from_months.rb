class RemoveImagesFromMonths < ActiveRecord::Migration[5.0]
  def change
    remove_column :calendar_months, :cover_advert, :string
    remove_column :calendar_months, :small_advert, :string
  end
end
