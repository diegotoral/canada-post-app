class AddTimeStmapToReportMoversError < ActiveRecord::Migration[5.0]
  def change
    add_column :report_movers_errors, :created_at, :datetime
    add_column :report_movers_errors, :updated_at, :datetime
  end
end
