class AddTinyMapFlagToLocation < ActiveRecord::Migration[5.0]
  def change
    add_column :calendar_locations, :tiny_map, :boolean, default: false
  end
end
