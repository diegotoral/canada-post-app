class ChangeOpeningHoursInLocation < ActiveRecord::Migration[5.0]
  def change
    rename_column :calendar_locations, :opening_hours_mon_sat, :opening_hours_mon_fri
    add_column :calendar_locations, :opening_hours_sat, :string
  end
end
