class AddTinyMapFields < ActiveRecord::Migration[5.0]
  def change
    add_column :calendar_advertisers, :logo, :string
    add_column :calendar_advertisers, :tiny_map, :boolean, default: false

    add_column :calendar_locations, :opening_hours_mon_sat, :string
    add_column :calendar_locations, :opening_hours_sun, :string
  end
end
