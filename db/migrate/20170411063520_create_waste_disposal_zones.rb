class CreateWasteDisposalZones < ActiveRecord::Migration[5.0]
  def change
    create_table :waste_disposal_zones do |t|
      t.json :coordinates

      t.timestamps
    end
  end
end
