class AddStatusToRegionConfigs < ActiveRecord::Migration[5.0]
  def change
    add_column :calendar_region_configs, :status, :integer, default: 4
  end
end
