class AddNumberToCalendars < ActiveRecord::Migration[5.0]
  def change
    add_column :report_movers_calendars, :number, :integer
  end
end
