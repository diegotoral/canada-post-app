class AddJobIdsToReportMovers < ActiveRecord::Migration[5.0]
  def change
    add_column :report_movers, :job_ids, :string, array: true, default: []
  end
end
