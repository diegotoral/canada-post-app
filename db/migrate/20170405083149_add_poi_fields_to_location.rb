class AddPoiFieldsToLocation < ActiveRecord::Migration[5.0]
  def change
    add_column :calendar_locations, :category, :integer, default: 0
    add_column :calendar_locations, :region_config_id, :integer
  end
end
