class CreateRegionConfig < ActiveRecord::Migration[5.0]
  def change
    create_table :region_configs do |t|
      t.string :region
      t.string :front
      t.string :signature
      t.string :front_left
      t.string :last_page
    end
  end
end
