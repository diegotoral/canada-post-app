class ChangeOpeningHoursForLocation < ActiveRecord::Migration[5.0]
  def change
    rename_column :calendar_locations, :opening_hours_mon_fri, :opening_hours_first_line
    rename_column :calendar_locations, :opening_hours_sun, :opening_hours_second_line
    rename_column :calendar_locations, :opening_hours_sat, :opening_hours_third_line

    rename_column :calendar_locations, :fr_opening_hours_mon_fri, :fr_opening_hours_first_line
    rename_column :calendar_locations, :fr_opening_hours_sun, :fr_opening_hours_second_line
    rename_column :calendar_locations, :fr_opening_hours_sat, :fr_opening_hours_third_line
  end
end
