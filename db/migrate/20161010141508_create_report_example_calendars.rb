class CreateReportExampleCalendars < ActiveRecord::Migration[5.0]
  def change
    create_table :report_example_calendars do |t|
      t.integer :report_id
      t.string  :calendar
    end
  end
end
