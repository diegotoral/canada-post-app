class AddPhoneAndWebsiteToAdvertiser < ActiveRecord::Migration[5.0]
  def change
    add_column :calendar_advertisers, :website, :string
    add_column :calendar_advertisers, :phone, :string
  end
end
