class AddWasteTypeToZone < ActiveRecord::Migration[5.0]
  def change
    add_column :waste_disposal_zones, :waste_types, :string, array: true, default: []
  end
end
