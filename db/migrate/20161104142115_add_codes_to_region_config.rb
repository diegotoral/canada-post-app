class AddCodesToRegionConfig < ActiveRecord::Migration[5.0]
  def change
    add_column :calendar_region_configs, :codes, :string, array: true, default: []
  end
end
