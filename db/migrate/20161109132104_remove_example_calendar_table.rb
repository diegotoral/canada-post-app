class RemoveExampleCalendarTable < ActiveRecord::Migration[5.0]
  def self.up
    drop_table :report_example_calendars
  end

  def self.down
    create_table :report_example_calendars do |t|
      t.integer :report_id
      t.string  :calendar
    end
  end
end
