class CreateReportMovers < ActiveRecord::Migration[5.0]
  def change
    create_table :report_movers do |t|
      t.string :file_name
      t.integer :parsed_count, null: false
      t.json :errors_hash, null: false, default: {}

      t.timestamps
    end
  end
end
