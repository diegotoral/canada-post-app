Admin.create(password: 'password123', email: 'canada-post@binarapps.com')

regions = %w(Toronto)
Calendar::RegionConfig.where(region: regions).destroy_all
Calendar::Advertiser.destroy_all

front_img = Rack::Test::UploadedFile.new("#{Rails.root}/app/assets/images/calendar_2016/iStock_65568981_XLARGE_fmt.png")
signature_img = Rack::Test::UploadedFile.new("#{Rails.root}/app/assets/images/calendar_2016/Tinas_Signature_fmt.png")
front_left_img = Rack::Test::UploadedFile.new("#{Rails.root}/app/assets/images/calendar_2016/bg-homekopie_fmt.png")
last_page_img = Rack::Test::UploadedFile.new("#{Rails.root}/vendor/assets/images/calendar_2017/last_page.jpg")
small_advert_img = Rack::Test::UploadedFile.new("#{Rails.root}/app/assets/images/calendar_2016/indicia_fmt.png")

advertisers = [
  'Alive',
  'Desjardins',
  'Videotron',
  'Cancer Risk',
  'Jean Coutu',
  'Rona',
  'Metro',
  'Couche Tard',
  'Metro',
  'Metro',
  'Metro',
  'Metro',
]

regions.each do |region|
  codes = Rack::Test::UploadedFile.new("#{Rails.root}/vendor/region_codes/#{region.downcase}.txt")
  parsed_codes = Parser::RegionCodesService.new(codes).call
  created_region = Calendar::RegionConfig.create(region: region, front: front_img, signature: signature_img,
                                                front_left: front_left_img, last_page: last_page_img, codes: parsed_codes, waste_pickup_url: 'toronto.ca/wastewizard')
  advertisers.each_with_index do |advertiser, index|
    big = Rack::Test::UploadedFile.new("#{Rails.root}/vendor/assets/images/calendar_2017/#{advertiser.downcase.gsub(' ', '_')}_big.jpg")
    small = Rack::Test::UploadedFile.new("#{Rails.root}/vendor/assets/images/calendar_2017/#{advertiser.downcase.gsub(' ', '_')}_small.jpg")
    Calendar::Month.create(
      month_number: index+1,
      region_config: created_region,
      advertiser: Calendar::Advertiser.find_or_create_by(name: advertiser) do |advert|
        advert.cover_advert = big
        left_advert_1 = small
        grid = 0
        phone = Faker::PhoneNumber.cell_phone
        website = Faker::Internet.url
      end
    )
  end
end

Calendar::WasteDisposalZone::KMLImporter.new(Rails.root.join('spec', 'support', 'parser', 'map.kml.xml')).call
