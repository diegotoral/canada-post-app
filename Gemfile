source 'https://rubygems.org'

gem 'rails', '~> 5.0.0', '>= 5.0.0.1'
gem 'pg', '~> 0.18'
gem 'puma', '~> 3.0'

gem 'sass-rails', '~> 5.0'
gem 'uglifier', '>= 1.3.0'
gem 'jquery-rails'
gem 'haml', '~> 4.0', '>= 4.0.7'
gem 'i18n-js'

gem 'devise', '~> 4.2.0'
gem 'virtus', '~> 1.0.5'
gem 'carrierwave', '>= 1.0.0.beta', '< 2.0'
gem 'barby', '~> 0.6.5'
gem 'rails-i18n', '~> 5.0.0'
gem 'rchardet'

gem 'unicorn'
gem 'capistrano', '3.6.1'
gem 'capistrano-rails', '~> 1.1'
gem 'capistrano-bundler'
gem 'capistrano-rvm'

gem 'sidekiq'
gem 'sidekiq-batch'

gem 'redis-namespace'
gem 'foreman'
gem 'mini_magick'
gem 'non-stupid-digest-assets'
gem 'whenever', require: false
gem 'chartkick'
gem 'groupdate'
gem 'geocoder'
gem 'geokit'

gem 'rubyzip'
gem 'will_paginate', '~> 3.1.0'
gem 'will_paginate-bootstrap'
gem 'faker', '~> 1.6.6'

group :development, :test do
  gem 'bullet'
  gem 'pry', '~> 0.10.4'
  gem 'rspec-rails', '~> 3.5'
  gem 'factory_girl_rails', '~> 4.7.0'
end

group :development do
  gem 'web-console', '~> 3.3.1'
  gem 'listen', '~> 3.0.5'
  gem 'capistrano-sidekiq'
end

group :test do
  gem 'database_cleaner', '~> 1.5.3'
end
