class Calendar::Advertiser::Update
  include Virtus.model
  include ActiveModel::Validations

  attribute :id, Integer
  attribute :cover_advert, ActionDispatch::Http::UploadedFile
  attribute :left_advert_1, ActionDispatch::Http::UploadedFile
  attribute :left_advert_2, ActionDispatch::Http::UploadedFile
  attribute :left_advert_3, ActionDispatch::Http::UploadedFile
  attribute :right_advert, ActionDispatch::Http::UploadedFile
  attribute :fr_cover_advert, ActionDispatch::Http::UploadedFile
  attribute :fr_left_advert_1, ActionDispatch::Http::UploadedFile
  attribute :fr_left_advert_2, ActionDispatch::Http::UploadedFile
  attribute :fr_left_advert_3, ActionDispatch::Http::UploadedFile
  attribute :tiny_address_logo, ActionDispatch::Http::UploadedFile
  attribute :locations_file, ActionDispatch::Http::UploadedFile
  attribute :logo, ActionDispatch::Http::UploadedFile
  attribute :name, String
  attribute :grid, Integer
  attribute :remove_right_advert, Boolean
  attribute :phone, String
  attribute :hide_on_map, :Boolean
  attribute :tiny_map, Boolean
  attribute :tiny_address, Boolean

  ATTR_KEYS = %w(logo tiny_map tiny_address_logo tiny_address cover_advert left_advert_1 left_advert_2 left_advert_3 right_advert name grid website phone fr_cover_advert fr_left_advert_1 fr_left_advert_2 fr_left_advert_3 hide_on_map)

  validates :grid, :name, :cover_advert, presence: true
  validates :left_advert_1, presence: true, if: :left_advert_1_required?
  validates :left_advert_2, presence: true, if: :left_advert_2_required?
  validates :left_advert_3, presence: true, if: :left_advert_3_required?

  def initialize(attributes)
    super
    @advertiser = Calendar::Advertiser.find(id)
  end

  def call
    update_advertiser
    update_locations if locations_file.present?
    update_region_status
  end

  private

  def left_advert_1_required?
    return false if tiny_map
    !tiny_address
  end

  def left_advert_2_required?
    grid != Calendar::Advertiser.grids['whole_page'] unless tiny_map
  end

  def left_advert_3_required?
    grid == Calendar::Advertiser.grids['1:1:1'] unless tiny_map
  end

  def update_region_status
    @advertiser.region_configs.each do |region|
      Calendar::RegionConfigValidationStatusService.new(region).call
    end
  end

  def update_advertiser
    remove_right_advert_hash = remove_right_advert.present? ? { right_advert: nil } : {}
    @advertiser.update(sanitized_attributes.merge(remove_right_advert_hash))
  end

  def sanitized_attributes
    attributes.reject { |key, value| !ATTR_KEYS.include?(key.to_s) || value.nil? }
  end

  def update_locations
    Calendar::Location::DeleteFromAdvertiser.new(@advertiser).call
    Parser::CreateLocationsService.new(file: locations_file.tempfile, advertiser_id: @advertiser.id).call
  end
end
