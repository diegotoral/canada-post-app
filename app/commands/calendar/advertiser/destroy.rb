class Calendar::Advertiser::Destroy
  def initialize(id)
    @advertiser = Calendar::Advertiser.find(id)
  end

  def call
    raise Calendar::Advertiser::Destroy::AdvertiserIsConnectedToMonth if @advertiser.months.any?
    @advertiser.destroy
  end
end
