class Calendar::Location::DeleteFromRegion
  include Virtus.model
  include ActiveModel::Validations

  def initialize(region, category)
    @region = region
    validate_category!(category)
    @category = category
  end

  def call
    @region.locations.send(@category.to_sym).destroy_all
  end

  private

  def validate_category!(category)
    raise Calendar::Location::WrongCategory unless Calendar::Location.categories.keys.include?(category)
  end
end
