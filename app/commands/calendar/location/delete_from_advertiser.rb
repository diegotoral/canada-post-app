class Calendar::Location::DeleteFromAdvertiser
  include Virtus.model
  include ActiveModel::Validations

  def initialize(advertiser)
    @advertiser = advertiser
  end

  def call
    @advertiser.locations.destroy_all
  end
end
