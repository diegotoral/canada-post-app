class Calendar::Location::Create
  include Virtus.model
  include ActiveModel::Validations

  attribute :name, String
  attribute :street_address, String
  attribute :latitude, Float
  attribute :longitude, Float
  attribute :postal_code, String
  attribute :province, String
  attribute :city, String
  attribute :phone, String
  attribute :website, String
  attribute :tiny_map, Boolean
  attribute :opening_hours_first_line, String
  attribute :opening_hours_second_line, String
  attribute :opening_hours_third_line, String
  attribute :opening_hours_fourth_line, String
  attribute :fr_opening_hours_first_line, String
  attribute :fr_opening_hours_second_line, String
  attribute :fr_opening_hours_third_line, String
  attribute :fr_opening_hours_fourth_line, String
  attribute :advertiser_id, Integer
  attribute :region_config_id, Integer
  attribute :category, Integer

  def initialize(attributes:, advertiser_id: nil, region_id: nil, category: 0)
    super(attributes)
    @latitude = attributes['lat']
    @longitude = attributes['long']
    @advertiser_id = advertiser_id
    @region_config_id = region_id
    @category = category
    @tiny_map = attributes['tiny_map'] ? attributes['tiny_map'].downcase == "yes" : false
  end

  def call
    fetch_lat_long if @latitude.nil? || @longitude.nil?
    format_postal_code
    Calendar::Location.create(attributes)
  end

  private

  def format_postal_code
    @postal_code = Calendar::PostalCodeFormatService.new(@postal_code).call
  end

  def fetch_lat_long
    @latitude, @longitude = Calendar::Locations::GeocodeService.new("#{street_address} #{province} #{postal_code}").call
  end
end
