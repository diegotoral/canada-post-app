class Calendar::AdvertiserEvent::Update
  include Virtus.model
  include ActiveModel::Validations

  attribute :name, String
  attribute :fr_name, String
  attribute :color, String
  attribute :start_date, Date
  attribute :end_date, Date
  attribute :id, Integer

  ATTR_KEYS = %w(name start_date end_date fr_name color)

  validates :start_date, :end_date, :name, :color, presence: true

  def initialize(attributes)
    super
    @event = Calendar::AdvertiserEvent.find(id)
  end

  def call
    validate!
    update_event
    update_region_status
  end

  private

  def update_region_status
    @event.advertiser.region_configs.each do |region|
      Calendar::RegionConfigValidationStatusService.new(region).call
    end
  end

  def update_event
    @event.update(sanitized_attributes)
  end

  def sanitized_attributes
    attributes.reject { |key, value| !ATTR_KEYS.include?(key.to_s) || value.nil? }
  end
end
