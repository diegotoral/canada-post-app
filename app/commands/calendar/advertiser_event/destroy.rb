class Calendar::AdvertiserEvent::Destroy
  def initialize(id)
    @event = Calendar::AdvertiserEvent.find(id)
  end

  def call
    update_region_status
    @event.destroy
  end

  def update_region_status
    @event.advertiser.region_configs.each do |region|
      Calendar::RegionConfigValidationStatusService.new(region).call
    end
  end
end
