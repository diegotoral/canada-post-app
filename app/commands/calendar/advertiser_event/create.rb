class Calendar::AdvertiserEvent::Create
  include Virtus.model
  include ActiveModel::Validations

  attribute :name, String
  attribute :fr_name, String
  attribute :color, String
  attribute :start_date, Date
  attribute :end_date, Date
  attribute :advertiser_id, Integer

  validates :start_date, :end_date, :name, :advertiser_id, :color, presence: true

  def call
    validate!
    create_event
    @event.tap { update_region_status }
  end

  private

  def update_region_status
    Calendar::Advertiser.find(advertiser_id).region_configs.each do |region|
      Calendar::RegionConfigValidationStatusService.new(region).call
    end
  end

  def create_event
    @event = Calendar::AdvertiserEvent.create(attributes)
  end
end
