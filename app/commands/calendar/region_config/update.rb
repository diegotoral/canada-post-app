class Calendar::RegionConfig::Update
  include Virtus.model
  include ActiveModel::Validations

  attribute :id, Integer
  attribute :front, ActionDispatch::Http::UploadedFile
  attribute :signature, ActionDispatch::Http::UploadedFile
  attribute :front_left, ActionDispatch::Http::UploadedFile
  attribute :last_page, ActionDispatch::Http::UploadedFile
  attribute :anniversary, ActionDispatch::Http::UploadedFile
  attribute :codes_file, ActionDispatch::Http::UploadedFile
  attribute :fr_front_left, ActionDispatch::Http::UploadedFile
  attribute :fr_last_page, ActionDispatch::Http::UploadedFile
  attribute :fr_anniversary, ActionDispatch::Http::UploadedFile
  attribute :fr_front, ActionDispatch::Http::UploadedFile
  attribute :fr_front, ActionDispatch::Http::UploadedFile
  attribute :fr_neighbourhood, ActionDispatch::Http::UploadedFile
  attribute :neighbourhood, ActionDispatch::Http::UploadedFile
  attribute :stamp, ActionDispatch::Http::UploadedFile
  attribute :fr_stamp, ActionDispatch::Http::UploadedFile
  attribute :codes, Array
  attribute :waste_pickup_url, String
  attribute :calendar_months, Hash


  ATTR_KEYS = %w(front signature fr_stamp front_left last_page codes waste_pickup_url anniversary fr_front fr_front_left fr_last_page fr_anniversary stamp fr_neighbourhood neighbourhood)

  def initialize(attributes)
    super
    @region = Calendar::RegionConfig.find(id)
  end

  def call
    parse_codes
    update_region
    update_months
    Calendar::RegionConfigValidationStatusService.new(@region).call
  end

  private

  def update_months
    calendar_months.each do |id, params|
      Calendar::Month::Update.new(params.merge(id: id)).call
    end
  end

  def calendar_changed?
    front || signature || front_left || last_page
  end

  def update_region
    @region.update(sanitized_attributes)
  end

  def parse_codes
    @codes = codes_file.present? ? Parser::RegionCodesService.new(codes_file).call : nil
  end

  def sanitized_attributes
    attributes.reject { |key, value| !ATTR_KEYS.include?(key.to_s) || value.nil? }
  end
end
