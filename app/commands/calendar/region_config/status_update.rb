class Calendar::RegionConfig::StatusUpdate

  def initialize(region, status)
    @region = region
    @status = Calendar::RegionConfig.statuses[status]
  end

  def call
    update_status
  end

  private

  def update_status
    @region.update(status: @status)
  end
end
