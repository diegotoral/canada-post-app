class Calendar::RegionConfig::Create
  class RegionAlreadyExists < StandardError; end

  include Virtus.model
  include ActiveModel::Validations

  attribute :region, String
  attribute :front, ActionDispatch::Http::UploadedFile
  attribute :signature, ActionDispatch::Http::UploadedFile
  attribute :front_left, ActionDispatch::Http::UploadedFile
  attribute :last_page, ActionDispatch::Http::UploadedFile
  attribute :codes_file, ActionDispatch::Http::UploadedFile
  attribute :anniversary, ActionDispatch::Http::UploadedFile
  attribute :stamp, ActionDispatch::Http::UploadedFile
  attribute :fr_stamp, ActionDispatch::Http::UploadedFile
  attribute :fr_front_left, ActionDispatch::Http::UploadedFile
  attribute :fr_last_page, ActionDispatch::Http::UploadedFile
  attribute :fr_anniversary, ActionDispatch::Http::UploadedFile
  attribute :fr_front, ActionDispatch::Http::UploadedFile
  attribute :fr_neighbourhood, ActionDispatch::Http::UploadedFile
  attribute :neighbourhood, ActionDispatch::Http::UploadedFile
  attribute :stamp, ActionDispatch::Http::UploadedFile
  attribute :codes, Array
  attribute :waste_pickup_url, String

  validates :region, presence: true
  validate  :region, :region_uniqueness

  ATTR_KEYS = %w(region fr_stamp front signature front_left last_page codes waste_pickup_url anniversary fr_front fr_front_left fr_last_page fr_anniversary stamp fr_neighbourhood neighbourhood)

  def call
    validate!
    parse_codes
    create_region_with_months
  end

  private

  def parse_codes
    @codes = codes_file.present? ? Parser::RegionCodesService.new(codes_file).call : nil
  end

  def create_region_with_months
    ActiveRecord::Base.transaction do
      region_config = create_region
      create_months(region_config)
      Calendar::RegionConfigValidationStatusService.new(region_config).call
      region_config
    end
  end

  def create_region
    Calendar::RegionConfig.create(sanitized_attributes)
  end

  def create_months(region)
    (1..12).each { |month| Calendar::Month::Create.new(region_config_id: region.id, month_number: month).call }
  end

  def region_uniqueness
    raise RegionAlreadyExists if Calendar::RegionConfig.where(region: region).exists?
  end

  def sanitized_attributes
    attributes.reject { |key, value| !ATTR_KEYS.include?(key.to_s) || value.nil? }
  end
end
