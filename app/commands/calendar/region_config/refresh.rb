class Calendar::RegionConfig::Refresh

  def initialize(region, calendar)
    @calendar = calendar
    @region = region
  end

  def call
    update_region
  end

  private

  def update_region
    @region.update({calendar: @calendar, status: :complete})
  end
end
