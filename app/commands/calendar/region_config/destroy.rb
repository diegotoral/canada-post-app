class Calendar::RegionConfig::Destroy

  def initialize(region_id)
    @region = Calendar::RegionConfig.find(region_id)
  end

  def call
    ActiveRecord::Base.transaction do
      Calendar::Month::DestroyAll.new(@region.months).call
      Calendar::WastePickup::DestroyAll.new(@region.waste_pickups).call
      @region.destroy
    end
  end

  private

  def update_status
    @region.update(status: @status)
  end
end
