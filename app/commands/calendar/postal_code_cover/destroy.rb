class Calendar::PostalCodeCover::Destroy

  def initialize(id)
    @postal_code_cover = Calendar::PostalCodeCover.find(id)
  end

  def call
    @postal_code_cover.destroy
  end
end
