class Calendar::PostalCodeCover::Update
  include Virtus.model
  include ActiveModel::Validations

  attribute :name, String
  attribute :front, ActionDispatch::Http::UploadedFile
  attribute :fr_front, ActionDispatch::Http::UploadedFile
  attribute :codes_file, ActionDispatch::Http::UploadedFile
  attribute :codes, Array

  ATTR_KEYS = %w(name front fr_front codes region_config)

  def initialize(params, id)
    super(params)
    @postal_code_cover = Calendar::PostalCodeCover.find(id)
  end

  def call
    validate!
    parse_codes
    update_postal_code_cover
    @postal_code_cover
  end

  private

  def update_postal_code_cover
    @postal_code_cover.update(sanitized_attributes)
  end

  def parse_codes
    @codes = codes_file.present? ? Parser::RegionCodesService.new(codes_file).call : nil
  end

  def sanitized_attributes
    attributes.reject { |key, value| !ATTR_KEYS.include?(key.to_s) || value.nil? }
  end
end
