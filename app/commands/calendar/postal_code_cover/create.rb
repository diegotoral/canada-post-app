class Calendar::PostalCodeCover::Create
  include Virtus.model
  include ActiveModel::Validations

  attribute :name, String
  attribute :region_config_id, Integer
  attribute :front, ActionDispatch::Http::UploadedFile
  attribute :fr_front, ActionDispatch::Http::UploadedFile
  attribute :codes_file, ActionDispatch::Http::UploadedFile
  attribute :codes, Array
  attribute :region_config, Calendar::RegionConfig

  validates :region_config, presence: true

  ATTR_KEYS = %w(name front fr_front codes region_config)

  def call
    fetch_region_config
    validate!
    parse_codes
    create_postal_code_cover
  end

  private

  def fetch_region_config
    @region_config = Calendar::RegionConfig.find(region_config_id)
  end

  def create_postal_code_cover
    Calendar::PostalCodeCover.create(sanitized_attributes)
  end

  def parse_codes
    @codes = codes_file.present? ? Parser::RegionCodesService.new(codes_file).call : nil
  end

  def sanitized_attributes
    attributes.reject { |key, value| !ATTR_KEYS.include?(key.to_s) || value.nil? }
  end
end
