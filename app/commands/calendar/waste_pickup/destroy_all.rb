Calendar::WastePickup::DestroyAll = Struct.new(:waste_pickups) do
  def call
    waste_pickups.destroy_all
  end
end
