class Calendar::WastePickup::UpdateDayInfo
  include Virtus.model

  attribute :garbage, Boolean
  attribute :green_bin, Boolean
  attribute :recycling, Boolean
  attribute :christmas_tree, Boolean
  attribute :yard_waste, Boolean

  attribute :date, DateTime
  attribute :zone, Calendar::WasteDisposalZone

  ATTR_KEYS = %i(garbage green_bin recycling christmas_tree yard_waste date)

  def call
    if waste_pickup && any_waste_pickup?
      waste_pickup.update(sanitized_attributes)
    elsif waste_pickup
      waste_pickup.destroy
    elsif any_waste_pickup?
      zone.waste_pickups.create!(sanitized_attributes)
    end
  end

  private

  def sanitized_attributes
    attributes.slice(*ATTR_KEYS)
  end

  def any_waste_pickup?
    garbage || green_bin || recycling || yard_waste || christmas_tree
  end

  def waste_pickup
    zone.waste_pickups.find_date(date).first
  end
end
