class Calendar::CategoryFilter::Update
  include Virtus.model
  include ActiveModel::Validations

  attribute :id, Integer
  attribute :words, String

  def initialize(attributes)
    super
    @category_filter = Calendar::CategoryFilter.find(id)
  end

  def call
    update_filter
  end

  private

  def update_filter
    @category_filter.update(words: words.split(', '))
  end
end
