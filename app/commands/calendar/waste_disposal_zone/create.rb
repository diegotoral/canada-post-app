class Calendar::WasteDisposalZone::Create
  include Virtus.model

  WrongCoordinatesFormat = Class.new(Calendar::WasteDisposalZone::WasteDisposalError)
  WrongWasteTypes = Class.new(Calendar::WasteDisposalZone::WasteDisposalError)

  attribute :coordinates, Array, default: []
  attribute :name, String, default: ''
  attribute :description, String, default: ''
  attribute :region_id, Integer
  attribute :waste_types, Array

  def call
    split_waste_types
    Validator.new(self).call
    zone.tap do |record|
      record.update(description: description, coordinates: coordinates, waste_types: waste_types)
    end
  end

  def region
    @region ||= Calendar::RegionConfig.find(region_id)
  end

  def zone
    @zone ||= region.waste_disposal_zones.find_or_create_by(name: name)
  end

  Validator = Struct.new(:obj) do
    def call
      validate_coordinates_list
      validate_coordinates_numeric_values
      validate_coordinates_values_in_range
      validate_region_presence
      validate_waste_types_correctness
    end

    private

    def validate_waste_types_correctness
      raise_duplicated_error if obj.waste_types.count != obj.waste_types.uniq.count
      raise_wrong_types_error if (obj.waste_types - Calendar::WastePickup::WASTE_TYPES).count > 0
    end


    def raise_duplicated_error
      raise(Calendar::WasteDisposalZone::Create::WrongWasteTypes, 'waste types are duplicated')
    end

    def raise_wrong_types_error
      raise(Calendar::WasteDisposalZone::Create::WrongWasteTypes, "wrong waste types. Allowed: #{Calendar::WastePickup::WASTE_TYPES.to_sentence}")
    end


    def validate_coordinates_list
      return if obj.coordinates.is_a?(Enumerable) && obj.coordinates.all? do |list|
        list.is_a?(Enumerable) && list.all? { |e| e.is_a?(Enumerable) }
      end
      raise(Calendar::WasteDisposalZone::Create::WrongCoordinatesFormat, 'argument is not a proper list')
    end

    def validate_coordinates_numeric_values
      return if obj.coordinates.all? do |list|
        list.all? { |value| value.first.is_a?(Numeric) && value.last.is_a?(Numeric) }
      end
      raise(Calendar::WasteDisposalZone::Create::WrongCoordinatesFormat, 'values are not numbers')
    end

    def validate_coordinates_values_in_range
      return if obj.coordinates.all? do |list|
        list.all? { |e| e.first.between?(-180.0, 180.0) && e.last.between?(-90, 90) }
      end
      raise(Calendar::WasteDisposalZone::Create::WrongCoordinatesFormat, 'values out of range')
    end

    def validate_region_presence
      return if obj.region.present?
      raise(Calendar::WasteDisposalZone::Create::RegionNotFound, obj.region_name)
    end
  end

  private

  def split_waste_types
    self.waste_types = waste_types.first.delete(' ').split(',') if waste_types.count == 1
  end
end
