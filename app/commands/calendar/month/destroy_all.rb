class Calendar::Month::DestroyAll

  def initialize(months)
    @months = months
  end

  def call
    @months.destroy_all
  end
end
