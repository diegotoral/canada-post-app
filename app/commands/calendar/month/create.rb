class Calendar::Month::Create
  include Virtus.model
  include ActiveModel::Validations

  attribute :month_number, Integer
  attribute :region_config_id, Integer

  validates :month_number, :region_config_id, presence: true

  def call
    validate!
    create_month
  end

  private

  def create_month
    ::Calendar::Month.create(month_number: month_number, region_config_id: region_config_id)
  end
end
