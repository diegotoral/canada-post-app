class Calendar::Month::Update
  include Virtus.model
  include ActiveModel::Validations

  attribute :id, Integer
  attribute :advertiser_id, Integer

  def initialize(attributes)
    super
    @month = Calendar::Month.find(id)
  end

  def call
    update_month
  end

  private

  def update_month
    @month.update(advertiser_id: advertiser_id) if @month.advertiser_id != advertiser_id
  end
end
