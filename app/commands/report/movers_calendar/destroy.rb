class Report::MoversCalendar::Destroy
  include Virtus.model

  def initialize(calendar)
    @calendar = calendar
  end

  def call
    @calendar.destroy
  end
end
