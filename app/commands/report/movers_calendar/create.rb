class Report::MoversCalendar::Create
  include Virtus.model

  attribute :report_id, Integer
  attribute :path, String

  def call
    create_report_calendar
  end

  private

  def fetch_calendar_number
    path.split("/").last.split("-").first.split("_").first
  end

  def create_report_calendar
    ::Report::MoversCalendar.create(report_movers_id: report_id, path: path, number: fetch_calendar_number)
  end
end
