class Report::MoversCalendar::ZipCollection
  attr_accessor :report, :zip_path

  def initialize(report_id:)
    @report_id = report_id
    @report = Report::Movers.find(report_id)
    @zip_path = @report.zip_path
  end

  def call
    Report::Movers::StatusUpdate.new(@report, :zip_generating).call
    CalendarZipWorker.perform_async(@report_id)
  end
end
