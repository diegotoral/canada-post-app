require 'fileutils'

class Report::Movers::RemoveCalendars
  include Virtus.model

  def initialize(report)
    @report = report
  end

  def call
    remove_generated_calendars_files
    remove_generated_calendars_paths
  end

  private

  def remove_generated_calendars_paths
    @report.calendars.each { |calendar| Report::MoversCalendar::Destroy.new(calendar).call }
  end

  def remove_generated_calendars_files
    FileUtils.rm_rf("#{Rails.root}/calendars/#{@report.id.to_i}_*")
  end
end
