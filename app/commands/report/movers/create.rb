class Report::Movers::Create
  include Virtus.model

  attribute :file_name, String
  attribute :parsed_count, Integer

  def call
    create_report
  end

  private

  def create_report
    ::Report::Movers.create(file_name: file_name, parsed_count: parsed_count)
  end
end
