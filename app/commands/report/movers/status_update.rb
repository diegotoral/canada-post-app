class Report::Movers::StatusUpdate
  include Virtus.model

  def initialize(report, status)
    @report = report
    @status = Report::Movers.statuses[status]
  end

  def call
    update_status
  end

  private

  def update_status
    @report.update(status: @status)
  end
end
