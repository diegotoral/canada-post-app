class Report::Movers::BatchIdUpdate
  include Virtus.model

  attribute :report, Report::Movers
  attribute :batch_id, String

  def call
    update_batch_id
  end

  private

  def update_batch_id
    report.update(batch_id: batch_id)
  end
end
