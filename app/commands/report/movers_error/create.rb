class Report::MoversError::Create
  include Virtus.model

  attribute :report_id, Integer
  attribute :row, String
  attribute :message, String
  attribute :type, String

  def initialize(attributes)
    super
    @report = ::Report::Movers.find(report_id)
    @type = ::Report::MoversError.error_types[type]
  end

  def call
    create_report_error
  end

  private

  def create_report_error
    ::Report::MoversError.create(report_movers: @report, row: row, message: message.to_sentence, error_type: @type)
  end
end
