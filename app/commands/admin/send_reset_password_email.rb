class Admin::SendResetPasswordEmail
  def initialize(id)
    @admin = Admin.find(id)
  end

  def call
    @admin.send_reset_password_instructions
  end
end
