class Admin::Create
  class AdminAlreadyExists < StandardError; end

  include Virtus.model
  include ActiveModel::Validations

  attribute :email, String
  attribute :password, String
  attribute :password_confirmation, String

  validates                 :email, :password, presence: true
  validate                  :email, :email_uniqueness
  validates_confirmation_of :password
  validates_length_of       :password, within: 6..128

  def initialize(params)
    @email = Invitation.find_by!(token: params[:token]).email
    @password = params[:admin][:password]
    @password_confirmation = params[:admin][:password_confirmation]
  end

  def call
    validate!
    Admin.create(email: @email, password: @password)
  end

  private

  def email_uniqueness
    raise AdminAlreadyExists if Admin.where(email: @email).exists?
  end
end
