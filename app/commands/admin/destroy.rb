class Admin::Destroy
  def initialize(id)
    @admin = Admin.find(id)
  end

  def call
    @admin.destroy
  end
end
