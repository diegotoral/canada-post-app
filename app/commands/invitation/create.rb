class Invitation::Create
  class AdminAlreadyExists < StandardError; end

  include Virtus.model
  include ActiveModel::Validations

  attribute :email, String

  validates :email, presence: true
  validate :email_must_be_unique

  def initialize(params)
    @token = Devise.friendly_token(40)
    @email = params[:email].downcase
  end

  def call
    validate!
    create_invitation
    send_email
  end

  private

  def email_must_be_unique
    raise AdminAlreadyExists if Admin.where(email: @email).exists?
  end

  def create_invitation
    ::Invitation.create(email: @email, token: @token)
  end

  def send_email
    ::Invitation::InvitationMailer.invitation_email(@email, @token).deliver_later
  end
end
