class Notifications::CalendarsGeneratedMailer < InternalMailer

  def notify(email, report)
    @report = report
    mail(to: email,
         subject: 'Calendars generation finished',
         template_path: 'mailers/notifications/calendars_generated')
  end
end
