class Invitation::InvitationMailer < InternalMailer

  def invitation_email(email, token)
    @token = token
    mail(to: email,
         subject: 'You have been invited to cp admin panel',
         template_path: 'mailers/invitation')
  end
end
