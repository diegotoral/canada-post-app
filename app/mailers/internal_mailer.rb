class InternalMailer < ActionMailer::Base
  default from: 'administration@cp.com'
end
