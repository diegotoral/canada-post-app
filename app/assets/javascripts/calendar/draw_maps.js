L.mapbox.accessToken = MAPBOX_TOKEN

function DrawTinyMapChrome(place, container){
  var tinyMap = L.mapbox.map(container).setView(place, 17);
  L.mapbox.styleLayer(MAPBOX_STYLE_URL).addTo(tinyMap);

  var poiIcon = L.icon({
    iconUrl: PHANTOMJS_BASE_URL + '/assets/pushpins/tiny_map.png',
    iconSize: [70, 70],
    iconAnchor: [35, 70]
  });
  L.marker(place, {icon: poiIcon}).addTo(tinyMap);

  return new Promise(function(resolve, reject) {
    $('#' + container).waitForImages(function() {
      resolve()
    });
  })
}

function DrawTinyMapPhantom(place, container){
  Draw(place, container).then(function(){
    setTimeout(function(){
      $('#' + container + ' .leaflet-tile-loaded').each(function(){
        var matrix = $(this).css('transform').split(',');
        var posX = parseInt(matrix[4]);
        var posY = parseInt(matrix[5]);
        $(this).css({
          top: posY,
          left: posX,
          transform: 'none'
        });
        $('.leaflet-tile-container').css({position: 'absolute'})
      });
      $('#' + container + ' *:not(.leaflet-marker-icon)').css('transform', 'none');
      setTimeout(function(){window.status = 'map-ready'})
    }, 3000)
  })

  function Draw(place, container){
    var tinyMap = L.mapbox.map(container).setView(place, 17);
    L.mapbox.styleLayer(MAPBOX_STYLE_URL).addTo(tinyMap);

    var poiIcon = L.icon({
      iconUrl: PHANTOMJS_BASE_URL + '/assets/pushpins/tiny_map.png',
      iconSize: [70, 70],
      iconAnchor: [35, 70]
    });
    L.marker(place, {icon: poiIcon}).addTo(tinyMap);

    return new Promise(function(resolve, reject) {
      $('#' + container).waitForImages(function() {
        resolve()
      });
    })
  }
}

function DrawMaps(places, advertiserPlaces, nearestPost){
  var civicPoi = JSON.parse(places[0])
  var nearestPost = nearestPost
  var advertPoi = advertiserPlaces
  var smallPoi = JSON.parse(places[2])
  var latlng = places[1]
  var civicPoiArray = []
  var advertPoiArray = []
  var visible = false

  this.call = function() {
    return Promise.all([drawCivicMap(), drawAdvertMap()]);
  }

  function drawCivicMap(){
    var civicMap = L.mapbox.map('civicMapContainer').setView(latlng, 17);
    L.mapbox.styleLayer(MAPBOX_STYLE_URL).addTo(civicMap);
    for (var i = 0; i < civicPoi.length; ++i) {
      if(civicPoi[i]){
        visible = civicMap.getBounds().contains(L.latLng(civicPoi[i].latitude, civicPoi[i].longitude))
        civicPoiArray.push(
          {
            poi: civicPoi[i],
            visible: visible
          }
        )
        if(visible){
          var poiIcon = L.icon({
            iconUrl: fetchCivicIcon(civicPoi[i].category),
            iconSize: [80, 110],
            iconAnchor: [40, 110]
          });
          L.marker([civicPoi[i].latitude, civicPoi[i].longitude], {icon: poiIcon}).addTo(civicMap);
        }
      }
    }

    if(nearestPost){
      visible = civicMap.getBounds().contains(L.latLng(nearestPost.latitude, nearestPost.longitude))
      civicPoiArray.push(
        {
          poi: nearestPost,
          visible: visible
        }
      )
      if(visible){
        var poiIcon = L.icon({
          iconUrl: fetchCivicIcon("canada_posts"),
          iconSize: [80, 110],
          iconAnchor: [40, 110]
        });
        L.marker([nearestPost.latitude, nearestPost.longitude], {icon: poiIcon}).addTo(civicMap);
      }
    }
    for (var i = 0; i < smallPoi.length; ++i) {
      if(smallPoi[i].length > 0){
        var poiIcon = L.icon({
          iconUrl: fetchSmallPoiIcon(smallPoi[i][0].category),
          iconSize: [86, 119],
          iconAnchor: [43, 119]
        });
        for(var j = 0; j < smallPoi[i].length; j++){
          L.marker([smallPoi[i][j].latitude, smallPoi[i][j].longitude], {icon: poiIcon}).addTo(civicMap);
        }
      }
    }


    var myIcon = L.icon({
    	iconUrl: PHANTOMJS_BASE_URL + '/assets/pushpins/home_fmt.svg',
    	iconSize: [125, 172],
      iconAnchor: [62, 172]
    });
    L.marker(latlng, {icon: myIcon}).addTo(civicMap);

    civicPoiArray.push(
      {
        home: {
          lat: latlng[0],
          lng: latlng[1]
        }
      }
    )

    return new Promise(function(resolve, reject) {
      $('#civicMapContainer').waitForImages(function() {
        resolve(civicPoiArray)
      });
    })
  }

  function drawAdvertMap(){
    var advertMap = L.mapbox.map('advertMapContainer').setView(latlng, 15);
    L.mapbox.styleLayer(MAPBOX_STYLE_URL).addTo(advertMap);

    var counter = 0;
    for (var i = 0; i < advertPoi.length; ++i) {
      if(advertPoi[i].nearest){
        for(var j = 0; j < advertPoi[i].nearest.length; j++){
          visible = advertMap.getBounds().contains(L.latLng(advertPoi[i].nearest[j].latitude, advertPoi[i].nearest[j].longitude));
          advertPoiArray.push(
            {
              poi: advertPoi[i].nearest[j],
              visible: visible
            }
          )

          if(visible){
            var poiIcon = L.icon({
              iconUrl: fetchPushpinAdvertIcon(counter),
              iconSize: [80, 110],
              iconAnchor: [40, 110]
            });
            L.marker([advertPoi[i].nearest[j].latitude, advertPoi[i].nearest[j].longitude], {icon: poiIcon}).addTo(advertMap);
            counter = counter + 1;
          }
        }
      }
    }

    var myIcon = L.icon({
    	iconUrl: PHANTOMJS_BASE_URL + '/assets/pushpins/home_fmt.svg',
    	iconSize: [125, 172],
      iconAnchor: [62, 172]
    });
    L.marker(latlng, {icon: myIcon}).addTo(advertMap);

    return new Promise(function(resolve, reject) {
      $('#advertMapContainer').waitForImages(function() {
        resolve(advertPoiArray)
      });
    })
  }
}
