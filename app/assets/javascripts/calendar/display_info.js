function DisplayInfo(places){
  var civicPoi = places[0]
  var advertPoi = places[1]
  var lat = _.last(places[0]).home.lat
  var lng = _.last(places[0]).home.lng

  this.call = function() {
    return displayPlacesInfo()
  }

  function displayPlacesInfo(){
    return new Promise(function(resolve, reject) {
      displayCivicResults()
      displayAdvertResults()
      resolve(places)
    });
  }

  function displayCivicResults(){
    for (var i = 0; i < civicPoi.length - 1; ++i) {
      $('.civic-text > ol').append(
        "<li>" +
        "<div class='civic-place-name'>" + "<img src='"+fetchCivicIcon(civicPoi[i].poi.category)+"'></img> &nbsp;"
        + "<div class='title'>" + displayCivicPlaceName(civicPoi[i].poi.name) + "</div>"
        + "<div class='civicmap-description'>"
          + displayPlaceAddress(civicPoi[i].poi.street_address)
          + displayPlaceCity(civicPoi[i].poi.city + " " + civicPoi[i].poi.province + "\xa0\xa0" + civicPoi[i].poi.postal_code)
          + "<div class='map-place-distance'>"
            + displayCivicPhoneNumber(civicPoi[i].poi)
            + formatDistance(calculateDistance(lat, lng, civicPoi[i].poi.latitude, civicPoi[i].poi.longitude, 'K'))
          + "</div>"
        + "</div>"
        + "</li>"
      )
    }
  }

  function displayAdvertResults(){
    var sortedPlaces = _.sortBy(advertPoi, [function(o) { return !o.visible; }])
    var markerNumber = 1;
    var visiblePlaces = _.filter(sortedPlaces, { visible: true })
    var nonVisiblePlaces = _.filter(sortedPlaces, { visible: false })
    //Mark close places
    for (var i = 0; i < visiblePlaces.length; ++i) {
      $('.advert-column .column-'+(Math.ceil(markerNumber/2))).append(
        "<li class='no-style'>"
        + "<div class='advert-icon-image'><img src=" + fetchPushpinAdvertIcon(markerNumber-1) + "></img></div>"
        + "<div class='advert-name-with-pin'>"
        + displayAdvertPlaceName(visiblePlaces[i].poi.name)
        + "</div>"
        + "<div class='advert-li-text'>"
        + displayPlaceAddress(visiblePlaces[i].poi.street_address)
        + displayPlaceCity(visiblePlaces[i].poi.city + " " + visiblePlaces[i].poi.province + "\xa0\xa0" + visiblePlaces[i].poi.postal_code)
        + displayPhoneNumber(visiblePlaces[i].poi.phone)
        + "<div class='advert-website'>"
        + displayAdvertWebsite(visiblePlaces[i].poi.website)
        + "</div>"
        + "<div class='advert-distance map-place-distance'>"
        + formatDistance(calculateDistance(lat, lng, visiblePlaces[i].poi.latitude, visiblePlaces[i].poi.longitude, 'K'))
        + "</div>"
        + "</div>"
        + "</li>"
        + "</br>"
        + "</br>"
      )
      markerNumber = markerNumber+1;
    }
    //Mark distant places
    for (var i = 0; i < nonVisiblePlaces.length; ++i) {
      var distance = calculateDistance(lat, lng, nonVisiblePlaces[i].poi.latitude, nonVisiblePlaces[i].poi.longitude, 'K');
      if(distance < MAX_ADVERT_VENUE_DISTANCE_KM) {
        $('.advert-column .column-'+(Math.ceil(markerNumber/2))).append(
          "<li class='no-style'>"
          + "<div class='advert-name-without-pin'>"
          + displayAdvertPlaceName(nonVisiblePlaces[i].poi.name)
          + "</div>"
          + "<div class='advert-li-text'>"
          + displayPlaceAddress(nonVisiblePlaces[i].poi.street_address)
          + displayPlaceCity(nonVisiblePlaces[i].poi.city + " " + nonVisiblePlaces[i].poi.province + "\xa0\xa0" + nonVisiblePlaces[i].poi.postal_code)
          + displayPhoneNumber(nonVisiblePlaces[i].poi.phone)
          + "<div class='advert-website'>"
          + displayAdvertWebsite(nonVisiblePlaces[i].poi.website)
          + "</div>"
          + "<div class='advert-distance map-place-distance'>"
          + formatDistance(distance)
          + "</div>"
          + "</div>"
          + "<div class='outside-map-info'>"
          + I18n.t("js.map.out_of_range")
          + "</div>"
          + "</li>"
          + "</br>"
          + "</br>"
        )
        markerNumber = markerNumber+1;
      }
    }
  }

  function calculateDistance(lat1, lon1, lat2, lon2, unit) {
  	var radlat1 = Math.PI * lat1/180
  	var radlat2 = Math.PI * lat2/180
  	var theta = lon1-lon2
  	var radtheta = Math.PI * theta/180
  	var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
  	dist = Math.acos(dist)
  	dist = dist * 180/Math.PI
  	dist = dist * 60 * 1.1515
  	if (unit=="K") { dist = dist * 1.609344 }
  	if (unit=="N") { dist = dist * 0.8684 }
  	return dist
  }

  function formatDistance(dist){
    if(dist<1){
      return "(" + I18n.t("js.map.distance", {distance: Math.round(dist*1000)+" m"})+ ")"
    }
    else
    {
      return "(" + I18n.t("js.map.distance", {distance: _.ceil(dist,1).toString().replace(".",I18n.t('js.number.separator'))+" km"})+ ")"
    }
  }

  function displayCivicPlaceName(name){
    if(name){
      return name + "</div><br/>"
    }
    else{
      return ""
    }
  }

  function displayPlaceAddress(address){
    if(address){
      return address.replace(/\bavenue\b/i, "AVE").replace(/\bstreet\b/i, "ST").replace(/\broad\b/i, "RD") + "<br/>"
    }
    else{
      return ""
    }
  }

  function displayPlacePostalCode(code){
    if(code){
      return code + ", "
    }
    else{
      return ""
    }
  }

  function displayPlaceCity(city){
    if(city.replace(/ |\xa0/g,'').length > 0){
      return city + "<br/>"
    }
    else{
      return ""
    }
  }

  function displayAdvertPlaceName(name){
    if(name){
      return name + "<br/>"
    }
    else{
      return ""
    }
  }

  function displayAdvertWebsite(website){
    if(website){
      return website + "<br/>"
    }
    else{
      return ""
    }
  }
  function displayCivicPhoneNumber(poi){
    if(poi.category == 'polices' || poi.category == 'hospitals' || poi.category == 'fire_stations'){
      if(poi.phone){
        return "<span class='map-place-phone'>" + poi.phone + " " + "</span>"
      } else {
        return ''
      }
    }
    else {
      return ''
    }
  }

  function displayPhoneNumber(phone){
    if(phone){
      return phone + "<br/>"
    }
    else{
      return ""
    }
  }
}
