function GeocodeAddress(mover){
  this.mover = mover;

  this.call = function() {
    return geocode(mover)
  }

  function geocode(mover){
    return new Promise(function(resolve, reject) {
      $.get({
        url: "https://api.mapbox.com/geocoding/v5/mapbox.places/"
        + mover['address_line_1']+" "+mover['address_line_2']+" "+mover['city']+".json?"
        + "access_token="+MAPBOX_TOKEN
        + "&country=CA",
        dataType: 'json'
      })
      .done(function(data,status,xhr) {
        coords = { lat: data.features[0].center[1], lng: data.features[0].center[0] }
        resolve(coords);
      })
      .fail(function(xhr, textStatus, errorThrown) {
        reject();
      });
    });
  }
}
