function fetchCivicIcon(place){
  switch(place){
    case "polices":
      return PHANTOMJS_BASE_URL + '/assets/pushpins/police_fmt.svg'
      break;
    case "hospitals":
      return PHANTOMJS_BASE_URL + '/assets/pushpins/Hos_fmt.svg'
      break;
    case "canada_posts":
      return PHANTOMJS_BASE_URL + '/assets/pushpins/CP_fmt.svg'
      break;
    case "libraries":
      return PHANTOMJS_BASE_URL + '/assets/pushpins/small_pois/library.svg'
      break;
    case "fire_stations":
      return PHANTOMJS_BASE_URL + '/assets/pushpins/small_pois/fire-station.svg'
      break;
  }
}

function fetchPushpinAdvertIcon(index){
  switch(index) {
    case 0:
      return PHANTOMJS_BASE_URL + '/assets/pushpins/number_1.svg'
      break;
    case 1:
      return PHANTOMJS_BASE_URL + '/assets/pushpins/number_2.svg'
      break;
    case 2:
      return PHANTOMJS_BASE_URL + '/assets/pushpins/number_3.svg'
      break;
    case 3:
      return PHANTOMJS_BASE_URL + '/assets/pushpins/number_4.svg'
      break;
    case 4:
      return PHANTOMJS_BASE_URL + '/assets/pushpins/number_5.svg'
      break;
    case 5:
      return PHANTOMJS_BASE_URL + '/assets/pushpins/number_6.svg'
      break;
    case 6:
      return PHANTOMJS_BASE_URL + '/assets/pushpins/number_7.svg'
      break;
    case 7:
      return PHANTOMJS_BASE_URL + '/assets/pushpins/number_8.svg'
      break;
    case 8:
      return PHANTOMJS_BASE_URL + '/assets/pushpins/number_9.svg'
      break;
    case 9:
      return PHANTOMJS_BASE_URL + '/assets/pushpins/number_10.svg'
      break;
    case 10:
      return PHANTOMJS_BASE_URL + '/assets/pushpins/number_11.svg'
      break;
    case 11:
      return PHANTOMJS_BASE_URL + '/assets/pushpins/number_12.svg'
      break;
  }
}

function fetchSmallPoiIcon(place){
  return PHANTOMJS_BASE_URL + '/assets/pushpins/small_pois/'+place+'.svg'
}
