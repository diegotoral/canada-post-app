function advertiser_images_preview(){
  function readURL(input, target_id) {
   if (input.files && input.files[0]) {
     var reader = new FileReader();
     reader.onload = function (e) {
       $(target_id).attr('src', e.target.result);
     }
     reader.readAsDataURL(input.files[0]);
    }
  }
  $('#calendar_advertiser_left_advert_1').change(function () {
    readURL(this, '#left_advert_1_image');
  });
  $('#calendar_advertiser_left_advert_2').change(function () {
    readURL(this, '#left_advert_2_image');
  });
  $('#calendar_advertiser_left_advert_3').change(function () {
    readURL(this, '#left_advert_3_image');
  });
}

function advertiser_grids_preview(){
  $('#calendar_advertiser_grid').change(function(){
    switch($('#calendar_advertiser_grid').val()) {
    case '0':
      $('#left_advert_2').hide();
      $('#left_advert_3').hide();
      $('#left_advert_2_image').hide();
      $('#left_advert_3_image').hide();
      _remove_grid_classes();
      $('#left_advert_1_image').addClass('whole_page');
      break;
    case '1':
      $('#left_advert_2').show();
      $('#left_advert_3').show();
      $('#left_advert_2_image').show();
      $('#left_advert_3_image').show();
      _remove_grid_classes();
      $('#left_advert_1_image').addClass('one_third');
      $('#left_advert_2_image').addClass('one_third');
      $('#left_advert_3_image').addClass('one_third');
      break;
    case '2':
      $('#left_advert_2').show();
      $('#left_advert_3').hide();
      $('#left_advert_2_image').show();
      $('#left_advert_3_image').hide();
      _remove_grid_classes();
      $('#left_advert_1_image').addClass('one_third');
      $('#left_advert_2_image').addClass('two_third');
      break;
    case '3':
      $('#left_advert_2').show();
      $('#left_advert_3').hide();
      $('#left_advert_2_image').show();
      $('#left_advert_3_image').hide();
      _remove_grid_classes();
      $('#left_advert_1_image').addClass('two_third');
      $('#left_advert_2_image').addClass('one_third');
      break;
    default:
      break;
    };
  });
}

function _remove_grid_classes(){
  $('#left_advert_1_image').removeClass('whole_page one_one_one one_third two_third')
  $('#left_advert_2_image').removeClass('one_one_one one_third two_third')
  $('#left_advert_3_image').removeClass('one_one_one one_third two_third')
}
