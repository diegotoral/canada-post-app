MoverGeocodeFail = Class.new(StandardError)
BatchInvalid = Class.new(StandardError)

class CalendarGenerateWorker
  include Sidekiq::Worker

  def perform(hash_mover, report_id, headless_chrome = false)
    ensure_batch
    @report_id = report_id
    @hash_mover = hash_mover.symbolize_keys
    @headless_chrome = headless_chrome
    ensure_geocoding
    update_report_status
    generate_pdf
  rescue BatchInvalid
  rescue MoverGeocodeFail
    add_error_message(:location_invalid, 'couldn\'t geocode mover location')
  rescue PdfGenerator::RegionNotFound
    add_error_message(:location_invalid, 'postal code not found')
  rescue PdfGenerator::MapGenerationError
    add_error_message(:map_generation_error, 'location was invalid')
  rescue PdfGenerator::HeadlessChromeScriptError
    add_error_message(:map_generation_error, 'error while executing chrome script')
  rescue => e
    puts e
    CALENDAR_GENERATION_LOGGER.error("Report_id: #{report_id}, Postal_code: #{@hash_mover} - #{e.message}")
    add_error_message("unknown error #{e.message}", type: :location_invalid)
  end

  private

  def generate_pdf
    if @headless_chrome
      generator = PdfGenerator::HeadlessChromeService.new(@hash_mover, report_id: @report_id)
      generator.call
    else
      generator = PdfGenerator::GenerateService.new(@hash_mover, report_id: @report_id, bid: batch.bid)
      generator.call
    end
    Report::MoversCalendar::Create.new(report_id: @report_id, path: generator.file_path).call if generator.file_path.present?
  end

  def update_report_status
    Report::Movers::StatusUpdate.new(report, :in_progress).call if report.status == 'pending'
  end

  def ensure_geocoding
    @hash_mover = MoverStructure.new(@hash_mover).tap {|mover| mover.geocode}.to_h
    return if @hash_mover[:lat].present? && @hash_mover[:long].present?
    raise MoverGeocodeFail
  end

  def ensure_batch
    raise BatchInvalid if Sidekiq.redis { |r| r.exists("invalidated-bid-#{batch.bid}") }
  end

  def add_error_message(type, message)
    Report::MoversError::Create.new(report_id: @report_id, row: @hash_mover, message: [message], type: type).call
  end

  def report
    @report ||= Report::Movers.find(@report_id)
  end
end
