class RegionExampleCalendarGenerateWorker
  include Sidekiq::Worker

  def perform(hash_mover, region_id)
    @hash_mover = hash_mover
    @region = Calendar::RegionConfig.find(region_id)
    in_progress_status
    generator = PdfGenerator::GenerateService.new(@hash_mover, report_id: 0, temporary: true)
    generator.call
    Calendar::RegionConfig::Refresh.new(@region, File.open("#{Rails.root}#{generator.file_path}")).call
  rescue => e
    to_refresh_status
    CALENDAR_GENERATION_LOGGER.error("Example calendar, region_id: #{region_id} - #{e.message}")
  ensure
    FileUtils.rm("#{Rails.root}#{generator.file_path}") if generator.file_path
  end

  private

  def to_refresh_status
    Calendar::RegionConfig::StatusUpdate.new(@region, :to_refresh).call
  end

  def in_progress_status
    Calendar::RegionConfig::StatusUpdate.new(@region, :in_progress).call
  end
end
