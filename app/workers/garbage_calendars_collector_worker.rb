class GarbageCalendarsCollectorWorker
  include Sidekiq::Worker

  def perform
    Report::Movers.canceled_with_calendars.each do |report|
      remove_generated_calendars(report)
    end
  end

  private

  def remove_generated_calendars(report)
    Report::Movers::RemoveCalendars.new(report).call
  end
end
