require 'zip'

class CalendarZipWorker
  include Sidekiq::Worker

  def perform(report_id)
    report = Report::MoversPresenter.new(Report::Movers.includes(:calendars).find(report_id))
    calendars = report.calendars_presenter
    folder = calendars.first.calendar_folder
    input_filenames = calendars.map{|c| c.path.split("/").last}

    zipfile_name = "#{folder}/archive.zip"

    Zip::File.open(zipfile_name, Zip::File::CREATE) do |zipfile|
      input_filenames.each do |filename|
        zipfile.add(filename, folder + '/' + filename)
      end
    end
    report.update(zip_path: zipfile_name)
    Report::Movers::StatusUpdate.new(report, :completed).call
  end
end
