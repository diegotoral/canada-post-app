class InvitationsController < ApplicationController
  def invite_admin
    render 'invitations/invite_admin', locals: { admin: Admin.new }
  end

  def send_admin_invitation
    Invitation::Create.new(invitation_attributes).call
    redirect_to admin_list_path, flash: { notice: t('flash.notice.invitation.has_been_sent') }
  rescue Invitation::Create::AdminAlreadyExists
    redirect_to invite_admin_path, flash: { danger: t('flash.errors.admin.already_exists') }
  rescue ActiveModel::ValidationError => e
    redirect_to invite_admin_path, flash: { danger: e.model.errors.full_messages.join() }
  end

  private

  def invitation_attributes
    params.require(:admin).permit(:email).to_h
  end
end
