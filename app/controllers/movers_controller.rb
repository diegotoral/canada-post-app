class MoversController < ApplicationController

  def upload_file
    if ::Calendar::RegionConfig.with_wrong_status.present?
      return redirect_to movers_reports_path, flash: { danger: t('flash.error.movers.invalid_region') }
    end
    Calendar::EnqueueCalendarGeneratorsService.new(calendar_params).call
    redirect_to movers_reports_path, flash: { notice: t('flash.success.movers.parsed') }
  rescue Encoding::UndefinedConversionError
    redirect_to home_path, flash: { danger: t('flash.errors.movers.wrong_file') }
  rescue ActiveRecord::RecordNotFound
    redirect_to home_path, flash: { danger: t('flash.errors.region.not_found') }
  rescue Parser::HeadersException
    redirect_to home_path, flash: { danger: t('flash.errors.movers.wrong_headers') }
  rescue Parser::NoValidRecords
    redirect_to home_path, flash: { danger: t('flash.errors.movers.no_valid_records') }
  rescue Calendar::FileNotFound
    redirect_to home_path, flash: { danger: t('flash.errors.movers.not_found') }
  rescue Parser::InvalidByteSequenceException
    redirect_to home_path, flash: { danger: t('flash.errors.common.invalid_byte_sequence') }
  end

  protected

  def calendar_params
    params.permit(:file, :headless_chrome)
  end
end
