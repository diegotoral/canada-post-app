class AdminsController < ApplicationController
  skip_before_action :authenticate_admin!, only: [ :new_admin, :create_admin ]

  def admin_list
    render 'admins/admin_list',
            locals: { admins: Admin.all }
  end

  def new_admin
    invitation = Invitation::TokenVerifyService.new(token: params[:token])
    invitation.call

    sign_out(current_admin) if current_admin
    render 'admins/new_admin',
            locals: { admin: Admin.new(email: invitation.invitation.email) }
  rescue ActiveRecord::RecordNotFound
    redirect_to new_admin_session_path,
                flash: { danger: t('flash.errors.token.invalid') }
  end

  def create_admin
    Admin::Create.new(admin_create_params).call

    redirect_to new_admin_session_path, flash: { success: t('flash.success.admin.registered') }
  rescue ActiveModel::ValidationError => e
    redirect_to new_admin_path(token: admin_create_params[:token]),
                flash: { danger: e.model.errors.full_messages.join(',') }
  rescue ActiveRecord::RecordNotFound
    redirect_to new_admin_session_path,
                flash: { danger: t('flash.errors.invitation.token.not_found') }
  rescue Admin::Create::AdminAlreadyExists
    redirect_to invite_admin_path,
                flash: { danger: t('flash.errors.admin.already_exists') }
  end

  def remove_admin
    Admin::Destroy.new(params[:id]).call
    redirect_to admin_list_path,
                flash: { success: t('flash.success.admin.removed') }
  rescue ActiveRecord::RecordNotFound
    redirect_to admin_list_path,
                flash: { danger: t('flash.errors.admin.not_found') }
  end

  def reset_password
    Admin::SendResetPasswordEmail.new(params[:id]).call
    redirect_to admin_list_path,
                flash: { success: t('flash.success.password.instructions_sent') }
  rescue ActiveRecord::RecordNotFound
    redirect_to admin_list_path,
                flash: { danger: t('flash.errors.admin.not_found') }
  end

  private

  def admin_create_params
    params.permit(:token, admin: [:email, :password, :password_confirmation])
  end
end
