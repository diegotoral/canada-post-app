class ReportsController < ApplicationController
  skip_before_action :authenticate_admin!, only: [ :phantom_calendar ]

  def movers_reports
    start_date, end_date = Parser::WeekPickerService.new(params[:week]).call
    reports_presenter = Report::Movers.includes(:calendars).between_date(start_date, end_date).created_at_desc.map{ |report| Report::MoversPresenter.new(report) }
    render 'reports/movers_reports', locals: { movers_reports: reports_presenter, start_date: start_date, end_date: end_date }
  end

  def cancel_movers_generator
    Report::CancelGeneratorService.new(params[:id]).call
    redirect_to movers_reports_path
  rescue ActiveRecord::RecordNotFound
    redirect_to movers_reports_path, flash: { alert: t('flash.errors.report.not_found') }
  end

  def csv_errors
    respond_to do |format|
      format.csv { send_data Report::MoversToCsvService.new(params[:id]).call }
    end
  rescue => e
    redirect_to movers_reports_path, flash: { alert: t('flash.errors.report.not_found') }
  end

  def phantom_calendar
    @headless_chrome = params[:headless_chrome]
    calendar_data = Calendar::PhantomViewService.new(params[:id], phantom_calendar_params, params[:region_id]).call
    I18n.with_locale(set_locale(calendar_data[:mover])) do
      render 'calendar/show', layout: 'layouts/calendar', locals: calendar_data
    end
  end

  def calendars_index
    report = Report::Movers.find(params[:id])
    calendar_presenters = report.calendars.order(number: :asc)
    unless params[:skip_pagination]
      calendar_presenters = calendar_presenters.paginate(per_page: 50, page: params[:page])
    end
    render 'reports/calendars_index', locals: { report: report, calendars: calendar_presenters, skip_pagination: params[:skip_pagination] }
  rescue ActiveRecord::RecordNotFound
    redirect_to movers_reports_path, flash: { alert: t('flash.errors.report.not_found') }
  end

  def zip_calendars
    zip_collection = Report::MoversCalendar::ZipCollection.new(report_id: params[:id])

    if zip_collection.report.zip_path.present?
      send_file zip_collection.zip_path, disposition: :attachment
    else
      zip_collection.call
      redirect_to movers_reports_path, flash: { notice: t('flash.notice.reports.zip_is_being_generated') }
    end
  rescue ActiveRecord::RecordNotFound
    redirect_to movers_reports_path, flash: { alert: t('flash.errors.report.not_found') }
  end

  private

  def set_locale(mover)
    mover.french? ? :fr : :en
  end

  def phantom_calendar_params
    params.permit(:first_name, :last_name, :address_line_1, :address_line_2, :city, :province, :postal_code, :lat, :long,
                  :kit_no, :record_id, :start_date, :language, :multi_unit_building, :bagbun, :dmc, :list_order, :waste_pickup_url)
  end
end
