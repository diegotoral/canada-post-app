class Calendar::AdvertisersController < ApplicationController

  def index
    render 'calendar/advertisers/index', locals: { advertisers: Calendar::Advertiser.order(:name) }
  end

  def new
    render 'calendar/advertisers/new', locals: { advertiser: Calendar::Advertiser.new }
  end

  def create
    Calendar::Advertiser::Create.new(advertiser_params).call
    redirect_to calendar_advertisers_path, flash: { notice: t('flash.success.advertiser.created')}
  rescue ActiveModel::ValidationError => e
    redirect_to calendar_advertisers_path, flash: { danger: e.model.errors.full_messages.join(',') }
  end

  def edit
    advertiser = Calendar::Advertiser.includes(:locations).find(params[:id])
    render 'calendar/advertisers/edit', locals: { advertiser: advertiser }
  rescue ActiveRecord::RecordNotFound
    redirect_to calendar_advertisers_path, flash: { danger: t('flash.errors.advertiser.not_found') }
  end

  def update
    Calendar::Advertiser::Update.new(advertiser_params_on_update).call
    redirect_to calendar_advertisers_path, flash: { notice: t('flash.success.advertiser.updated')}
  rescue ActiveModel::ValidationError => e
    redirect_to calendar_advertisers_path, flash: { danger: e.model.errors.full_messages.join(',') }
  rescue ActiveRecord::RecordNotFound
    redirect_to calendar_advertisers_path, flash: { danger: t('flash.errors.advertiser.not_found') }
  rescue ::Parser::HeadersException
    redirect_to calendar_advertisers_path, flash: { danger: t('flash.errors.advertiser.wrong_headers') }
  end

  def destroy
    Calendar::Advertiser::Destroy.new(params[:id]).call
    redirect_to calendar_advertisers_path, flash: { notice: t('flash.success.advertiser.deleted')}
  rescue Calendar::Advertiser::Destroy::AdvertiserIsConnectedToMonth
    redirect_to calendar_advertisers_path, flash: { danger:  t('flash.errors.advertiser.in_use') }
  rescue ActiveRecord::RecordNotFound
    redirect_to calendar_advertisers_path, flash: { danger: t('flash.errors.advertiser.not_found') }
  end

  private

  def advertiser_params_on_update
    advertiser_params.merge({id: params[:id]})
  end

  def advertiser_params
    params.require(:calendar_advertiser).permit(:locations_file, :name, :cover_advert, :left_advert_1, :left_advert_2,
                                                :left_advert_3, :right_advert, :grid, :remove_right_advert,
                                                :fr_cover_advert, :fr_left_advert_1, :fr_left_advert_2,
                                                :fr_left_advert_3, :phone, :website, :hide_on_map,
                                                :tiny_map, :tiny_address, :logo, :tiny_address_logo).to_h
  end

end
