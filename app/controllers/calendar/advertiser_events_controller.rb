class Calendar::AdvertiserEventsController < ApplicationController

  def index
    render 'calendar/advertiser_events/index', locals: { advertiser: advertiser, events: advertiser.events.order(:name) }
  rescue ActiveRecord::RecordNotFound
    redirect_to calendar_advertisers_path, flash: { danger: t('flash.errors.advertiser.not_found') }
  end

  def new
    render 'calendar/advertiser_events/new', locals: { event: Calendar::AdvertiserEvent.new }
  end

  def create
    Calendar::AdvertiserEvent::Create.new(event_params).call
    redirect_to calendar_advertiser_advertiser_events_path(advertiser), flash: { notice: t('flash.success.advertiser_event.created')}
  rescue ActiveModel::ValidationError => e
    redirect_to calendar_advertiser_advertiser_events_path(advertiser), flash: { danger: e.model.errors.full_messages.join(',') }
  end

  def edit
    event = Calendar::AdvertiserEvent.find(params[:id])
    render 'calendar/advertiser_events/edit', locals: { event: event }
  rescue ActiveRecord::RecordNotFound
    redirect_to calendar_advertisers_path, flash: { danger: t('flash.errors.advertiser.not_found') }
  end

  def update
    Calendar::AdvertiserEvent::Update.new(event_params_on_update).call
    redirect_to calendar_advertiser_advertiser_events_path(advertiser), flash: { notice: t('flash.success.advertiser_event.updated')}
  rescue ActiveModel::ValidationError => e
    redirect_to calendar_advertisers_path, flash: { danger: e.model.errors.full_messages.join(',') }
  rescue ActiveRecord::RecordNotFound
    redirect_to calendar_advertisers_path, flash: { danger: t('flash.errors.advertiser.not_found') }
  end

  def destroy
    Calendar::AdvertiserEvent::Destroy.new(params[:id]).call
    redirect_to calendar_advertiser_advertiser_events_path(advertiser), flash: { notice: t('flash.success.advertiser_event.deleted')}
  rescue ActiveRecord::RecordNotFound
    redirect_to calendar_advertisers_path, flash: { danger: t('errors.advertiser.not_found') }
  end

  private

  def advertiser
    Calendar::Advertiser.includes(:events).find(params[:advertiser_id])
  end

  def event_params_on_update
    event_params.merge({id: params[:id]})
  end

  def event_params
    params.require(:calendar_advertiser_event).permit(:color, :fr_name, :name, :start_date, :end_date).merge({advertiser_id: params[:advertiser_id]}).to_h
  end
end
