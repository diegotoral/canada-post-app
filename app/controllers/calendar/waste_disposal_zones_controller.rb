class Calendar::WasteDisposalZonesController < ApplicationController
  def index
    render 'calendar/waste_disposal_zones/index',
      locals: { region: region, waste_disposal_zones: waste_disposal_zones }
  end

  protected

  def waste_disposal_zones
    region.waste_disposal_zones.select(:id, :name, :description)
  end

  def region
    @region ||= Calendar::RegionConfig.find(params[:region_config_id])
  end
end
