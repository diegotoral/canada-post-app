class Calendar::RegionConfigsController < ApplicationController
  def index
    render 'calendar/region_configs/index', locals: { regions: Calendar::RegionConfig.order(:region).map { |region| Calendar::RegionConfigPresenter.new(region) } }
  end

  def new
    region = Calendar::RegionConfig.new
    render 'calendar/region_configs/new', locals: { region_config: region, available: Report::Movers.not_finished.empty?, }
  end

  def create
    if Report::Movers.not_finished.present?
      return redirect_to calendar_region_configs_path, flash: { danger: t('flash.errors.region.calendars_generating') }
    end
    region = Calendar::RegionConfig::Create.new(region_config_params).call
    redirect_to edit_calendar_region_config_path(region), flash: { notice: t('flash.success.region_config.created')}
  rescue ActiveModel::ValidationError
    redirect_to calendar_region_configs_path, flash: { danger: t('flash.errors.region_config.not_valid')}
  end

  def edit
    region = Calendar::RegionConfigPresenter.new(Calendar::RegionConfig.includes(:months).find(params[:id]))
    render 'calendar/region_configs/edit', locals: { region_config: region, available: Report::Movers.not_finished.empty?,
                                                     advertisers: Calendar::Advertiser.all}
  rescue ActiveRecord::RecordNotFound
    redirect_to calendar_region_configs_path, flash: { danger: t('flash.errors.region.not_found') }
  end

  def update
    if Report::Movers.not_finished.present?
      return redirect_to calendar_region_configs_path, flash: { danger: t('flash.errors.region.calendars_generating') }
    end
    Calendar::RegionConfig::Update.new(region_config_params.merge({id: params[:id]})).call
    redirect_to calendar_region_configs_path, flash: { notice: t('flash.success.region_config.updated') }
  rescue ActiveRecord::RecordNotFound
    redirect_to calendar_region_configs_path, flash: { danger: t('flash.errors.region.not_found') }
  rescue ActionController::ParameterMissing
    redirect_to calendar_region_configs_path, flash: { danger: t('flash.errors.common.param_missing') }
  end

  def destroy
    if Report::Movers.not_finished.present?
      return redirect_to calendar_region_configs_path, flash: { danger: t('flash.errors.region.calendars_generating') }
    end
    Calendar::RegionConfig::Destroy.new(params[:id]).call
    redirect_to calendar_region_configs_path, flash: { notice: t('flash.success.region_config.destroyed') }
  rescue ActiveRecord::RecordNotFound
    redirect_to calendar_region_configs_path, flash: { danger: t('flash.errors.region.not_found') }
  end

  def refresh_calendar
    Calendar::RegionConfigRefreshService.new(params[:id]).call
    redirect_to calendar_region_configs_path, locals: { regions: Calendar::RegionConfig.all }, flash: { notice: t('flash.success.region_config.refresh') }
  rescue ActiveRecord::RecordNotFound
    redirect_to calendar_region_configs_path, flash: { danger: t('flash.errors.region.not_found') }
  end

  def upload_pois
    region_config = Calendar::RegionConfig.find(params[:region_config_id])
    Calendar::Location::DeleteFromRegion.new(region_config, params[:category]).call
    Parser::CreateLocationsService.new(file: params[:poi_file].tempfile, category: Calendar::Location.categories[params[:category]], region_id: params[:region_config_id]).call
    redirect_to edit_calendar_region_config_path(region_config), flash: { notice: t('flash.success.region_config.updated')}
  rescue Parser::HeadersException
    redirect_to calendar_region_configs_path, flash: { danger: t('flash.errors.region.poi.wrong_headers') }
  rescue ActiveRecord::RecordNotFound
    redirect_to calendar_region_configs_path, flash: { danger: t('flash.errors.region.not_found') }
  rescue Calendar::Location::WrongCategory
    redirect_to calendar_region_configs_path, flash: { danger: t('flash.errors.region.category_not_found') }
  rescue Parser::InvalidByteSequenceException
    redirect_to calendar_region_configs_path, flash: { danger: t('flash.errors.common.invalid_byte_sequence') }
  end

  def upload_zones
    importer = Calendar::WasteDisposalZone::KMLImporter.new(params[:file].tempfile.path, params[:region_id])
    importer.call
    redirect_to calendar_region_configs_path, flash: { notice: 'Map imported succesfully!' }
  rescue => error
    redirect_to calendar_region_configs_path, flash: { danger: error.message }
  end

  protected

  def region_config_params
    params.require(:calendar_region_config).permit(:region, :stamp, :codes_file, :anniversary, :front,
                                                   :signature, :front_left, :last_page, :waste_pickup_url,
                                                   :fr_front, :fr_anniversary, :fr_front_left, :fr_last_page,
                                                   :fr_neighbourhood, :neighbourhood, :fr_stamp,
                                                   calendar_months: [:advertiser_id]).to_h
  end
end
