class Calendar::WastePickupsController < ApplicationController
  before_action :abort_when_movers_not_finished, except: :index

  def index
    render 'calendar/waste_pickups/index',
      locals: { zone: zone, waste_pickups: waste_pickups, year: year, month: month, region: region, available: available? }
  end

  def save
    Calendar::WastePickup::FetchFromParamsService.new(zone, waste_pickup_params).call
    redirect_to calendar_region_config_waste_disposal_zone_waste_pickups_path(region, zone),flash: { success: t('flash.success.waste_pickup.saved') }
  end

  def save_week_pattern
    Calendar::WastePickup::FetchFromPatternService.new(zone, waste_pickup_params).call
    redirect_to calendar_region_config_waste_disposal_zone_waste_pickups_path(region, zone),flash: { success: t('flash.success.waste_pickup.pattern_used') }
  rescue Calendar::WastePickup::InvalidDate
    redirect_to calendar_region_config_waste_disposal_zone_waste_pickups_path(region, zone),flash: { alert: t('flash.errors.waste_pickup.invalid_date') }
  end

  private

  def abort_when_movers_not_finished
    return unless Report::Movers.not_finished.present?
    redirect_to calendar_region_configs_path, flash: { danger: t('flash.errors.region.calendars_generating') }
  end

  def waste_pickups
    zone.waste_pickups
  end

  def available?
    Report::Movers.not_finished.empty?
  end

  def month
    (date[:month] || Time.now.month).to_i
  end

  def year
    (date[:year] || Time.now.year).to_i
  end

  def date
    date_param.match(/(?<month>\d+)\/(?<year>\d+)/) || {}
  end

  def date_param
    params[:month_year] || ''
  end

  def zone
    @zone ||= region.waste_disposal_zones.find(params[:waste_disposal_zone_id] || params[:id])
  end

  def region
    @region ||= Calendar::RegionConfig.find(params[:region_config_id])
  end

  def waste_pickup_params
    params.require(:calendar_waste_disposal_zone).permit(:year, :date_from, :date_to, :month,
      waste_pickups_week_1: %i(garbage recycling yard_waste green_bin christmas_tree),
      waste_pickups_week_2: %i(garbage recycling yard_waste green_bin christmas_tree),
      waste_pickups: %i(garbage recycling yard_waste green_bin christmas_tree))
  end
end
