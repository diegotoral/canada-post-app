class Calendar::CategoryFiltersController < ApplicationController
  def index
    render 'calendar/category_filters/index', locals: { filters: Calendar::CategoryFilter.order(:category) }
  end

  def update
    Calendar::CategoryFilter::Update.new(filter_params.merge(id: params[:id])).call
    redirect_to calendar_category_filters_path, flash: { notice: t('flash.success.category_filter.updated')}
  rescue => e
    redirect_to calendar_category_filters_path, flash: { danger: e }
  end

  private

  def filter_params
    params.require(:calendar_category_filter).permit( :words )
  end
end
