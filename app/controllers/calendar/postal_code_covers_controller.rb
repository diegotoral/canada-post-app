class Calendar::PostalCodeCoversController < ApplicationController
  def new
    code_cover = Calendar::PostalCodeCover.new(region_config_id: params[:region_config_id])
    render 'calendar/postal_code_covers/new', locals: { code_cover: code_cover }
  end

  def create
    code_cover = Calendar::PostalCodeCover::Create.new(postal_code_cover_params).call
    redirect_to edit_calendar_region_config_path(code_cover.region_config), flash: { notice: t('flash.success.postal_code_cover.created')}
  rescue ActiveModel::ValidationError
    render 'calendar/postal_code_covers/new', locals: { code_cover: code_cover }
  end

  def edit
    code_cover = Calendar::PostalCodeCover.find(params[:id])
    render 'calendar/postal_code_covers/edit', locals: { code_cover: code_cover }
  end

  def update
    code_cover = Calendar::PostalCodeCover::Update.new(postal_code_cover_params, params[:id]).call
    redirect_to edit_calendar_region_config_path(code_cover.region_config), flash: { notice: t('flash.success.postal_code_cover.updated')}
  rescue ActiveModel::ValidationError
    render 'calendar/postal_code_covers/edit', locals: { code_cover: code_cover }
  end

  def destroy
    region_config = Calendar::PostalCodeCover.find(params[:id]).region_config
    Calendar::PostalCodeCover::Destroy.new(params[:id]).call
    redirect_to edit_calendar_region_config_path(region_config), flash: { notice: t('flash.success.postal_code_cover.destroyed')}
  end

  private

  def postal_code_cover_params
    params.require(:calendar_postal_code_cover).permit(:region_config_id, :front, :fr_front, :codes_file, :name)
  end
end
