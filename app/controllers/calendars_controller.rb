class CalendarsController < ApplicationController
  def show
    if File.exists?(calendar_path)
      send_file(calendar_path, x_sendfile: true, disposition: :inline)
    else
      raise ActionController::RoutingError, 'resource not found'
    end
  end

  private

  def calendar_path
    @path ||= Rails.root.join('calendars', [params[:filepath], params[:format]].join('.'))
  end
end
