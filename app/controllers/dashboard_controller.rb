class DashboardController < ApplicationController

  def home
    reports = Report::Movers.includes(:calendars).completed_last(5).map{ |report| Report::MoversPresenter.new(report) }

    render 'dashboard/home', locals: {
      reports: reports,
      calendars: Report::MoversCalendar.group_by_day(:created_at, format: '%m/%d/%Y').count,
      available: ::Calendar::RegionConfig.with_wrong_status.empty?,
      calendars_weekly_current_month: Report::MoversCalendar.created_at_month(Time.zone.now).group_weekly,
      errors_weekly_current_month: Report::MoversError.created_at_month(Time.zone.now).group_weekly,
      calendars_monthly_overtime: Report::MoversCalendar.group_monthly,
      errors_monthly_overtime: Report::MoversError.group_monthly,
      parsed_count: Report::Movers.pluck(:parsed_count).sum,
      total_files: Report::Movers.count,
    }
  end

end
