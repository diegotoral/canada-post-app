
class Parser::CreateLocationsService
  attr_accessor :parsed, :wrong_records

  CORRECT_HEADERS = %w(name street_address city province postal_code phone website)
  COMMON_DELIMITERS = [',', '|', ';', "\t"]

  def initialize(file:, advertiser_id: nil, region_id: nil, category: 0)
    @column_separator = fetch_column_separator(file)
    begin
      @file = CSV.read(file, headers: true, skip_blanks: true, col_sep: @column_separator, encoding: Parser::EncodeDetectService.new(file).call )
    rescue ArgumentError => e
      Rails.logger.error(e)
      raise Parser::InvalidByteSequenceException
    end
    @advertiser_id = advertiser_id
    @region_id = region_id
    @category = category
    @headers = @file.headers
  end

  def call
    validate_headers!(@headers)
    @file.each do |row|
      begin
        location = Calendar::Location::Create.new(attributes: row.to_hash, advertiser_id: @advertiser_id,
                                                  region_id: @region_id, category: @category).call
      rescue => e
        Rails.logger.warn(e)
        next
      end
    end
  end

  protected

  def fetch_column_separator(file)
    first_line = File.read(file)[0..67]
    return nil unless first_line
    delimiters = {}
    COMMON_DELIMITERS.each {|delim| delimiters[delim] = first_line.count(delim)}
    delimiters = delimiters.sort { |a,b| b[1]<=>a[1] }
    delimiters.size > 0 ? delimiters[0][0] : nil
  end

  def validate_headers!(headers)
    raise ::Parser::HeadersException if (CORRECT_HEADERS - headers).present?
  end
end
