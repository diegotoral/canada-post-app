require 'rchardet'

class Parser::EncodeDetectService
  def initialize(file)
    @file = file.read
  end

  def call
    encoding_hash = CharDet.detect(@file)
    return "macroman" if encoding_hash['encoding'] == "SHIFT_JIS"
    encoding_hash['encoding'] || 'UTF-8'
  end
end
