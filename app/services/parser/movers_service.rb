class Parser::MoversService

  attr_accessor :parsed, :wrong_records

  CORRECT_HEADERS = %w(kit_no record_id start_date language first_name last_name address_line_1 address_line_2 city postal_code multi_unit_building bagbun dmc list_order)
  COMMON_DELIMITERS = [',', '|', ';', "\t"]

  def initialize(file)
    column_separator = fetch_column_separator(file.tempfile)
    begin
      @file = CSV.read(file.tempfile, headers: true, header_converters: lambda { |h| h.downcase.gsub(' ', '_') }, skip_blanks: true, col_sep: column_separator, encoding: Parser::EncodeDetectService.new(file).call )
    rescue ArgumentError => e
      Rails.logger.error(e)
      raise Parser::InvalidByteSequenceException
    end
    @headers = @file.headers
  end

  def call
    validate_headers!(@headers)
    @wrong_records = {}
    @parsed = []
    @file.each do |row|
      begin
        mover = Mover::BuildService.new(row.to_h).call
        mover.call
        @parsed << mover
      rescue => e
        CALENDAR_GENERATION_LOGGER.error("Record failed on parsing: #{e}")
        @wrong_records[row] = mover.errors.full_messages
        next
      end
    end
    raise Parser::NoValidRecords if @parsed.empty?
  end

  protected

  def fetch_column_separator(file)
    first_line = File.read(file)[0..67]
    return nil unless first_line
    delimiters = {}
    COMMON_DELIMITERS.each {|delim| delimiters[delim] = first_line.count(delim)}
    delimiters = delimiters.sort { |a,b| b[1]<=>a[1] }
    delimiters.size > 0 ? delimiters[0][0] : nil
  end

  def validate_headers!(headers)
    raise Parser::HeadersException if (CORRECT_HEADERS - headers).present?
  end
end
