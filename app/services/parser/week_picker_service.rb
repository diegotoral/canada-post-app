class Parser::WeekPickerService
  def initialize(week)
    @week = week
  end

  def call
    if @week
      @week.split('-')
    else
      [Time.now.beginning_of_week.strftime('%d/%m/%Y'), Time.now.end_of_week.to_date.strftime('%d/%m/%Y')]
    end
  end

end
