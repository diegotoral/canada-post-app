class Parser::RegionCodesService

  def initialize(file)
    @file = File.read(file.tempfile)
    @new_line_char = fetch_new_line_char
  end

  def call
    @file.each_line(@new_line_char).map do |row|
      row.strip
    end
  end

  protected

  def fetch_new_line_char
    new_line_chars = %W(\n \r)
    new_line_chars.each { |char| return char if @file.count(char) > 0 }
  end
end
