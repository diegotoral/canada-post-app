require 'timeout'

module PdfGenerator
  RegionNotFound = Class.new(StandardError)
  MapGenerationError = Class.new(StandardError)

  class PdfGenerator::GenerateService
    attr_accessor :file_path

    ALLOWED_RETRIES_COUNT = 50

    def initialize(hash_mover, options = {})
      @hash_mover = hash_mover.symbolize_keys
      @kit_no = hash_mover[:kit_no]
      @report_id = options[:report_id] || 0
      @temporary = options[:temporary] || false
      @bid = options[:bid]
    end

    def call
      run_phantomjs
    rescue => error
      raise error
    ensure
      unlink_phantomjs_file
    end

    def file_path
      @file_path ||= "/calendars#{@temporary ? '/temporary/' : '/'}#{@report_id}_#{@hash_mover[:kit_no]}/#{@hash_mover[:list_order]}_#{unique_file_name}.pdf"
    end

    private

    def run_phantomjs(retry_number = ALLOWED_RETRIES_COUNT)
      pid = Process.spawn("phantomjs --ssl-protocol=tlsv1 #{phantomjs_file.path}")
      Timeout.timeout(80) do
        Process.wait(pid)
        $?.success?
      end
    rescue Timeout::Error
      puts "Retry #{phantomjs_file.path} | elapsed retries: #{retry_number - 1}"
      Process.kill('TERM', pid)
      return if batch_invalid?
      retry_number.zero? ? report_map_error : run_phantomjs(retry_number - 1)
    end

    def batch_invalid?
      Sidekiq.redis { |r| r.exists("invalidated-bid-#{@bid}") }
    end

    def report_map_error
      raise PdfGenerator::MapGenerationError
    end

    def unlink_phantomjs_file
      phantomjs_file.unlink if phantomjs_file
    end

    def phantomjs_file
      @phantomjs_file ||= tempfile.tap do |file|
        file.write(render_js)
        file.close
      end
    end
    

    def render_js
      ActionController::Base.new.render_to_string 'calendar/phantom.js.erb', locals: {
        report_id: @report_id, mover: @hash_mover.to_param, file_path: file_path, region_id: region.id }
    end

    def region
      @region ||= ::Calendar::RegionConfig.find_by_region_code!(region_code)
    rescue ActiveRecord::RecordNotFound
      raise PdfGenerator::RegionNotFound
    end

    def region_code
      @hash_mover[:postal_code].first(3)
    end

    def tempfile
      Tempfile.new(%w(phantomjs_file .js), Rails.root.join('tmp'))
    end

    def unique_file_name
      [@hash_mover[:record_id], Random.rand(9999999), Time.now.to_i].join('-')
    end
  end
end
