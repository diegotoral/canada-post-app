module PdfGenerator
  RegionNotFound = Class.new(StandardError)
  MapGenerationError = Class.new(StandardError)
  HeadlessChromeScriptError = Class.new(StandardError)

  class PdfGenerator::HeadlessChromeService
    NODE_PATH = Rails.application.secrets.node
    ALLOWED_RETRIES_COUNT = 15

    attr_accessor :file_path

    def initialize(hash_mover, options = {})
      @hash_mover = hash_mover.symbolize_keys
      @kit_no = hash_mover[:kit_no]
      @report_id = options[:report_id] || 0
      @temporary = options[:temporary] || false
    end

    def call
      run_headless_chrome
    end

    def file_path
      @file_path ||= "/calendars#{@temporary ? '/temporary/' : '/'}#{@report_id}_#{@hash_mover[:kit_no]}/#{@hash_mover[:list_order]}_#{unique_file_name}.pdf"
    end

    private

    def region
      @region ||= ::Calendar::RegionConfig.find_by_region_code!(region_code)
    rescue ActiveRecord::RecordNotFound
      raise PdfGenerator::RegionNotFound
    end

    def region_code
      @hash_mover[:postal_code].first(3)
    end

    def run_headless_chrome(retry_number = 0)
      pid = Process.spawn("#{NODE_PATH} ./headless-chrome/headless_chrome.js '#{url_path}&headless_chrome=true' '#{file_path}' '#{CHROME_PORT}'")
      pid, status = Process.wait2
      if status.exitstatus == 2
        puts "Retry #{file_path} | #{retry_number + 1}"
        retry_number > ALLOWED_RETRIES_COUNT ? report_chrome_error : run_headless_chrome(retry_number + 1)
      elsif status.exitstatus != 0
        report_chrome_error
      end
    end

    def report_chrome_error
      raise HeadlessChromeScriptError
    end

    def url_path
      "#{PHANTOMJS_BASE_URL}/phantom_calendar/#{@report_id}?#{@hash_mover.to_param}&region_id=#{region.id}"
    end

    def unique_file_name
      [@hash_mover[:record_id], Random.rand(9999999), Time.now.to_i].join('-')
    end
  end
end
