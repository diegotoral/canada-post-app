class Mover::BuildService

  def initialize(params, options={})
    @params = params
    @example = options[:example] || false
    @region_code = options[:example_code] || 'M4C'
  end

  def call
    build_mover
  end

  private

  def build_mover
    if @example
      MoverStructure.new(start_date: Time.now, record_id: 123, postal_code: @region_code + '-XXX', language: 'F', first_name: 'XYZ', last_name: 'XYZ', address_line_1: 'Cowan Avenue',
                           address_line_2: '34', city: 'Toronto', province: 'ON', multi_unit_building: 'N', waste_pickup_url: 'http://exampleurl.com')
    else
      MoverStructure.new(@params)
    end
  end
end
