class Report::MoversToCsvService

  def initialize(report_id)
    @report = Report::Movers.find(report_id)
  end

  def call
    create_csv
  end

  private

  def create_csv
    CSV.generate do |csv|
      csv << %w(record_id errors)
      add_records_to_csv(csv, @report.wrong_records.records)
      add_records_to_csv(csv, @report.wrong_records.locations)
    end
  end

  def add_records_to_csv(csv, records)
    records.each do |record|
      csv << [record.row.strip, record.message]
    end
  end
end
