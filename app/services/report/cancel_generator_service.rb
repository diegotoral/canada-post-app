class Report::CancelGeneratorService
  def initialize(report_id)
    @report = ::Report::Movers.find(report_id)
    @batch = Sidekiq::Batch.new(@report.batch_id)
  end

  def call
    mark_as_invalidate
    ::Report::Movers::StatusUpdate.new(@report, :canceling).call
  end

  def mark_as_invalidate
    Sidekiq.redis do |r|
      r.setex("invalidated-bid-#{@batch.bid}", 108_000, 1)
    end
  end
end
