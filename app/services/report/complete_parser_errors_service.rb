class Report::CompleteParserErrorsService
  def initialize(report, error_hash)
    @report = report
    @error_hash = error_hash
  end

  def call
    @error_hash.each do |error_row, error_message|
      ::Report::MoversError::Create.new(report_id: @report.id, type: :record_invalid, row: error_row, message: error_message).call
    end
  end
end
