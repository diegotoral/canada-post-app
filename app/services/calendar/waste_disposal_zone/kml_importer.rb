class Calendar::WasteDisposalZone
  KMLImporter = Struct.new(:file_path, :region_id) do
    def call
      commands.each(&:call)
    end

    private

    def commands
      data.map { |data| Calendar::WasteDisposalZone::Create.new(data.merge(region_id: region_id)) }
    end

    def data
      @data ||= KMLParser.new(file_path).call
    end
  end
end
