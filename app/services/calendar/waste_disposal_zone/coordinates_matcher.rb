# require 'extensions/pinp'

Calendar::WasteDisposalZone::CoordinatesMatcher = Struct.new(:lat, :lng, :region) do
  def call
    region.waste_disposal_zones.select(&method(:in_zone?))
  end

  private

  def in_zone?(zone)
    zone.coordinates.any? { |coords| polygon(coords).contains?(point) }
  end

  def polygon(list)
    Geokit::Polygon.new(points(list))
  end

  def points(list)
    list.map { |coords| Geokit::LatLng.new(*coords) }
  end

  def point
    @point ||= Geokit::LatLng.new(lng, lat)
  end
end
