class Calendar::WasteDisposalZone
  KMLParser = Struct.new(:file_path) do
    def call
      layers.flat_map do |layer|
        zones(layer).map do |zone|
          {
            map_name: name(document),
            map_description: description(document),
            waste_types: name(layer),
            region_description: description(layer),
            name: name(zone),
            description: description(zone),
            coordinates: polygons(zone).map do |poly|
              coordinates(polygon_data(poly))
            end
          }
        end
      end
    end

    private

    def coordinates(text)
      (text.lines.count > 1 ? text.lines : text.split(' ')).
        map(&:chomp).
        map(&:strip).
        reject(&:blank?).
        map(&method(:remove_tail)).
        map(&method(:parse_coords))
    rescue ArgumentError, TypeError
      raise InvalidPolygonCoordinates.new(text)
    end

    def remove_tail(line)
      line.gsub(/,0$/, '')
    end

    def parse_coords(line)
      line.split(?,).map(&method(:Float))
    end

    def polygon_data(tag)
      tag.css('coordinates').first.content
    rescue NoMethodError
      ''
    end

    def polygons(zone)
      zone.css('Polygon')
    end

    def name(tag)
      tag.css('> name').first.content
    rescue NoMethodError
      ''
    end

    def description(tag)
      tag.css('> description').first.content
    rescue NoMethodError
      ''
    end

    def zones(layer)
      layer.css('Placemark')
    end

    def layers
      document.css('Folder')
    end

    def document
      @document ||= Nokogiri::XML(file).css('Document')
    end

    def file
      @file ||= File.open(file_path)
    end

    InvalidPolygonCoordinates = Class.new(Calendar::WasteDisposalZone::WasteDisposalError)
  end
end
