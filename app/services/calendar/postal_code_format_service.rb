class Calendar::PostalCodeFormatService
  def initialize(postal_code)
    @postal_code = postal_code
  end

  def call
    return @postal_code if @postal_code.length == 7 && @postal_code[3] == " "
    @postal_code.scan(/.{3}/).join(" ")
  end
end
