Calendar::Locations::GeocodeService = Struct.new(:address) do
  def call
    geocode_retrier
  end

  private

  def geocode_retrier(counter = 0)
    if counter < 5
      begin
        geocode
      rescue => e
        CALENDAR_GENERATION_LOGGER.error("Geocoding error : #{address} : #{e}")
        sleep(2)
        geocode_retrier(counter + 1)
      end
    else
      [nil, nil]
    end
  end

  def geocode
    Geocoder.coordinates(address) || [nil, nil]
  end
end
