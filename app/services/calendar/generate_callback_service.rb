class Calendar::GenerateCallbackService

  def on_complete(status, opts)
    @report = Report::Movers.find(opts['report_id'])
    update_status_after_finish
    remove_calendars
  rescue => e
    CALENDAR_GENERATION_LOGGER.error("Batch callback error!! #{e} | opts: #{opts} | status: #{status}")
  end

  private

  def remove_calendars
    GarbageCalendarsCollectorWorker.perform_async
  end

  def canceled?
    @report.status == 'canceling'
  end

  def update_status_after_finish
    canceled? ? canceled_status : completed_status
  end

  def canceled_status
    Report::Movers::StatusUpdate.new(@report, :canceled).call
  end

  def completed_status
    send_notification_emails
    Report::Movers::StatusUpdate.new(@report, :completed).call
  end

  def send_notification_emails
    Admin.all.each { |admin| Notifications::CalendarsGeneratedMailer.notify(admin.email, @report).deliver_later }
  end
end
