Calendar::WastePickup::FetchFromParamsService = Struct.new(:zone, :options) do
  def call
    attributes.each do |day, params|
      Calendar::WastePickup::UpdateDayInfo.new(params.to_h.merge(zone: zone, date: date(day))).call
    end
    Calendar::RegionConfigValidationStatusService.new(zone.region_config).call
  end

  private

  def date(day)
    Date.new(options[:year].to_i, options[:month].to_i, day.to_i)
  end

  def attributes
    options[:waste_pickups]
  end
end
