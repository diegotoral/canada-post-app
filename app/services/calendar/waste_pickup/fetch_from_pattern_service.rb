class Calendar::WastePickup::InvalidDate < StandardError; end

Calendar::WastePickup::FetchFromPatternService = Struct.new(:zone, :options) do
  def call
    purge
    persist
    validate_region unless options[:dont_validate_region]
  end

  private

  def persist
    (0..6).each do |week_day|
      even = false
      all_days_by_week_day[week_day].each do |day|
        if even
          even = false
          Calendar::WastePickup::UpdateDayInfo.new(waste_pickups_week_2[week_day.to_s].merge(zone: zone, date: day)).call
        else
          even = true
          Calendar::WastePickup::UpdateDayInfo.new(waste_pickups_week_1[week_day.to_s].merge(zone: zone, date: day)).call
        end
      end
    end
  end

  def validate_region
    Calendar::RegionConfigValidationStatusService.new(zone.region_config).call
  end

  def purge
    Calendar::WastePickup::DestroyAll.new(zone.waste_pickups.from_to(date_from, date_to)).call
  end

  def all_days_by_week_day
    @all_days ||= (date_from..date_to).group_by(&:wday)
  end

  def date_from
    begin
      @date_from ||= Date.parse(options[:date_from])
    rescue ArgumentError
      raise Calendar::WastePickup::InvalidDate
    end
  end

  def date_to
    begin
      @date_to ||= Date.parse(options[:date_to])
    rescue ArgumentError
      raise Calendar::WastePickup::InvalidDate
    end
  end

  def waste_pickups_week_1
    options[:waste_pickups_week_1].to_h
  end

  def waste_pickups_week_2
    options[:waste_pickups_week_2].to_h
  end
end
