Calendar::Events::WastePickupService = Struct.new(:zones, :date) do
  def call
    return [] unless zones.any?
    zones.flat_map do |zone|
      if zone.related_to
        zone = zones.select { |zone| zone.waste_types.include?(zone.related_to) }
      end
      next unless zone.respond_to?(:waste_pickups)
      zone.waste_pickups.newer_than_month(date).flat_map do |waste_pickup|
        waste_pickup.waste_list.map do |waste_type|
          if zone.waste_types.include?(waste_type)
            CalendarEventStructure.new(name: waste_type, type: 'waste', date: waste_pickup.date.to_date).call
          end
        end
      end
    end.compact
  end
end
