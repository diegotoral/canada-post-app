class Calendar::Events::GenerateService
  def initialize(moving_date, region, zones, multiunit)
    @moving_date = moving_date
    @region = region
    @zones = zones
    @multiunit = multiunit == "Y"
  end

  def call
    Calendar::Events::AdvertisersEventsMapService.new(@region, [@moving_date, @moving_date + 1.year]).call +
    Calendar::Events::MovingDateService.new(@moving_date).call +
    Calendar::Events::HolidaysService.new(@region, [@moving_date, @moving_date + 1.year]).call +
    (@multiunit ? [] : Calendar::Events::WastePickupService.new(@zones, @moving_date).call)
  end
end
