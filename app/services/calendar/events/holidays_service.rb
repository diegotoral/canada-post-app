class Calendar::Events::HolidaysService

  def initialize(region, forbidden_dates)
    @region = region
    @forbidden_dates = forbidden_dates
  end

  def call
    holidays_to_calendar_events
  end

  private

  def holidays_to_calendar_events
    @region.holidays.flat_map do |holiday|
      if @forbidden_dates.exclude?(holiday.date)
        CalendarEventStructure.new(name: holiday.name, fr_name: holiday.fr_name, date: holiday.date, type: 'holiday')
      end
    end.compact
  end
end
