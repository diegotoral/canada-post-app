class Calendar::Events::MovingDateService
  def initialize(date)
    @date = date
  end

  def call
    [moving_day_hash, anniversary_hash]
  end

  private

  def moving_day_hash
    CalendarEventStructure.new(name: 'Moving Day', type: 'moving_date', date: @date).call
  end

  def anniversary_hash
    CalendarEventStructure.new(name: 'Happy Anniversary', type: 'anniversary_moving_date', date: @date + 1.year).call
  end
end
