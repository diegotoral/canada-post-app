class Calendar::Events::AdvertisersEventsMapService

  def initialize(region, forbidden_dates)
    @region = region
    @forbidden_dates = forbidden_dates
  end

  def call
    advertiser_events_to_calendar_events
  end

  private

  def advertiser_events_to_calendar_events
    @region.advertisers.map do |advertiser|
      advertiser.events.map do |event|
        map_to_event(event).compact
      end
    end.flatten
  end

  def map_to_event(adv_event)
    days = (adv_event.end_date - adv_event.start_date).to_i
    (0..days).map do |day|
      if @forbidden_dates.exclude?(adv_event.start_date + day.day)
        CalendarEventStructure.new(event_color: adv_event.color, name: adv_event.name, fr_name: adv_event.fr_name, date: adv_event.start_date + day.day, type: 'advertiser_event')
      end
    end
  end
end
