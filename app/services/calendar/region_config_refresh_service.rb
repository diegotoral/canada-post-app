class Calendar::RegionConfigRefreshService
  def initialize(id)
    @region = Calendar::RegionConfig.find(id)
  end

  def call
    update_status
    generate_example_calendar
  end

  private

  def generate_example_calendar
    mover = Mover::BuildService.new({}, example: true, example_code: @region.codes.first).call
    RegionExampleCalendarGenerateWorker.perform_async(mover.to_hash, @region.id)
  end

  def update_status
    ::Calendar::RegionConfig::StatusUpdate.new(@region, :pending).call
  end
end
