module Calendar
  PhantomViewService = Struct.new(:report_id, :movers_data, :region_id) do
    def call
      { mover: mover, region: region, report_id: report_id, events: events }
    end

    private

    def events
      Calendar::Events::GenerateService.new(mover.start_date, region, zones, mover.multi_unit_building).call
    end

    def zones
      mover.zones
    end

    def region
      @region ||= Calendar::RegionConfig.includes(:months, advertisers: [:events]).find(region_id)
    end

    def mover
      @mover ||= MoverStructure.new(movers_data).call.tap {|mover| mover.find_zones}
    end
  end
end
