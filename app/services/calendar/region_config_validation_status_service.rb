class Calendar::RegionConfigValidationStatusService

  def initialize(region)
    @region = region
  end

  def call
    return region_config_invalid_status unless region_config_valid?
    return months_invalid_status unless months_valid?
    region_config_to_refresh_status
  end

  private

  def region_config_to_refresh_status
    ::Calendar::RegionConfig::StatusUpdate.new(@region, :to_refresh).call
  end

  def region_config_invalid_status
    ::Calendar::RegionConfig::StatusUpdate.new(@region, :region_config_invalid).call
  end

  def months_invalid_status
    ::Calendar::RegionConfig::StatusUpdate.new(@region, :advertisers_invalid).call
  end

  def region_config_valid?
    return false unless @region.codes.present? && @region.front.present? &&
                        @region.front_left.present? && @region.signature.present? &&
                        @region.last_page.present? && @region.anniversary.present? &&
                        @region.stamp.present? && @region.neighbourhood.present?

    true
  end

  def months_valid?
    @region.months.find_each { |month|  return false unless month.advertiser_id }
    true
  end
end
