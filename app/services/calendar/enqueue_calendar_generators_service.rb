Calendar::EnqueueCalendarGeneratorsService = Struct.new(:params) do
  def call
    add_errors_to_report
    update_batch_id
    generate_calendars
  end

  private

  def generate_calendars
    batch.jobs do
      records.map do |mover|
        CalendarGenerateWorker.perform_async(mover.to_h, report.id, params[:headless_chrome] || false)
      end
    end
  end

  def update_batch_id
    Report::Movers::BatchIdUpdate.new(report: report, batch_id: batch.bid).call
  end

  def add_errors_to_report
    Report::CompleteParserErrorsService.new(report, wrong_records).call
  end

  def batch
    @batch ||= Sidekiq::Batch.new.tap do |obj|
      obj.description = "Batch for #{file_name}"
      obj.on(:complete, 'Calendar::GenerateCallbackService', { report_id: report.id })
    end
  end

  def report
    @report ||= ActiveRecord::Base.transaction do
      ::Report::Movers::Create.new(file_name: file_name, parsed_count: records.count).call
    end
  end

  def records
    @records ||= parsed_data.parsed
  end

  def wrong_records
    @wrong_records ||= parsed_data.wrong_records
  end

  def parsed_data
    @parser_result ||= Parser::MoversService.new(file).tap(&:call)
  end

  def file_name
    @file_name ||= file.original_filename
  end

  def file
    @file ||= params[:file].tap do |value|
      raise Calendar::FileNotFound unless value.present?
    end
  end
end
