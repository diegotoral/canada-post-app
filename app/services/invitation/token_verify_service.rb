class Invitation::TokenVerifyService
  attr_accessor :invitation

  def initialize(token:)
    @token = token
  end

  def call
    @invitation = ::Invitation.find_by!(token: @token)
  end
end
