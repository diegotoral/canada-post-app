class Report::MoversError < ApplicationRecord
  self.table_name_prefix = 'report_'

  belongs_to :report_movers, class_name: Report::Movers

  enum error_type: %w(record_invalid location_invalid map_generation_error)

  scope :record_invalid, -> { where(error_type: Report::MoversError.error_types[:record_invalid]) }
  scope :locations, -> { where(error_type: Report::MoversError.error_types[:location_invalid]) }
  scope :map_generation, -> { where(error_type: Report::MoversError.error_types[:map_generation_error]) }
  scope :created_at_month, -> (date) { where("created_at > ? AND created_at < ?", date.beginning_of_month, date.end_of_month) }
  scope :group_weekly, -> { group_by_week(:created_at, format: '%m/%d/%Y').count  }
  scope :group_monthly, -> { group_by_month(:created_at, format: '%m/%d/%Y').count  }

end
