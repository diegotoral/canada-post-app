require 'csv'

class Report::Movers < ApplicationRecord
  self.table_name_prefix = 'report_'

  enum status: %w(pending in_progress completed canceling canceled zip_generating)

  has_many :calendars, class_name: Report::MoversCalendar, foreign_key: 'report_movers_id'
  has_many :wrong_records, class_name: Report::MoversError, foreign_key: 'report_movers_id', dependent: :destroy

  scope :created_at_desc, -> { order(created_at: :desc) }
  scope :not_finished, -> { where(status: not_finished_statuses)  }
  scope :canceled_with_calendars, -> { where(status: Report::Movers.statuses[:canceled]).joins(:calendars) }
  scope :between_date, -> (start_date, end_date) { where('created_at BETWEEN ? AND ?', start_date.to_date, end_date.to_date.end_of_day) }
  scope :completed_last, -> (number) { order(created_at: :desc).where(status: Report::Movers.statuses[:completed]).first(number) }

  def working?
    working_statuses = %w(pending in_progress)
    working_statuses.include?(status)
  end

  private

  def self.not_finished_statuses
    not_finished_statuses = %w(pending in_progress canceling)
    not_finished_statuses.map { |status| Report::Movers.statuses[status] }
  end
end
