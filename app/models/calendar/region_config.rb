class Calendar::RegionConfig < ApplicationRecord
  self.table_name_prefix = 'calendar_'

  mount_uploader :front, ::CalendarImageUploader
  mount_uploader :signature, ::CalendarImageUploader
  mount_uploader :front_left, ::CalendarImageUploader
  mount_uploader :last_page, ::CalendarImageUploader
  mount_uploader :anniversary, ::CalendarImageUploader
  mount_uploader :neighbourhood, ::CalendarImageUploader
  mount_uploader :stamp, ::CalendarImageUploader

  mount_uploader :fr_stamp, ::CalendarImageUploader
  mount_uploader :fr_front, ::CalendarImageUploader
  mount_uploader :fr_front_left, ::CalendarImageUploader
  mount_uploader :fr_last_page, ::CalendarImageUploader
  mount_uploader :fr_anniversary, ::CalendarImageUploader
  mount_uploader :fr_neighbourhood, ::CalendarImageUploader

  mount_uploader :calendar, ::CalendarUploader

  has_many :months, class_name: Calendar::Month, dependent: :destroy
  has_many :advertisers, -> { distinct }, :through => :months
  has_many :waste_disposal_zones
  has_many :waste_pickups, through: :waste_disposal_zones
  has_many :holidays
  has_many :locations
  has_many :postal_code_covers

  scope :region_desc, -> { order(region: :desc)  }
  scope :with_wrong_status, -> { where(status: wrong_statuses)  }

  enum status: %w(complete to_refresh pending in_progress region_config_invalid advertisers_invalid)

  def advert_map_locations(mover)
    advertisers_locations(mover) + advert_pois_locations(mover)
  end

  def self.find_by_region_code!(code)
    find_by!("'#{code}' = any (codes)")
  end

  def cover_page_for(code)
    find_postal_code_covers_by_code(code) || self
  end

  def find_postal_code_covers_by_code(code)
    postal_code_covers.find_by("'#{code}' = any (codes)")
  end

  private

  def advert_pois_locations(mover)
    Calendar::Location.nearest_advert_pois(self, mover.location)
  end

  def advertisers_locations(mover)
    advertisers.visible.uniq.map{ |advertiser| advertiser.to_advert_map(mover) }
  end

  def self.wrong_statuses
    wrong_statuses_array = %w(region_config_invalid advertisers_invalid)
    wrong_statuses_array.map { |status| Calendar::RegionConfig.statuses[status] }
  end
end
