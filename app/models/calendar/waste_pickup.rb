class Calendar::WastePickup < ActiveRecord::Base
  self.table_name_prefix = 'calendar_'

  belongs_to :waste_disposal_zone

  scope :year_month, -> (year,month) { where('extract(year from date) = ? AND extract(month from date) = ?', year, month)  }
  scope :day, -> (day) { where('extract(day from date) = ?', day) }
  scope :from_year, -> (year) { where('extract(year from date) = ?', year) }
  scope :from_to, -> (from, to) { where('date::date > ? AND date::date < ?', from, to) }
  scope :week_day, -> (week_day_number) { where('extract(dow from date) = ?', week_day_number) }
  scope :find_date, -> (date) { where('date::date = ?', date) }
  scope :newer_than_month, -> (date) { where('date >= ?', date.beginning_of_month) }

  WASTE_TYPES = %w(garbage green_bin recycling christmas_tree yard_waste)
  ALL_WASTE_TYPES = WASTE_TYPES + %w(organic)

  def waste_list
    WASTE_TYPES.map do |waste_type|
      waste_type if self.send(waste_type)
    end.compact
  end
end
