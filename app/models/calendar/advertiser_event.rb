class Calendar::AdvertiserEvent < ActiveRecord::Base
  self.table_name_prefix = 'calendar_'

  belongs_to :advertiser
  ALLOWED_COLORS  = ['#ed1b2d',
                     '#ee3123',
                     '#004990',
                     '#006b64',
                     '#00529b',
                     '#216db2',
                     '#DA291C',
                     '#104b8e',
                     '#0065a4',
                     '#000000',
                     '#482769',
                     '#E11523',
                     '#DF2382',
                     '#702983',
                     '#324697',
                     '#398BCB',
                     '#1D9A37',
                     '#D1D70A',
                     '#A85B15',
                     '#E44918']

  def self.allowed_colors_mapping
    Hash[ *ALLOWED_COLORS.collect { |v| [ v, v ] }.flatten ]
  end

end
