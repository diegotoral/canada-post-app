class Calendar::Location < ApplicationRecord
  self.table_name_prefix = 'calendar_'

  belongs_to :advertiser, required: false
  belongs_to :region_config, required: false

  enum category:  %w(advertisers canada_posts hospitals fire_stations polices libraries recreation_centres gas_station)
  POI_CATEGORIES = %w(hospitals fire_stations polices libraries recreation_centres)
  ADVERTMAP_POI_CATEGORIES = %w(gas_station)

  scope :without_advertiser, -> { where(advertiser_id: nil)  }
  scope :tiny_maps, -> { where(tiny_map: true) }

  geocoded_by :postal_code

  def to_advert_map
    { name: name,
      website: website,
      phone: phone,
      nearest: [self],
      category: '' }
  end

  def self.nearest_advert_pois(region, mover_location)
    ADVERTMAP_POI_CATEGORIES.flat_map do |category|
      region.locations.send(category.to_sym).near(mover_location, 5).first(2).map(&:to_advert_map)
    end
  end

  def self.nearest_post(mover_location)
    Calendar::Location.without_advertiser.canada_posts.near(mover_location, 5).first
  end

  def self.nearest_pois(region, mover_location)
    %w(hospitals fire_stations polices).flat_map do |category|
      region.locations.send(category.to_sym).near(mover_location, 25).first
    end
  end

  def self.nearest_small_pois(region, mover_location)
    %w(libraries recreation_centres).map do |category|
      region.locations.send(category.to_sym).near(mover_location, 5).first(3)
    end
  end
end
