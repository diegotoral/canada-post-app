class Calendar::Holiday < ApplicationRecord
  self.table_name_prefix = 'calendar_'

  belongs_to :region_config

end
