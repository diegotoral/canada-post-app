class Calendar::WasteDisposalZone < ApplicationRecord
  belongs_to :region_config
  has_many :waste_pickups

  WasteDisposalError = Class.new(StandardError)
end
