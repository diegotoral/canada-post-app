class Calendar::Month < ApplicationRecord
  self.table_name_prefix = 'calendar_'

  belongs_to :region_config
  belongs_to :advertiser, class_name: Calendar::Advertiser, required: false

  scope :this_year_months_from, -> (date) { where("month_number >= #{date.month}") }
  scope :next_year_months_from, -> (date) { where("month_number < #{date.month}") }
end
