class Calendar::PostalCodeCover < ApplicationRecord
  self.table_name_prefix = 'calendar_'

  mount_uploader :front, ::CalendarImageUploader
  mount_uploader :fr_front, ::CalendarImageUploader

  belongs_to :region_config
end
