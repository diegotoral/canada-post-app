class Calendar::Advertiser < ApplicationRecord
  self.table_name_prefix = 'calendar_'

  mount_uploader :cover_advert, ::CalendarImageUploader
  mount_uploader :left_advert_1, ::CalendarImageUploader
  mount_uploader :left_advert_2, ::CalendarImageUploader
  mount_uploader :left_advert_3, ::CalendarImageUploader
  mount_uploader :right_advert, ::CalendarImageUploader
  mount_uploader :logo, ::CalendarImageUploader
  mount_uploader :tiny_address_logo, ::CalendarImageUploader

  mount_uploader :fr_cover_advert, ::CalendarImageUploader
  mount_uploader :fr_left_advert_1, ::CalendarImageUploader
  mount_uploader :fr_left_advert_2, ::CalendarImageUploader
  mount_uploader :fr_left_advert_3, ::CalendarImageUploader


  enum grid: [ 'whole_page', '1:1:1', '1:2', '2:1' ]

  has_many :months
  has_many :region_configs, -> { distinct }, through: :months
  has_many :events, class_name: Calendar::AdvertiserEvent
  has_many :locations

  scope :visible, -> { where(hide_on_map: false)  }

  def to_advert_map(mover)
    { name: name,
      website: website,
      phone: phone,
      nearest: nearest_locs(mover.location),
      category: '' }
  end

  def nearest_locs(mover_location, tiny_map = false)
    scoped_locations = tiny_map ? locations.tiny_maps : locations
    scoped_locations.pluck(:name).uniq.map do |loc_name|
      scoped_locations.where(name: loc_name).near(mover_location, ADVERT_DISTANCE).first
    end.compact.sort_by {|location| location.distance}
  end

  def grid_value
    Calendar::Advertiser.grids[grid]
  end

  def complete?
    cover_advert && left_advert_1
  end
end
