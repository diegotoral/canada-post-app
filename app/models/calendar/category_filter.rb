class Calendar::CategoryFilter < ApplicationRecord
  self.table_name_prefix = 'calendar_'

  belongs_to :region_config

  enum category:  %w(hospital fire_station police library)

end
