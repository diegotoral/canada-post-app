class Admin < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :recoverable, :rememberable, :trackable, :validatable

  def send_devise_notification(notification, *args)
    message = devise_mailer.send(notification, self, *args)
    message.deliver_later
  end
end
