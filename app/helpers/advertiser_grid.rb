module AdvertiserGrid
  def self.left_advert_1_image_style(grid)
    return 'one_third' if grid == '1:1:1' || grid == '1:2'
    return 'whole_page' if grid == 'whole_page'
    return 'two_third' if grid == '2:1'
    'whole_page'
  end

  def self.left_advert_2_image_style(grid)
    return 'one_third' if grid == '1:1:1' || grid == '2:1'
    return 'two_third' if grid == '1:2'
    'none'
  end

  def self.left_advert_3_image_style(grid)
    return 'one_third' if grid == '1:1:1'
    'none'
  end
end
