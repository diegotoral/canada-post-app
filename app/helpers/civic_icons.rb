module CivicIcons
  def self.icon_path(name)
    "/assets/pushpins/small_pois/#{name}.svg"
  end
end
