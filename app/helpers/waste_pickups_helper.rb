module WastePickupsHelper
  def waste_check_box(form, type, day_number, pattern, week = "")
    form.check_box(type, id: waste_pickup_id(type, day_number, pattern, week), class: 'icon-checkbox')
  end

  def waste_label(type, day_number, pattern, week = "")
    label_tag(waste_pickup_id(type, day_number, pattern, week)) do
      concat content_tag(:div, '', class: [type.to_s, 'icon', 'inactive', 'unchecked'].join(' '))
      concat content_tag(:div, '', class: [type.to_s, 'icon', 'checked'].join(' '))
    end
  end

  private

  def waste_pickup_id(type, day_number, pattern, week = "")
    ['checkbox', day_number, week, ('pattern' if pattern), type.to_s].compact.join('_')
  end
end
