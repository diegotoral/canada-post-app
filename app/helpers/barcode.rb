module Barcode
  require 'barby'
  require 'barby/barcode/code_128'
  require 'barby/outputter/html_outputter'
  TOTAL_PAGES = 8

  def self.create(page, record_id)
    barcode = Barby::Code128.new("#{sprintf '%02d', page}#{sprintf '%02d', TOTAL_PAGES}#{record_id}")
    Barby::HtmlOutputter.new(barcode)
  end
end
