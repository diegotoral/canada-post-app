module PdfHelper
  def self.asset_for_pdf(asset_url)
    "#{PHANTOMJS_BASE_URL}#{asset_url}"
  end

  def self.js_script_file_path(js_path)
    File.read("#{Rails.root}/app/assets/javascripts/calendar/#{js_path}")
  end

  def self.year_scope_for(events, year)
    events.select { |event| event.date.year == year.to_i }
  end

  def self.translate_field(obj, field_name, french)
    return obj.send(field_name.to_sym) unless french
    obj.send("fr_#{field_name}".to_sym).present? ? obj.send("fr_#{field_name}".to_sym) : obj.send(field_name.to_sym)
  end

  def self.month_scope_for(events, month)
    events.select { |event| event.date.month == month.to_i }
  end

  def self.day_scope_for(events, day)
    events.select { |event| event.date.day == day.to_i }
  end

  def self.translate_date(date, locale)
    if locale == :fr
      "#{PdfHelper.french_ordinarie(date.day)} #{I18n.l(date, format: :month)} #{date.year}"
    else
      date.strftime("%B #{date.day.ordinalize}, %Y")
    end
  end

  def self.french_ordinarie(day)
    return "#{day}<sup>er</sup>" if day == 1
    # return "#{day}<sup>nd</sup>" if day == 2
    # "#{day}<sup>ème</sup>"
    "#{day}"
  end

  def self.js_language(locale)
    return 'fr' if locale == :fr
    'en'
  end

  private

  def self.events_with_previous_year_wastepickups(events, year)
    events.select { |event| event.date.year == year.to_i || (event.date.year == year.to_i - 1 && event.type = 'waste')}
  end
end
