class MoverStructure
  include Virtus.model
  include ActiveModel::Validations

  attribute :record_id, Integer
  attribute :start_date, Date
  attribute :language, String
  attribute :first_name, String
  attribute :last_name, String
  attribute :address_line_1, String
  attribute :address_line_2, String
  attribute :city, String
  attribute :province, String
  attribute :postal_code, String
  attribute :multi_unit_building, String
  attribute :kit_no, String
  attribute :lat, Float
  attribute :long, Float
  attribute :bagbun, String
  attribute :dmc, String
  attribute :list_order, String
  attribute :waste_pickup_url, String
  attribute :zones, Array, coerce: false, default: nil

  validates :record_id, :last_name, :address_line_1, :city,
            :province, :postal_code, presence: true
  validates :record_id, length: { maximum: 10 }
  validates :first_name, :city, length: { maximum: 30 }
  validates :last_name, :address_line_1, :address_line_2, length: { maximum: 40 }
  validates :province, length: { is: 2 }
  validates :postal_code, length: { is: 7 }
  validates :language, inclusion: { in: %w(E F) }
  validates :multi_unit_building, inclusion: { in: %w(N Y) }
  validate  :date_format

  def call
    sanitize_addres_line
    format_postal_code
    validate!
    self
  end

  def french?
    language == 'F'
  end

  def anniversary_date
    1.year.since(start_date)
  end

  def location
    [lat, long]
  end

  def address
    [address_line_1, province, postal_code].compact.join(' ')
  end

  def geocode_address
    if address_line_1.scan(/po box/i).present? || address_line_1.scan(/\ACP \d+/i).present?
      postal_code
    else
      address
    end
  end

  def near
    [city, province].compact.join(',')
  end

  def geocode
    coordinates = Calendar::Locations::GeocodeService.new(geocode_address).call
    self.lat ||= coordinates.first
    self.long ||= coordinates.last
  end

  def find_zones
    @zones ||= Calendar::WasteDisposalZone::CoordinatesMatcher.new(lat, long, fetch_region).call
  end

  private

  def fetch_region
    ::Calendar::RegionConfig.find_by_region_code!(postal_code.first(3))
  end

  def sanitize_addres_line
    self.address_line_1 = self.address_line_1.gsub(/,|\./, '') if address_line_1.present?
  end

  def format_postal_code
    self.postal_code = Calendar::PostalCodeFormatService.new(postal_code).call if postal_code.present?
  end

  def date_format
    errors.add(:start_date, 'has wrong format') and raise(WrongDateFormat) unless start_date.is_a?(Date)
  end
end
