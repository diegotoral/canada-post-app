class CalendarEventStructure

  include Virtus.model
  include ActiveModel::Validations

  attribute :name, String
  attribute :fr_name, String
  attribute :type, String
  attribute :date, Date
  attribute :event_color, String

  validate  :date_format

  TYPES = %w(moving_date anniversary_moving_date waste advertiser_event)

  def color
    event_color || Calendar::AdvertiserEvent::ALLOWED_COLORS.first
  end

  def call
    validate!
    self
  end

  protected

  def date_format
    if date.class != Date
      raise WrongDateFormat
    end
  end
end
