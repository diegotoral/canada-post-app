class Report::MoversPresenter < SimpleDelegator

  def date
    self.created_at.strftime('%d/%m/%Y')
  end

  def total_records
    self.parsed_count + invalid_records.count
  end

  def invalid_records
    self.wrong_records.record_invalid
  end

  def invalid_locations
    self.wrong_records.locations
  end

  def map_generation
    self.wrong_records.map_generation
  end

  def calendars_presenter
    calendars.map{|calendar| Calendar::DefaultPresenter.new(calendar)}
  end
end
