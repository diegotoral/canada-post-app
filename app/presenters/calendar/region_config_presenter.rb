class Calendar::RegionConfigPresenter < SimpleDelegator

  def sorted_months
    self.months.order('month_number')
  end

  def example_calendar
    calendar.url
  end
end
