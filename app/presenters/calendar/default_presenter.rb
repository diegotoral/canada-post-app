class Calendar::DefaultPresenter < SimpleDelegator

  def calendar_url(calendar_path)
    "#{PHANTOMJS_BASE_URL}#{calendar_path}".gsub('/public', '')
  end

  def calendar_folder
    path.split('/').first(3).join('/')[1..-1]
  end
end
