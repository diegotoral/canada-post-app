namespace :calendars do
  desc 'Updates numbers for calendars'
  task :fetch_numbers => :environment do |t, args|
    Report::MoversCalendar.all.each do |calendar|
      calendar.update(number: calendar.path.split("/").last.split("-").first.split("_").first)
    end
  end
end
