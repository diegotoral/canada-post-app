class TorontoParser
  require 'csv'

  CONVERT_HEADERS = { 'Calendar' => 'calendar', 'WeekStarting' => 'week_starting',
                      'GreenBin' => 'green_bin', 'Garbage' => 'garbage', 'Recycling' => 'recycling',
                      'YardWaste' => 'yard_waste', ' ChristmasTree' => 'christmas_tree' }
  WASTE_TYPES = %w(green_bin garbage recycling yard_waste chirstmas_tree)

  def initialize(file)
    @csv = load_csv_file(file)
    @region = Calendar::RegionConfig.find_by(region: 'Toronto')
  end

  def call
    validate_file!
    generate_schedule
  end

  private

  def hook_file
    Rack::Test::UploadedFile.new(Rails.root.join('vendor', 'waste_pickups', 'toronto.csv'), "text/csv")
  end

  def generate_schedule
    @csv.each do |row|
      # pickup_date = parse_pickup_date(row['week_starting'], row['calendar'])
      pickup_date = Date.strptime(row['week_starting'], "%m/%d/%y")
      wastes = row.to_hash.select { |key,value| WASTE_TYPES.include?(key) && value != '0' }
      wastes.each{ |key,value| wastes[key] = true}
      Calendar::WastePickup.new(wastes.merge({date: pickup_date, region_config: @region})).save
    end
  end

  def parse_pickup_date(week_date, calendar)
    Date.strptime(week_date, "%m/%d/%y") + week_day(calendar).days
  end

  def week_day(day)
    case day
    when /Monday/
      0
    when /Tuesday/
      1
    when /Wednesday/
      2
    when /Thursday/
      3
    when /Friday/
      4
    end
  end

  def load_csv_file(csv_file)
    CSV.read(csv_file.path, headers: true, skip_blanks: true, col_sep: ',', header_converters: lambda { |name| CONVERT_HEADERS[name] })
  end

  def validate_file!
    if @csv.count == 0
      raise Parser::EmptyCsvException
    elsif (@csv.first.to_hash.keys.compact).size != 7
      raise Parser::WrongFormatedCsvException
    end
  end
end
