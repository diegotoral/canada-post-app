namespace :parse_waste_pickups do
  require "#{Rails.root}/lib/tasks/parse_waste_pickups/toronto_parser"
  desc 'Load waste pickups'
  task :toronto => :environment do |t, args|
    file_path = "#{Rails.root}/vendor/waste_pickups/toronto.csv"
    puts File.open(file_path)
    TorontoParser.new(File.open(file_path)).call
  end
end
