require 'pp'
require "#{Rails.root}/lib/tasks/montreal/fetch_waste_pickups"

desc 'test'
namespace :montreal do
  task :parse_waste_pickups => :environment do |t, args|
    log = Logger.new(Rails.root.join('log', 'raketask_waste_pickups_import.log'))
    result = {organic: [], recycling: [], green_bin: [], yard_waste: [], garbage: []}.with_indifferent_access
    all_waste_types = %w(organic recycling green_bin garbage)
    wdays = %w(DIMANCHE LUNDI MARDI MERCREDI JEUDI VENDREDI SAMEDI)
    french_month_names = %w(blank Janvier Février Mars Avril Mai Juin Juillet Août Septembre Octobre Novembre D)
    YEAR = 2018

    all_waste_types.each do |waste_type|
      waste_hash = JSON.parse(File.read("#{Rails.root}/vendor/zones/montreal/#{waste_type}.geojson"))
      log.info "parsed file #{Rails.root}/vendor/zones/montreal/#{waste_type}.geojson"
      waste_hash['features'].each do |feature|
        log.debug " -- parsing feature"
        properties = feature['properties']
        zone = "#{properties['NO_ZONE']}-#{waste_type}"
        log.debug " ---- #{zone}"
        if properties['MESSAGE_FR'].scan(/Pour les/i).count >= 2
          message = properties['MESSAGE_FR'].split('Pour les')[1]
        else
          message = properties['MESSAGE_FR'].split('Pour les')[0]
        end
        next if message.nil?
        log.debug " ---- #{message}"
        days = message.scan(/"#{wdays.join('|')}"/i).uniq.join(',').upcase
        from_date = nil
        to_date = nil
        related_to = nil
        if message.scan(/du \d* \w* au/i).present?
           from_date = message.scan(/( du )(\d* \w*)( au )(\d* \w*)/i)[0][1].strip
           to_date = message.scan(/( du )(\d* \w*)( au )(\d* \w*)/i)[0][3].strip
           french_month_names.each_with_index do |month_name, index|
             from_date = from_date.gsub(/#{month_name}/i, index.to_s)
             to_date = to_date.gsub(/#{month_name}/i, index.to_s)
           end
           from_date = Date.strptime("#{from_date.match(/\d* \d*/)} #{YEAR}", '%d %m %Y')
           to_date = Date.strptime("#{to_date.match(/\d* \d*/)} #{YEAR}", '%d %m %Y')
        end
        if days.empty?
          if message.scan(/organiques/i).present?
            if organic_zone = result[:organic].find {|entry| entry['NO_ZONE'] == "#{zone.split("-")[0]}-organic"}
              days = organic_zone["WDAY"]
            else
              related_to = 'organic'
            end
          elsif message.scan(/recyclables/i).present?
            if organic_zone = result[:recycling].find {|entry| entry['NO_ZONE'] == "#{zone.split("-")[0]}-recycling"}
              days = organic_zone["WDAY"]
            else
              related_to = 'recycling'
            end
          end
        end
        log.debug " ---- #{days}"
        log.debug " ---- #{related_to}"
        result[waste_type] << {"MESSAGE" => properties["MESSAGE"], "NO_ZONE" => zone, "RELATED_TO" => related_to, "WDAY" => days, "from_date" => from_date, "to_date" => to_date}
      end
    end


    all = {}
    all_waste_types.each do |waste_type|
      log.info "filtering and merging #{waste_type}"
      result[waste_type].each do |entry|
        name = entry['NO_ZONE']
        if entry['WDAY'].present?
          wds = entry['WDAY'].split(',')
          all[name] ||= {'waste_pickups' => {}}
          wdays.each_with_index do |wday, i|
            next unless wds.include?(wday)
            all[name]['waste_pickups'].deep_merge!(i.to_s => { waste_type => { 'from_date' => entry['from_date'], 'to_date' => entry['to_date'] } })
          end
        end
      end
    end

    # related to different zones
    all_waste_types.each do |waste_type|
      log.info "no zone pickups #{waste_type}"
      result[waste_type].each do |entry|
        if entry['RELATED_TO']
          name = entry['NO_ZONE']
          unless all[name].present?
            all[name] = {related_to: entry['RELATED_TO']}
          end
        end
      end
    end

    all.map do |name, attrs|
      begin
        log.info "saving #{name}"
        zone = Calendar::WasteDisposalZone.find_by!(name: name)
        log.debug " -- zone_id #{zone.id}"
        log.debug " -- attrs #{attrs}"
        if attrs[:related_to].present?
          zone.update(related_to: attrs[:related_to], date_from: attrs[:date_from], date_to: attrs[:date_to])
        else
          s = FetchWastePickups.new(zone, { year: '2017', waste_pickups: attrs['waste_pickups'], dont_validate_region: true })
          s.call
          s = FetchWastePickups.new(zone, { year: '2018', waste_pickups: attrs['waste_pickups'], dont_validate_region: true })
          s.call
        end
      rescue => e
        log.error e.message
        log.error e.backtrace
      end
    end
  end
end
