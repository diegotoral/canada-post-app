FetchWastePickups = Struct.new(:zone, :options) do
  def call
    persist
    validate_region unless options[:dont_validate_region]
  end

  private

  def persist
    waste_pickups.each do |wday, waste_types|
      all_days_by_week_day[(wday.to_i)].each do |day|
        if fetch_waste_types(waste_types, day)
          Calendar::WastePickup::UpdateDayInfo.new(fetch_waste_types(waste_types, day).merge(zone: zone, date: day)).call
        end
      end
    end
  end

  def fetch_waste_types(waste_types, date)
    wastes = {}
    waste_types.each do |wtype, attrs|
      if attrs['from_date'] == nil || attrs['to_date'] == nil
        wastes[wtype] = true
      elsif (attrs['from_date']..attrs['to_date']).include?(date)
        wastes[wtype] = true
      end
    end
    wastes
  end

  def validate_region
    Calendar::RegionConfigValidationStatusService.new(zone.region_config).call
  end

  def all_days_by_week_day
    @all_days ||= (year..year.end_of_year).group_by(&:wday)
  end

  def year
    @year ||= Date.new(options[:year].to_i)
  end

  def waste_pickups
    options[:waste_pickups]
  end
end
