require 'pp'
desc 'test'
namespace :montreal do
  task :parse_zones => :environment do |t, args|
    montreal = Calendar::RegionConfig.find_by(region: 'Montreal')
    montreal.waste_disposal_zones.each { |zone| zone.waste_pickups.destroy_all }
    montreal.waste_disposal_zones.destroy_all

    %w(organic yard_waste green_bin garbage recycling).each do |waste_type|
      json = JSON.parse(File.read("#{Rails.root}/vendor/zones/montreal/#{waste_type}.geojson"))

      json['features'].each do |zone|
        name = zone['properties']['NO_ZONE']
        coordinates = if zone['geometry']['type'] == 'Polygon'
          zone['geometry']['coordinates']
        else
          zone['geometry']['coordinates'].first
        end

        montreal.waste_disposal_zones.find_or_create_by(name: "#{name}-#{waste_type}").tap do |zone|
          zone.update(coordinates: coordinates)
          if Calendar::WastePickup::ALL_WASTE_TYPES.include?(waste_type)
            zone.update(waste_types: (zone.waste_types << waste_type))
          end
        end
      end
    end
  end
end
