namespace :holidays do
  desc 'Generates holidays for region configs'
  task :generate => :environment do |t, args|
    file = File.open("#{Rails.root}/lib/tasks/holidays/holiday.csv")
    holidays_csv = CSV.read(file, headers: true, skip_blanks: true, col_sep: ";", encoding: Parser::EncodeDetectService.new(file).call )
    holidays_csv.each do |row|
      row['regions'].split(',').each do |region_name|
        region = Calendar::RegionConfig.find_by(region: region_name.strip)
        if region
          holiday = Calendar::Holiday.find_or_create_by(region_config: region, name: row['name'], date: DateTime.strptime(row['date'], '%e-%b-%y'))
          holiday.update(fr_name: row['fr_name']) if holiday.fr_name != row['fr_name']
        else
          puts "#{region_name} not found"
        end
      end
    end
  end
end
