namespace :calendar do
  desc 'Export zones to JSON files'
  task :export_zones, %i[region_id] => :environment do |t, args|
    region = Calendar::RegionConfig.find(args[:region_id].to_i)

    dir = Rails.root.join('tmp', args[:region_id]).to_path
    FileUtils.mkdir_p(dir) unless File.directory?(dir)

    region.waste_disposal_zones.each do |zone|
      File.open(Rails.root.join('tmp', args[:region_id], "#{zone.name}.json"), 'w') do |f|
        f.write zone.waste_pickups.to_json(except: %i[id created_at updated_at])
      end
    end
  end

  desc 'Import zones form JSON files'
  task :import_zones, %i[region_id] => :environment do |t, args|
    region = Calendar::RegionConfig.find(args[:region_id].to_i)

    Dir.glob(Rails.root.join('tmp', region_id, '*.json')).map do |path|
      zone_name = path.split('/').last.split('.').first
      entities = JSON.parse(File.read(path))
      zone = region.waste_disposal_zones.find_by_name(zone_name)
      next if zone.nil?
      puts ">> #{zone_name} #{zone.id} : #{entities.count} : #{entities.last}"
      entities.each do |entity|
        zone.create_waste_pickup(entity)
      end
    end
  end

  desc 'Remove waste pickups duplicates'
  task remove_duplicates: :environment do |t, args|
    Calendar::WastePickup.all.group_by { |m| [m.date, m.green_bin, m.garbage, m.recycling, m.yard_waste, m.christmas_tree, m.waste_disposal_zone_id] }.values.each do |duplicates|
      print '.'
      duplicates.shift
      duplicates.each(&:destroy)
    end
  end

  desc 'Extend schedule'
  task :extend_schedule, %i[region_id from_year to_year] => :environment do |t, args|
    region = Calendar::RegionConfig.find(args[:region_id].to_i)
    from_year = args[:from_year].to_i
    to_year   = args[:to_year].to_i

    region.waste_pickups.where(date: Date.new(from_year, 1, 1)..Date.new(from_year, 12, 31)).each do |wp|
      wp.dup.tap do |obj|
        # change year to target year
        target_date = obj.date.change(year: to_year)
        # offset is to keep the schedule in the same week days pattern
        offset = obj.date.wday - target_date.wday
        obj.date = target_date + offset
      end.save!
    end
  end

  desc 'Clear schedule'
  task :clear_schedule, %i[region_id] => :environment do |t, args|
    region = Calendar::RegionConfig.find(args[:region_id].to_i)
    region.waste_pickups.each(&:destroy)
  end

  desc 'Merge waste pickups duplicates'
  task merge_duplicates: :environment do |t, args|
    Calendar::WastePickup.all.group_by { |m| [m.date, m.waste_disposal_zone_id] }.values.each do |duplicates|
      print '.'
      garbage = duplicates.any?(&:garbage)
      green_bin = duplicates.any?(&:green_bin)
      recycling = duplicates.any?(&:recycling)
      yard_waste = duplicates.any?(&:yard_waste)
      christmas_tree = duplicates.any?(&:christmas_tree)
      first = duplicates.shift
      duplicates.each(&:destroy)
      first.update(garbage: garbage, green_bin: green_bin, recycling: recycling, yard_waste: yard_waste, christmas_tree: christmas_tree)
    end
  end
end
