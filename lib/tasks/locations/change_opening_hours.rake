namespace :locations do
  desc 'Change opening hours for locations'
  task :change_opening_hours => :environment do |t, args|
    Calendar::Location.all.each do |location|
      if location.opening_hours_first_line || location.opening_hours_second_line || location.opening_hours_third_line || location.fr_opening_hours_first_line || location.fr_opening_hours_second_line || location.fr_opening_hours_third_line
        first_line = "Mon-Fri #{location.opening_hours_first_line}" if location.opening_hours_first_line
        second_line = "Sat #{location.opening_hours_second_line}" if location.opening_hours_second_line
        third_line = "Sun #{location.opening_hours_third_line}" if location.opening_hours_third_line

        fr_first_line = "Lun - ven #{location.fr_opening_hours_first_line}" if location.fr_opening_hours_first_line
        fr_second_line = "Sam #{location.fr_opening_hours_second_line}" if location.fr_opening_hours_second_line
        fr_third_line = "Dim #{location.fr_opening_hours_third_line}" if location.fr_opening_hours_third_line

        location.update(
          opening_hours_first_line: first_line,
          opening_hours_second_line: second_line,
          opening_hours_third_line: third_line,
          fr_opening_hours_first_line: fr_first_line,
          fr_opening_hours_second_line: fr_second_line,
          fr_opening_hours_third_line: fr_third_line
        )
      end
    end
  end
end
