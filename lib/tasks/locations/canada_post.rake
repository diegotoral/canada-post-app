namespace :locations do
  desc 'Create canada post locations'
  task :canada_post => :environment do |t, args|
    file_path = "#{Rails.root}/vendor/locations/canada_post_location.csv"
    puts "Deleting all canada posts locations..."
    Calendar::Location.without_advertiser.canada_posts.destroy_all
    puts "Creating new locations..."
    Parser::CreateLocationsService.new(file: File.open(file_path), category: Calendar::Location.categories['canada_posts']).call
    Calendar::Location.canada_posts.each { |cp| cp.update(name: cp.name) }
    puts "Generated #{Calendar::Location.without_advertiser.count} locations"
  end

  desc 'Format postal codes for locations'
  task :format_postal_codes => :environment do |t,args|
    Calendar::Location.all.each do |location|
      puts location.id
      location.update(postal_code: Calendar::PostalCodeFormatService.new(location.postal_code).call)
    end
  end
end
