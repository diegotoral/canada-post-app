desc 'Unflatten the zones so they can contain multiple polygons'
task wrap_waste_disposal_zones: :environment do |t, args|
  Calendar::WasteDisposalZone.find_each do |zone|
    zone.coordinates = [zone.coordinates]
    zone.save!
  end
end
