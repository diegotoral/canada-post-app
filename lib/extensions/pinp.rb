module PINP
  Polygon = Struct.new(:points) do
    def include?(point)
      edges.count { |e| e.intersect?(point) }.odd?
    end

    def edges
      @edges ||= [].tap do |result|
        size.times do |i|
          result.push(Edge.new(*points[0..1]))
          points.rotate!
        end
      end
    end

    def size
      @size ||= points.size
    end
  end

  Point = Struct.new(:a, :b) do
    def x; a.to_f; end
    def y; b.to_f; end
  end

  Edge = Struct.new(:start_point, :end_point) do
    def intersect?(point)
      within_range?(point) && intersect_with_line?(point)
    end

    private

    def intersect_with_line?(point)
      return false if run.zero?
      point.y < rise / run * (point.x - start_point.x) + start_point.y
    end

    def within_range?(point)
      point.x <= to_a.map(&:x).max && point.x > to_a.map(&:x).min
    end

    def rise
      end_point.y - start_point.y
    end

    def run
      end_point.x - start_point.x
    end
  end
end
