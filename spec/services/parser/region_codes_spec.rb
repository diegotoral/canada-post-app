require 'rails_helper'

RSpec.describe Parser::RegionCodesService do
  let(:test_file) { Rack::Test::UploadedFile.new(Rails.root.join('spec', 'support', 'parser', 'toronto_codes.txt'), "text/txt") }

  describe 'call' do
    it 'should respond with array of codes' do
      expect(Parser::RegionCodesService.new(test_file).call).to eq(%w(M4C M4E M4G))
    end
  end
end
