require 'rails_helper'

RSpec.describe Parser::MoversService do
  let(:test_file) { Rack::Test::UploadedFile.new(Rails.root.join('spec', 'support', 'parser', 'valid_movers_data.txt'), "text/txt") }
  let(:wrong_headers_test_file) { Rack::Test::UploadedFile.new(Rails.root.join('spec', 'support', 'parser', 'invalid_movers_data.txt'), "text/txt") }
  let(:invalid_record_file) { Rack::Test::UploadedFile.new(Rails.root.join('spec', 'support', 'parser', 'invalid_record.txt'), "text/txt") }
  let(:all_invalid) { Rack::Test::UploadedFile.new(Rails.root.join('spec', 'support', 'parser', 'all_records_invalid.txt'), "text/txt") }
  before(:each) do
    allow(Geocoder).to receive(:coordinates).and_return([1,1])
  end

  describe 'call' do
    context 'file is correct' do
      before(:each) do
        @parser = Parser::MoversService.new(test_file)
        @parser.call
      end

      it { expect(@parser.parsed.count).to eq(5) }
      it { expect(@parser.parsed.last.first_name).to eq("VÉRONIQUE") }
      it 'should parse multiple names' do
        expect(@parser.parsed.first.first_name).to eq("DOROTHY DOROTHY")
      end

      it 'should raise wrong headers exception' do
        expect{ Parser::MoversService.new(wrong_headers_test_file).call }.to raise_error(Parser::HeadersException)
      end

      it 'should raise no rows exception' do
        expect{ Parser::MoversService.new(all_invalid).call }.to raise_error(Parser::NoValidRecords)
      end
    end

    context 'record invalid' do
      it 'should return only valid records' do
        parser = Parser::MoversService.new(invalid_record_file)
        parser.call
        expect(parser.parsed.count).to be(4)
      end
    end
  end
end
