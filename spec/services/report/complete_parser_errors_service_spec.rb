require 'rails_helper'

RSpec.describe Report::CompleteParserErrorsService do
  let(:report) { create(:report_movers) }
  let(:error_hash) { { error_1: ['error'], error_2: ['error'] } }

  describe 'call' do
    it 'should create errors objects' do
      Report::CompleteParserErrorsService.new(report, error_hash).call
      expect(report.wrong_records.count).to eq(2)
    end
  end
end
