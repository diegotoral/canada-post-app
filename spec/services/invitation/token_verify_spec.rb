require 'rails_helper'

RSpec.describe Invitation::TokenVerifyService do
  let(:invitation) { create(:invitation) }

  describe 'call' do
    context 'token is incorrect' do
      it 'should raise error' do
        expect{ Invitation::TokenVerifyService.new(token: 'wrong-token').call }.to raise_error(ActiveRecord::RecordNotFound)
      end
    end
  end
end
