require 'rails_helper'

RSpec.describe PdfGenerator::GenerateService do
  let(:service) { PdfGenerator::GenerateService.new(mover, options) }
  let(:mover) { { record_id: 'some_record_id', kit_no: '999', list_order: 'asc', postal_code: 'ABCfetgfqgse' } }
  let(:options) { { report_id: 'some_report_id' } }

  describe 'call' do
    before { allow(service).to receive(:unlink_phantomjs_file) }
    subject { service.send(:call) }

    context 'successful' do
      before { allow(service).to receive(:run_phantomjs).and_return(:success) }

      it { is_expected.to eq(:success) }
      it { expect(service).not_to have_received(:unlink_phantomjs_file) }
    end

    context 'fail' do
      before { allow(service).to receive(:run_phantomjs).and_raise('boom') }

      it do
        expect{subject}.to raise_error('boom')
        expect(service).to have_received(:unlink_phantomjs_file)
      end
    end
  end

  describe 'batch_invalid?' do
    subject { service.send(:batch_invalid?) }

    [true, false].each do |value|
      context value do
        before { allow(Sidekiq).to receive(:redis).and_return(value) }

        it { is_expected.to eq(value) }
      end
    end
  end

  describe 'report_map_error' do
    subject { service.send(:report_map_error) }

    it { expect{subject}.to raise_error(PdfGenerator::MapGenerationError) }
  end

  describe 'unlink_phantomjs_file' do
    before { allow(service).to receive(:phantomjs_file).and_return(file) }
    subject { service.send(:unlink_phantomjs_file) }
    before { subject }

    context 'file exists' do
      let(:file) { spy }

      it { expect(file).to have_received(:unlink) }
    end

    context 'file does not exist' do
      let(:file) { nil }

      it { is_expected.to eq(nil) }
    end
  end

  describe 'phantomjs_file' do
    before { allow(service).to receive(:render_js).and_return('rendered_java_script') }
    let(:phantomjs_file) { service.send(:phantomjs_file) }
    subject { phantomjs_file }
    after { phantomjs_file.unlink }

    it { is_expected.to be_a(Tempfile) }
    it { expect(subject.path).to match(%r(/tmp/phantomjs_file)) }
    it { expect(subject.path).to match(%r(\.js$)) }

    context 'saving content' do
      let(:tempfile) { spy }
      before { allow(service).to receive(:tempfile).and_return(tempfile) }
      before { subject }

      it { expect(tempfile).to have_received(:write).with('rendered_java_script') }
    end
  end

  describe 'render_js' do
    before { allow(service).to receive(:file_path).and_return('/some/file_path') }
    before { allow(service).to receive(:region).and_return(double(id: 'some_region_id')) }
    subject { service.send(:render_js) }

    it { is_expected.to eq("var page = require('webpage').create();\npage.paperSize = { width: '3750px', height: '5550px' };\npage.settings.userAgent = 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:27.0) Gecko/20100101 Firefox/27.0'\n\npage.open(\"/phantom_calendar/some_report_id?kit_no=999&amp;list_order=asc&amp;postal_code=ABCfetgfqgse&amp;record_id=some_record_id&region_id=some_region_id\", function(status) {\n  if (status !== 'success') {\n        console.log('Unable to load the address!');\n        phantom.exit(1);\n    } else {\n        window.setInterval(function () {\n            var windowStatus = page.evaluate(function () {\n                return window.status;\n            });\n            if (windowStatus == 'map-ready') {\n              page.render(\"./some/file_path\");\n              phantom.exit();\n            }\n            if (windowStatus == 'location-invalid') {\n              console.log('location-invalid');\n              phantom.exit(1);\n            }\n        }, 2000); // Change timeout as required to allow sufficient time\n    }\n});\n") }
  end

  describe 'region' do
    subject { service.send(:region) }

    context 'no region found' do
      it { expect{subject}.to raise_error(PdfGenerator::RegionNotFound) }
    end

    context 'region found' do
      let!(:region) { create(:region, codes: %w(ABC CBA CBŚ BOR)) }

      it { is_expected.to eq(region) }
    end
  end

  describe 'region_code' do
    subject { service.send(:region_code) }

    it { is_expected.to eq('ABC') }
  end

  describe 'tempfile' do
    let(:tempfile) { service.send(:tempfile) }
    subject { tempfile }
    after { tempfile.unlink }

    it { is_expected.to be_a(Tempfile) }
    it { expect(subject.path).to match(%r(/tmp/phantomjs_file)) }
    it { expect(subject.path).to match(%r(\.js$)) }
  end

  describe 'file_path' do
    subject { service.send(:file_path) }

    context 'normal' do
      it { is_expected.to match(%r(^/calendars/some_report_id_999/asc_some_record_id-\d+-\d+\.pdf$)) }
    end

    context 'temporary' do
      let(:options) { { report_id: 'some_report_id', temporary: true } }

      it { is_expected.to match(%r(^/calendars/temporary/some_report_id_999/asc_some_record_id-\d+-\d+\.pdf$)) }
    end
  end

  describe 'unique_file_name' do
    subject { service.send(:unique_file_name) }

    it { is_expected.to match(%r(^some_record_id-\d+-\d+$)) }
  end
end
