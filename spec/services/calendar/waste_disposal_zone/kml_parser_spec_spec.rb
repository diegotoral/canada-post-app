require 'rails_helper'

RSpec.describe Calendar::WasteDisposalZone::KMLParser do
  let(:service) { Calendar::WasteDisposalZone::KMLParser.new(file_path) }
  let(:file_path) { Rails.root.join('spec', 'support', 'parser', 'map.kml.xml') }

  describe 'call' do
    subject { service.call }

    it do
      is_expected.to include(
        { map_name: 'Canada-cp',
          map_description: '',
          waste_types: 'garbage, green_bin, recycling',
          region_description: '',
          name: 'District 1 Thursday 1',
          description: '',
          coordinates:
          [[[-79.577928, 43.6777542],
            [-79.5798111, 43.674142],
            [-79.5795572, 43.6745749],
            [-79.5798111, 43.6740799],
            [-79.5794311, 43.67479],
            [-79.5701981, 43.6905287],
            [-79.5873642, 43.7280028],
            [-79.5807552, 43.7277547],
            [-79.5798111, 43.7301736],
            [-79.5774937, 43.7342049],
            [-79.5901966, 43.7611143],
            [-79.5976639, 43.7596266],
            [-79.6392918, 43.7504512],
            [-79.6295071, 43.7351971],
            [-79.6280479, 43.7335536],
            [-79.6269321, 43.7306698],
            [-79.6210098, 43.7199392],
            [-79.6044445, 43.6922044],
            [-79.5994663, 43.6845084],
            [-79.5965481, 43.6834532],
            [-79.5929432, 43.6802255],
            [-79.5927715, 43.6770596],
            [-79.5891666, 43.6705412],
            [-79.5841026, 43.6716587],
            [-79.577928, 43.6777542]]] },
        { map_name: 'Canada-cp',
          map_description: '',
          waste_types: 'garbage, green_bin, recycling',
          region_description: '',
          name: 'Distcrict 1 Friday 1',
          description: '',
          coordinates:
          [[[-79.5891666, 43.6705412],
            [-79.5890808, 43.6647672],
            [-79.6093369, 43.6460134],
            [-79.6000671, 43.6439016],
            [-79.5921707, 43.6439016],
            [-79.5882225, 43.6424109],
            [-79.5861626, 43.6389326],
            [-79.5853043, 43.6380629],
            [-79.5835876, 43.6396779],
            [-79.5803261, 43.6415414],
            [-79.5638466, 43.645268],
            [-79.5674515, 43.6544595],
            [-79.5471954, 43.6590547],
            [-79.5487404, 43.6620352],
            [-79.5623016, 43.671845],
            [-79.5633316, 43.6744524],
            [-79.550972, 43.6771838],
            [-79.5588684, 43.6941903],
            [-79.5706058, 43.6915217],
            [-79.5798111, 43.674142],
            [-79.5861626, 43.6707275],
            [-79.5891666, 43.6705412]]] }
      )
    end
  end

  describe 'coordinates' do
    subject { service.send(:coordinates, text) }

    context 'valid text' do
      let(:text) { "\n                19.4241714,51.7992738,0\n                19.4427109,51.7971505,0\n                19.4440842,51.797469,0\n                19.439621,51.8062797,0\n                19.4250298,51.8091454,0\n                19.4258881,51.8053244,0\n                19.4241714,51.7992738,0\n              " }

      it { expect{subject}.not_to raise_error }
      it { is_expected.to eq([[19.4241714, 51.7992738], [19.4427109, 51.7971505], [19.4440842, 51.797469], [19.439621, 51.8062797], [19.4250298, 51.8091454], [19.4258881, 51.8053244], [19.4241714, 51.7992738]]) }
    end

    context 'invalid text' do
      let(:text) { "\n                19.4241714,51.7992738,0\n                19.4427109,51.7971505,0\n                a,b,0\n                19.439621,51.8062797,0\n                19.4250298,51.8091454,0\n                19.4258881,51.8053244,0\n                19.4241714,51.7992738,0\n              " }

      it { expect{subject}.to raise_error(Calendar::WasteDisposalZone::InvalidPolygonCoordinates).with_message(text) }
    end
  end

  describe 'remove_tail' do
    subject { service.send(:remove_tail, line) }
    let(:line) { '19.4241714,51.7992738,0' }

    it { is_expected.to eq('19.4241714,51.7992738') }
  end

  describe 'parse_coords' do
    subject { service.send(:parse_coords, line) }

    context 'valid line' do
      let(:line) { '19.4241714,51.7992738' }

      it { expect{subject}.not_to raise_error }
      it { is_expected.to eq([19.4241714, 51.7992738]) }
    end

    context 'invalid line' do
      let(:line) { '19.4241714,a' }

      it { expect{subject}.to raise_error(ArgumentError) }
    end
  end

  describe 'polygon_data' do
    let(:zone) { service.send(:zones, service.send(:document)).first }
    subject { service.send(:polygon_data, zone) }

    it { is_expected.to eq("\n                -79.577928,43.6777542,0\n                -79.5798111,43.674142,0\n                -79.5795572,43.6745749,0\n                -79.5798111,43.6740799,0\n                -79.5794311,43.67479,0\n                -79.5701981,43.6905287,0\n                -79.5873642,43.7280028,0\n                -79.5807552,43.7277547,0\n                -79.5798111,43.7301736,0\n                -79.5774937,43.7342049,0\n                -79.5901966,43.7611143,0\n                -79.5976639,43.7596266,0\n                -79.6392918,43.7504512,0\n                -79.6295071,43.7351971,0\n                -79.6280479,43.7335536,0\n                -79.6269321,43.7306698,0\n                -79.6210098,43.7199392,0\n                -79.6044445,43.6922044,0\n                -79.5994663,43.6845084,0\n                -79.5965481,43.6834532,0\n                -79.5929432,43.6802255,0\n                -79.5927715,43.6770596,0\n                -79.5891666,43.6705412,0\n                -79.5841026,43.6716587,0\n                -79.577928,43.6777542,0\n              ") }
  end

  describe 'description' do
    subject { service.send(:description, service.send(:document)) }

    it { is_expected.to eq('') }
  end

  describe 'name' do
    subject { service.send(:name, service.send(:document)) }

    it { is_expected.to eq('Canada-cp') }
  end

  describe 'zones' do
    let(:layer) { service.send(:layers).first }
    subject { service.send(:zones, layer) }

    it { is_expected.to all(be_a(Nokogiri::XML::Element)) }
    it { is_expected.to all(respond_to(:name)) }
    it { expect(subject.map(&:name)).to all(eq('Placemark')) }
  end

  describe 'layers' do
    subject { service.send(:layers) }

    it { is_expected.to all(be_a(Nokogiri::XML::Element)) }
    it { is_expected.to all(respond_to(:name)) }
    it { expect(subject.map(&:name)).to all(eq('Folder')) }
  end

  describe 'document' do
    subject { service.send(:document) }

    it { is_expected.to all(be_a(Nokogiri::XML::Element)) }
    it { is_expected.to all(respond_to(:name)) }
    it { expect(subject.map(&:name)).to all(eq('Document')) }
  end

  describe 'file' do
    subject { service.send(:file) }

    it { is_expected.to be_a(File) }
  end
end
