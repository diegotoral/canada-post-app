require 'rails_helper'

RSpec.describe Calendar::WasteDisposalZone::KMLImporter do
  let(:region_1) {create(:region, region: 'Toronto')}
  let(:region_2) {create(:region, region: 'Vancouver')}
  let(:service) { Calendar::WasteDisposalZone::KMLImporter.new('some_file_path', region_1.id) }
  let(:data) { [
    { map_name: 'waste', map_description: 'sth', region_name: 'Toronto', region_description: '', name: 'Zone I', description: 'sth', coordinates: [[[19.4241714, 51.7992738], [19.4427109, 51.7971505], [19.4440842, 51.797469], [19.439621, 51.8062797], [19.4250298, 51.8091454], [19.4258881, 51.8053244], [19.4241714, 51.7992738]]] },
    { map_name: 'waste', map_description: 'sth', region_name: 'Toronto', region_description: '', name: 'Zone II', description: 'sth', coordinates: [[[19.4383764, 51.8089066], [19.4392347, 51.8075003], [19.439621, 51.8062797], [19.4440842, 51.797469], [19.4574308, 51.8011315], [19.4587183, 51.8034403], [19.4591475, 51.8053244], [19.4591475, 51.8060939], [19.45889, 51.8067308], [19.4585466, 51.8073411], [19.4576025, 51.8084555], [19.4568729, 51.8092516], [19.4561005, 51.8103129], [19.4554996, 51.8115334], [19.4552851, 51.8126478], [19.4552851, 51.8131253], [19.4548988, 51.8131519], [19.4383764, 51.8089066]]] },
    { map_name: 'waste', map_description: 'sth', region_name: 'Toronto', region_description: '', name: 'ZONE III', description: 'sth', coordinates: [[[19.4440842, 51.797469], [19.4488049, 51.7911254], [19.4491053, 51.7904087], [19.4493198, 51.7895327], [19.4490194, 51.7879665], [19.4490623, 51.787117], [19.4498348, 51.7869311], [19.4511223, 51.7868515], [19.4518518, 51.7871435], [19.4516373, 51.7874886], [19.454298, 51.7875152], [19.454298, 51.7902228], [19.4547272, 51.7913377], [19.4557142, 51.7961154], [19.4561005, 51.7982918], [19.4574308, 51.8011315], [19.4440842, 51.797469]]] },
    { map_name: 'waste', map_description: 'sth', region_name: 'Toronto', region_description: '', name: 'ZONE IV', description: 'sth', coordinates: [[[19.4490623, 51.787117], [19.4513369, 51.7802939], [19.4517231, 51.7787009], [19.461422, 51.7796036], [19.4602633, 51.7846215], [19.4596624, 51.7875417], [19.454298, 51.7875152], [19.4516373, 51.7874886], [19.4518518, 51.7871435], [19.4511223, 51.7868515], [19.4490623, 51.787117]]] },
    { map_name: 'waste', map_description: 'sth', region_name: 'Vancouver', region_description: '', name: 'ZONE V', description: 'sth', coordinates: [[[19.4241714, 51.7992738], [19.4231415, 51.7945495], [19.4216824, 51.7920544], [19.4300079, 51.7903025], [19.4443417, 51.7875948], [19.4490623, 51.787117], [19.4490194, 51.7879665], [19.4493198, 51.7895327], [19.4488049, 51.7911254], [19.4440842, 51.797469], [19.4427109, 51.7971505], [19.4241714, 51.7992738]]] }
    ] }
  before { allow(service).to receive(:data).and_return(data) }

  describe 'call' do
    subject { service.call }

    context 'all regions present' do
      it { expect{subject}.to change{Calendar::WasteDisposalZone.count}.by(+5) }
    end
  end

  describe 'commands' do
    subject { service.send(:commands) }

    it { is_expected.to all(be_a(Calendar::WasteDisposalZone::Create)) }
  end
end
