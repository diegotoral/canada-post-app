require 'rails_helper'

RSpec.describe Calendar::WasteDisposalZone::CoordinatesMatcher do
  let!(:zone) { create(:waste_disposal_zone) }
  let(:service) { Calendar::WasteDisposalZone::CoordinatesMatcher.new(*coordinates, zone.region_config) }
  let(:place_within_coordinates) { [43.751, -79.507] }
  let(:place_out_of_coordinates) { [19.453, -51.764] }
  let(:coordinates) { nil }

  describe 'call' do
    subject { service.call }

    context 'coordinates within polygon' do
      let(:coordinates) { place_within_coordinates }

      it { is_expected.to eq([zone]) }
    end

    context 'coordinates out of polygon' do
      let(:coordinates) { place_out_of_coordinates }

      it { is_expected.to eq([]) }
    end
  end

  describe 'in_zone?' do
    subject { service.send(:in_zone?, double(coordinates: zone.coordinates)) }

    context 'matching' do
      let(:coordinates) { place_within_coordinates }

      it { is_expected.to eq(true) }
    end

    context 'not_matching' do
      let(:coordinates) { place_out_of_coordinates }

      it { is_expected.to eq(false) }
    end
  end

  describe 'polygon' do
    subject { service.send(:polygon, zone.coordinates.first) }

    it { expect(subject.points).to eq([
      Geokit::LatLng.new(-79.5791244, 43.763036),
      Geokit::LatLng.new(-79.5806694, 43.7604325),
      Geokit::LatLng.new(-79.5841026, 43.7570849),
      Geokit::LatLng.new(-79.5816994, 43.7538611),
      Geokit::LatLng.new(-79.5786095, 43.7524972),
      Geokit::LatLng.new(-79.5715714, 43.7490252),
      Geokit::LatLng.new(-79.5679665, 43.7454289),
      Geokit::LatLng.new(-79.5595551, 43.7422046),
      Geokit::LatLng.new(-79.5540619, 43.7446849),
      Geokit::LatLng.new(-79.549427,  43.7435687),
      Geokit::LatLng.new(-79.5478821, 43.7410884),
      Geokit::LatLng.new(-79.5501137, 43.7332746),
      Geokit::LatLng.new(-79.5437622, 43.7327784),
      Geokit::LatLng.new(-79.5396423, 43.728561),
      Geokit::LatLng.new(-79.5411873, 43.7232268),
      Geokit::LatLng.new(-79.5437622, 43.7178921),
      Geokit::LatLng.new(-79.5401573, 43.7168995),
      Geokit::LatLng.new(-79.538784,  43.7133014),
      Geokit::LatLng.new(-79.527626,  43.7154107),
      Geokit::LatLng.new(-79.5039368, 43.7178921),
      Geokit::LatLng.new(-79.4869423, 43.7211178),
      Geokit::LatLng.new(-79.4486618, 43.7305457),
      Geokit::LatLng.new(-79.4522667, 43.7400962),
      Geokit::LatLng.new(-79.4558716, 43.7431967),
      Geokit::LatLng.new(-79.4596481, 43.745925),
      Geokit::LatLng.new(-79.462738,  43.7477851),
      Geokit::LatLng.new(-79.4635963, 43.7505132),
      Geokit::LatLng.new(-79.464798,  43.7579528),
      Geokit::LatLng.new(-79.4654846, 43.7614243),
      Geokit::LatLng.new(-79.4672012, 43.7688625),
      Geokit::LatLng.new(-79.4699478, 43.7805139),
      Geokit::LatLng.new(-79.4701195, 43.7838602),
      Geokit::LatLng.new(-79.4699478, 43.7873301),
      Geokit::LatLng.new(-79.4852257, 43.7836123),
      Geokit::LatLng.new(-79.5113182, 43.7774154),
      Geokit::LatLng.new(-79.5429039, 43.7708459),
      Geokit::LatLng.new(-79.5720863, 43.7650196),
      Geokit::LatLng.new(-79.5791244, 43.763036)
    ]) }
  end

  describe 'points' do
    subject { service.send(:points, zone.coordinates.first) }

    it { is_expected.to eq([
      Geokit::LatLng.new(-79.5791244, 43.763036),
      Geokit::LatLng.new(-79.5806694, 43.7604325),
      Geokit::LatLng.new(-79.5841026, 43.7570849),
      Geokit::LatLng.new(-79.5816994, 43.7538611),
      Geokit::LatLng.new(-79.5786095, 43.7524972),
      Geokit::LatLng.new(-79.5715714, 43.7490252),
      Geokit::LatLng.new(-79.5679665, 43.7454289),
      Geokit::LatLng.new(-79.5595551, 43.7422046),
      Geokit::LatLng.new(-79.5540619, 43.7446849),
      Geokit::LatLng.new(-79.549427,  43.7435687),
      Geokit::LatLng.new(-79.5478821, 43.7410884),
      Geokit::LatLng.new(-79.5501137, 43.7332746),
      Geokit::LatLng.new(-79.5437622, 43.7327784),
      Geokit::LatLng.new(-79.5396423, 43.728561),
      Geokit::LatLng.new(-79.5411873, 43.7232268),
      Geokit::LatLng.new(-79.5437622, 43.7178921),
      Geokit::LatLng.new(-79.5401573, 43.7168995),
      Geokit::LatLng.new(-79.538784,  43.7133014),
      Geokit::LatLng.new(-79.527626,  43.7154107),
      Geokit::LatLng.new(-79.5039368, 43.7178921),
      Geokit::LatLng.new(-79.4869423, 43.7211178),
      Geokit::LatLng.new(-79.4486618, 43.7305457),
      Geokit::LatLng.new(-79.4522667, 43.7400962),
      Geokit::LatLng.new(-79.4558716, 43.7431967),
      Geokit::LatLng.new(-79.4596481, 43.745925),
      Geokit::LatLng.new(-79.462738,  43.7477851),
      Geokit::LatLng.new(-79.4635963, 43.7505132),
      Geokit::LatLng.new(-79.464798,  43.7579528),
      Geokit::LatLng.new(-79.4654846, 43.7614243),
      Geokit::LatLng.new(-79.4672012, 43.7688625),
      Geokit::LatLng.new(-79.4699478, 43.7805139),
      Geokit::LatLng.new(-79.4701195, 43.7838602),
      Geokit::LatLng.new(-79.4699478, 43.7873301),
      Geokit::LatLng.new(-79.4852257, 43.7836123),
      Geokit::LatLng.new(-79.5113182, 43.7774154),
      Geokit::LatLng.new(-79.5429039, 43.7708459),
      Geokit::LatLng.new(-79.5720863, 43.7650196),
      Geokit::LatLng.new(-79.5791244, 43.763036)
    ]) }
  end

  describe 'point' do
    subject { service.send(:point) }

    context 'coordinates within polygon' do
      let(:coordinates) { place_within_coordinates }

      it { is_expected.to eq(Geokit::LatLng.new(-79.507, 43.751)) }
    end

    context 'coordinates out of polygon' do
      let(:coordinates) { place_out_of_coordinates }

      it { is_expected.to eq(Geokit::LatLng.new(-51.764, 19.453)) }
    end
  end
end
