require 'rails_helper'

RSpec.describe Calendar::Events::GenerateService do
  let(:region) { create(:region_no_images, region: 'toronto')}

  describe 'call' do
    before(:each) do
      allow_any_instance_of(Calendar::Events::AdvertisersEventsMapService).to receive(:call).and_return([1,2,3])
      allow_any_instance_of(Calendar::Events::MovingDateService).to receive(:call).and_return([4,5,6])
      allow_any_instance_of(Calendar::Events::WastePickupService).to receive(:call).and_return([7,8,9])
    end
    let(:region) { create(:region_no_images, region: 'toronto')}

    it 'should return proper array' do
      expect(Calendar::Events::GenerateService.new(DateTime.now, region, double, 'N').call).to eq((1..9).to_a)
    end
  end
end
