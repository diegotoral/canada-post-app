require 'rails_helper'

RSpec.describe Calendar::Events::AdvertisersEventsMapService do
  let(:advertiser) { create(:advertiser_no_images)}
  let(:month_config) { create(:month_config, advertiser_id: advertiser.id)}
  let(:region) { month_config.region_config }
  let(:past_date) { Date.new - 5.days }

  describe 'map_to_event' do
    subject { Calendar::Events::AdvertisersEventsMapService }
    let(:calendar_event) { create(:advertiser_event, advertiser: advertiser, start_date: DateTime.now, end_date: DateTime.now)}

    it 'should generate one struct event due to one day event' do
      expect( subject.new(region, [past_date]).send(:map_to_event, calendar_event).count ).to eq(1)
    end

    it 'should generate proper calendar event' do
      expect( subject.new(region, [past_date]).send(:map_to_event, calendar_event).first.to_hash ).to eq({event_color: 'red', name: calendar_event.name, fr_name: nil, date: calendar_event.start_date, type: 'advertiser_event'})
    end
  end

  describe 'advertiser_events_to_calendar_events' do
    let!(:calendar_event) { create(:advertiser_event, advertiser: advertiser, start_date: DateTime.now, end_date: DateTime.now)}
    subject { Calendar::Events::AdvertisersEventsMapService.new(region, [past_date]).send(:advertiser_events_to_calendar_events) }

    before(:each) do
      allow_any_instance_of(Calendar::Events::GenerateService).to receive(:call).and_return([CalendarEventStructure.new(name: 'XYZ', date: DateTime.now, type: 'advertiser_event')])
    end

    it 'should generate flat array with one advertiser and one event' do
      expect( subject.first.to_hash ).to eq({event_color: 'red', name: calendar_event.name, fr_name: nil, date: calendar_event.start_date, type: 'advertiser_event'})
    end
  end
end
