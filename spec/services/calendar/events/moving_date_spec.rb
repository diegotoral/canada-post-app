require 'rails_helper'

RSpec.describe Calendar::Events::MovingDateService do
  let(:date) { DateTime.now.to_date }

  describe 'moving_day_hash' do
    subject { Calendar::Events::MovingDateService.new(date).send(:moving_day_hash) }

    it 'should build calendar event' do
      expect(subject.to_hash).to eq({event_color: nil, name: 'Moving Day', fr_name: nil, type: 'moving_date', date: date})
    end
  end

  describe 'anniversary_hash' do
    subject { Calendar::Events::MovingDateService.new(date).send(:anniversary_hash) }

    it 'should build calendar event' do
      expect(subject.to_hash).to eq({event_color: nil, name: 'Happy Anniversary', fr_name: nil, type: 'anniversary_moving_date', date: date + 1.year})
    end
  end

  describe 'call' do
    subject { Calendar::Events::MovingDateService.new(date).call }

    it 'should respond with array of 2 elements' do
      expect(subject.count).to eq(2)
    end

    it 'should call anniversary_hash method' do
      allow_any_instance_of(Calendar::Events::MovingDateService).to receive(:anniversary_hash).and_raise('AnniversaryHash')
      expect{subject}.to raise_error('AnniversaryHash')
    end

    it 'should call moving_day_hash method' do
      allow_any_instance_of(Calendar::Events::MovingDateService).to receive(:moving_day_hash).and_raise('MovingDayHash')
      expect{subject}.to raise_error('MovingDayHash')
    end
  end
end
