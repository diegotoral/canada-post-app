require 'rails_helper'

RSpec.describe Calendar::EnqueueCalendarGeneratorsService do
  let(:service) { Calendar::EnqueueCalendarGeneratorsService.new(params) }
  let(:params)  { { file: Rack::Test::UploadedFile.new(Rails.root.join('spec', 'support', 'parser', 'valid_movers_data.txt')) } }

  describe 'call' do
    before { allow(service).to receive(:add_errors_to_report) }
    before { allow(service).to receive(:update_batch_id) }
    before { allow(service).to receive(:generate_calendars) }
    before { service.send(:call) }

    it { expect(service).to have_received(:add_errors_to_report).ordered }
    it { expect(service).to have_received(:update_batch_id).ordered }
    it { expect(service).to have_received(:generate_calendars).ordered }
  end

  describe 'generate_calendars' do
    let(:records) { [double(to_h: :mover)] }
    let(:report) { double(id: :record_id) }
    let(:batch) { double }
    before { def batch.jobs; yield; end }
    before { allow(service).to receive(:batch).and_return(batch) }
    before { allow(service).to receive(:report).and_return(report) }
    before { allow(service).to receive(:records).and_return(records) }
    before { allow(CalendarGenerateWorker).to receive(:perform_async) }
    before { service.send(:generate_calendars) }

    it { expect(CalendarGenerateWorker).to have_received(:perform_async).with(:mover, :record_id, false) }
  end

  describe 'update_batch_id' do
    let(:report) { double }
    let(:batch) { double(bid: :some_id) }
    let(:srv) { spy }
    before { allow(service).to receive(:report).and_return(report) }
    before { allow(service).to receive(:batch).and_return(batch) }
    before { allow(Report::Movers::BatchIdUpdate).to receive(:new).and_return(srv) }
    before { service.send(:update_batch_id) }

    it { expect(Report::Movers::BatchIdUpdate).to have_received(:new).with(report: report, batch_id: batch.bid) }
    it { expect(srv).to have_received(:call) }
  end

  describe 'add_errors_to_report' do
    let(:report) { double }
    let(:wrong_records) { double }
    let(:srv) { spy }
    before { allow(service).to receive(:report).and_return(report) }
    before { allow(service).to receive(:wrong_records).and_return(wrong_records) }
    before { allow(Report::CompleteParserErrorsService).to receive(:new).and_return(srv) }
    before { service.send(:add_errors_to_report) }

    it { expect(Report::CompleteParserErrorsService).to have_received(:new).with(report, wrong_records) }
    it { expect(srv).to have_received(:call) }
  end

  describe 'batch' do
    let(:batch) { spy }
    before { allow(Sidekiq::Batch).to receive(:new).and_return(batch) }
    before { allow(service).to receive(:report).and_return(double(id: :id_of_report)) }
    subject { service.send(:batch) }
    before { subject }

    it { is_expected.to eq(batch) }
    it { expect(batch).to have_received(:description=).with('Batch for valid_movers_data.txt') }
    it { expect(batch).to have_received(:on).with(:complete, 'Calendar::GenerateCallbackService', { report_id: :id_of_report }) }
    it { expect(subject).to respond_to(:bid) }
  end

  describe 'report' do
    let(:movers_create_service) { spy }
    before { allow(service).to receive(:records).and_return(double(count: :count_of_records)) }
    before { allow(Report::Movers::Create).to receive(:new).and_return(movers_create_service) }
    subject { service.send(:report) }

    it { is_expected.to eq(movers_create_service) }
    it { subject; expect(Report::Movers::Create).to have_received(:new).with(file_name: 'valid_movers_data.txt', parsed_count: :count_of_records) }
    it { subject; expect(movers_create_service).to have_received(:call) }
  end

  describe 'parsing data' do
    let(:movers_service) { double(parsed: :some_parsed_records, wrong_records: :some_not_parsed_records) }
    before { allow(Parser::MoversService).to receive(:new).and_return(movers_service) }
    before { allow(movers_service).to receive(:call).and_return(movers_service) }

    describe 'records' do
      subject { service.send(:records) }

      it { is_expected.to eq(:some_parsed_records) }
      it { subject; expect(Parser::MoversService).to have_received(:new).with(params[:file]) }
    end

    describe 'wrong_records' do
      subject { service.send(:wrong_records) }

      it { is_expected.to eq(:some_not_parsed_records) }
      it { subject; expect(Parser::MoversService).to have_received(:new).with(params[:file]) }
    end

    describe 'parsed_data' do
      subject { service.send(:parsed_data) }

      it { is_expected.to eq(movers_service) }
      it { subject; expect(Parser::MoversService).to have_received(:new).with(params[:file]) }
    end
  end

  describe 'file_name' do
    subject { service.send(:file_name) }

    it { is_expected.to eq('valid_movers_data.txt') }
  end

  describe 'file' do
    subject { service.send(:file) }

    context 'no such param' do
      let(:params) { {} }

      it { expect{subject}.to raise_error(Calendar::FileNotFound) }
    end

    context 'file param given' do
      it { is_expected.to eq(params[:file]) }
    end
  end
end
