require 'rails_helper'

RSpec.describe Calendar::WastePickup::FetchFromPatternService do
  let(:service) { Calendar::WastePickup::FetchFromPatternService.new(zone, options) }
  let(:zone) { create(:waste_disposal_zone) }
  let(:waste_day_schedule) { { 'garbage' => '1', 'recycling' => '0', 'yard_waste' => '0', 'green_bin' => '0', 'christmas_tree' => '0' } }
  let(:waste_week_schedule) { { '0' => waste_day_schedule, '1' => waste_day_schedule, '2' => waste_day_schedule, '3' => waste_day_schedule, '4' => waste_day_schedule, '5' => waste_day_schedule, '6' => waste_day_schedule } }
  let(:options) { { 'date_from' => '01-01-2017', 'date_to' => '31-01-2017',
                    'waste_pickups_week_1' => waste_week_schedule,
                    'waste_pickups_week_2' => waste_week_schedule }.symbolize_keys }

  describe 'call' do
    subject { service.send(:call) }
    before { allow(service).to receive(:purge) }
    before { allow(service).to receive(:persist) }
    before { allow(service).to receive(:validate_region) }
    before { subject }

    it { expect(service).to have_received(:purge) }
    it { expect(service).to have_received(:persist) }
    it { expect(service).to have_received(:validate_region) }
  end

  describe 'persist' do
    subject { service.send(:persist) }

    let(:updater) { spy }
    before { allow(Calendar::WastePickup::UpdateDayInfo).to receive(:new).and_return(updater) }
    before { subject }

    it { expect(Calendar::WastePickup::UpdateDayInfo).to have_received(:new).at_least(4).times }
    it { expect(updater).to have_received(:call).at_least(4).times }
  end

  describe 'validate_region' do
    subject { service.send(:validate_region) }

    let(:validator) { spy }
    before { allow(Calendar::RegionConfigValidationStatusService).to receive(:new).and_return(validator) }
    before { subject }

    it { expect(Calendar::RegionConfigValidationStatusService).to have_received(:new) }
    it { expect(validator).to have_received(:call) }
  end

  describe 'purge' do
    subject { service.send(:purge) }

    let(:destructor) { spy }
    let(:pickups) { spy }
    before { allow(Calendar::WastePickup::DestroyAll).to receive(:new).and_return(destructor) }
    before { allow(zone).to receive(:waste_pickups).and_return(pickups) }
    before { subject }

    it { expect(Calendar::WastePickup::DestroyAll).to have_received(:new).with(pickups) }
    it { expect(destructor).to have_received(:call) }
  end

  describe 'all_days_by_week_day' do
    subject { service.send(:all_days_by_week_day) }

    it { is_expected.to eq((Date.new(2017,1,1)..Date.new(2017,01,31)).group_by(&:wday)) }
  end

  describe 'date_to' do
    subject { service.send(:date_to) }

    it { is_expected.to eq(Date.new(2017,1,31)) }
  end

  describe 'date_from' do
    subject { service.send(:date_from) }

    it { is_expected.to eq(Date.new(2017,1,1)) }
  end

  describe 'waste_pickups_week_1' do
    subject { service.send(:waste_pickups_week_1) }

    it { is_expected.to eq(waste_week_schedule) }
  end

  describe 'waste_pickups_week_2' do
    subject { service.send(:waste_pickups_week_2) }

    it { is_expected.to eq(waste_week_schedule) }
  end

end
