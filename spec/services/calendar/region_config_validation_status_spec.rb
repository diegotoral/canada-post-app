require 'rails_helper'

RSpec.describe Calendar::RegionConfigValidationStatusService do

  describe 'region_config_valid?' do
    let(:region_config) { create(:region) }

    it 'should be valid' do
      expect(Calendar::RegionConfigValidationStatusService.new(region_config).send(:region_config_valid?)).to be(true)
    end

    it 'should be invalid' do
      region_config.front = nil
      expect(Calendar::RegionConfigValidationStatusService.new(region_config).send(:region_config_valid?)).to be(false)
    end
  end

  describe 'months_valid?' do
    let(:month) { create(:full_month_config) }
    let(:advertiser) { create(:advertiser) }

    it 'should be valid' do
      month.update(advertiser: advertiser)
      expect(Calendar::RegionConfigValidationStatusService.new(month.region_config).send(:months_valid?)).to be(true)
    end

    it 'should be invalid - no advertiser' do
      expect(Calendar::RegionConfigValidationStatusService.new(month.region_config).send(:months_valid?)).to be(false)
    end
  end
end
