require 'rails_helper'

RSpec.describe Calendar::WastePickup::DestroyAll do
  let!(:waste_pickup) { create(:waste_pickup) }

  describe 'call' do
    it 'destroys all objects in collection' do
      Calendar::WastePickup::DestroyAll.new(Calendar::WastePickup.all).call
      expect(Calendar::WastePickup.count).to eq(0)
    end

    it 'does nothing with empty collection' do
      Calendar::WastePickup::DestroyAll.new(Calendar::WastePickup.none).call
      expect(Calendar::WastePickup.count).to eq(1)
    end
  end
end
