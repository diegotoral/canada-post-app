require 'rails_helper'

RSpec.describe Calendar::RegionConfig::Refresh do
  let(:region_config) { create(:region) }
  let(:calendar) { File.read("#{Rails.root}/spec/rails_helper.rb")}

  describe 'call' do
    it 'should update calendar' do
      region_config
      expect{ Calendar::RegionConfig::Refresh.new(region_config, calendar).call }.to change{region_config.calendar}
    end

    it 'should update status' do
      region_config
      expect{ Calendar::RegionConfig::Refresh.new(region_config, calendar).call }.to change{region_config.status}
    end
  end
end
