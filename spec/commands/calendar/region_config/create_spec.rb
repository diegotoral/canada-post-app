require 'rails_helper'

RSpec.describe Calendar::RegionConfig::Create do
  let(:region_config) { create(:region) }

  describe 'call' do
    context 'region exists' do
      it 'should raise an error' do
        expect{ Calendar::RegionConfig::Create.new(region: region_config.region).call }.to raise_error(Calendar::RegionConfig::Create::RegionAlreadyExists)
      end
    end

    context 'validate' do
      it 'presence of region' do
        expect{ Calendar::RegionConfig::Create.new(region: nil).call }.to raise_error{ActiveModel::ValidationError}
      end
    end

    context 'create with months' do
      it 'should create region with all months' do
        expect( Calendar::RegionConfig::Create.new(region: Faker::Address.city ).call.months.count ).to eq(12)
      end
    end
  end
end
