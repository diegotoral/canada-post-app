require 'rails_helper'

RSpec.describe Calendar::RegionConfig::Destroy do
  let(:region_config) { create(:region) }

  describe 'call' do
    it 'should call months destroy all command' do
      allow_any_instance_of(Calendar::Month::DestroyAll).to receive(:call).and_raise('MonthDestroyAll')
      region_config
      expect{Calendar::RegionConfig::Destroy.new(region_config.id).call}.to raise_error('MonthDestroyAll')
    end

    it 'should call waste pickups destroy all command' do
      allow_any_instance_of(Calendar::WastePickup::DestroyAll).to receive(:call).and_raise('WastePickupDestroyAll')
      region_config
      expect{Calendar::RegionConfig::Destroy.new(region_config.id).call}.to raise_error('WastePickupDestroyAll')
    end

    it 'should remove region' do
      region_config
      expect{Calendar::RegionConfig::Destroy.new(region_config.id).call}.to change{Calendar::RegionConfig.count - 1}
    end
  end
end
