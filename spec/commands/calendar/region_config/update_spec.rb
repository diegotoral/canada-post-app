require 'rails_helper'

RSpec.describe Calendar::RegionConfig::Update do
  let(:image2) {Rack::Test::UploadedFile.new(Rails.root.join('spec', 'support', 'images', 'image2.png'), "image/png")}
  let(:codes_file) { Rack::Test::UploadedFile.new(Rails.root.join('spec', 'support', 'parser', 'toronto_codes.txt'), "text/txt") }
  let(:region_config) { FactoryGirl.create(:region) }

  describe 'call' do
    context 'update' do
      it 'shouldn\'t change front' do
        expect{ Calendar::RegionConfig::Update.new(id: region_config.id, front: nil).call }.not_to change{Calendar::RegionConfig.find(region_config.id).front.path}
      end

      it 'shouldn\'t change signature' do
        expect{ Calendar::RegionConfig::Update.new(id: region_config.id, signature: nil).call }.not_to change{Calendar::RegionConfig.find(region_config.id).signature.path}
      end

      it 'shouldn\'t change front_left' do
        expect{ Calendar::RegionConfig::Update.new(id: region_config.id, front_left: nil).call }.not_to change{Calendar::RegionConfig.find(region_config.id).front_left.path}
      end

      it 'shouldn\'t change last_page' do
        expect{ Calendar::RegionConfig::Update.new(id: region_config.id, last_page: nil).call }.not_to change{Calendar::RegionConfig.find(region_config.id).last_page.path}
      end

      it 'shouldn\'t change codes' do
        expect{ Calendar::RegionConfig::Update.new(id: region_config.id, codes_file: nil).call }.not_to change{Calendar::RegionConfig.find(region_config.id).codes}
      end

      it 'should change front' do
        expect {Calendar::RegionConfig::Update.new(id: region_config.id, front: image2).call}.to change{Calendar::RegionConfig.find(region_config.id).front.path}
      end

      it 'should change signature' do
        expect {Calendar::RegionConfig::Update.new(id: region_config.id, signature: image2).call}.to change{Calendar::RegionConfig.find(region_config.id).signature.path}
      end

      it 'should change front_left' do
        expect {Calendar::RegionConfig::Update.new(id: region_config.id, front_left: image2).call}.to change{Calendar::RegionConfig.find(region_config.id).front_left.path}
      end

      it 'should change last_page' do
        expect {Calendar::RegionConfig::Update.new(id: region_config.id, last_page: image2).call}.to change{Calendar::RegionConfig.find(region_config.id).last_page.path}
      end

      it 'should change codes' do
        expect {Calendar::RegionConfig::Update.new(id: region_config.id, codes_file: codes_file).call}.to change{Calendar::RegionConfig.find(region_config.id).codes}
      end

      it 'should change waste_pickup_url' do
        expect {Calendar::RegionConfig::Update.new(id: region_config.id, waste_pickup_url: 'wastepickupurl').call}.to change{Calendar::RegionConfig.find(region_config.id).waste_pickup_url}
      end
    end

    context 'update region months' do
      let(:month) { FactoryGirl.create(:full_month_config) }
      it 'should call month command' do
        allow_any_instance_of(Calendar::Month::Update).to receive(:call).and_raise('CalendarMonthUpdate')
        expect {Calendar::RegionConfig::Update.new(id: region_config.id, calendar_months: {"#{month.id}" => {advertiser_id: 1}}).call}.to raise_error('CalendarMonthUpdate')
      end
    end

    context 'region config status' do
      before(:each) do
        allow_any_instance_of(Calendar::RegionConfigValidationStatusService).to receive(:call).and_raise('RegionConfigValidationStatus')
      end

      it 'should call Calendar::RegionConfigValidationStatusService service' do
        expect{Calendar::RegionConfig::Update.new(id: region_config.id, front: nil).call}.to raise_error('RegionConfigValidationStatus')
      end
    end
  end
end
