require 'rails_helper'

RSpec.describe Calendar::Month::Create do
  describe 'call' do
    context 'validate' do
      it 'presence of region_config_id' do
        expect{ Calendar::Month::Create.new(region_config_id: nil, month_number: 1).call }.to raise_error{ActiveModel::ValidationError}
      end

      it 'presence of month_number' do
        expect{ Calendar::Month::Create.new(region_config_id: 1, month_number: nil).call }.to raise_error{ActiveModel::ValidationError}
      end
    end
  end
end
