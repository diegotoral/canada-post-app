require 'rails_helper'

RSpec.describe Calendar::Month::DestroyAll do
  let(:month) { create(:month_config) }

  describe 'call' do
    it 'destroys all objects in collection' do
      month
      Calendar::Month::DestroyAll.new(Calendar::Month.all).call
      expect(Calendar::Month.count).to eq(0)
    end

    it 'does nothing with empty collection' do
      month
      Calendar::Month::DestroyAll.new(Calendar::Month.none).call
      expect(Calendar::Month.count).to eq(1)
    end
  end
end
