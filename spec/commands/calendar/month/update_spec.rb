require 'rails_helper'

RSpec.describe Calendar::Month::Update do
  let(:advertiser) { create(:advertiser)}
  let(:month_config) { FactoryGirl.create(:month_config) }

  describe 'call' do
    context 'update' do
      it 'shouldn\'t change advertiser' do
        Calendar::Month::Update.new(id: month_config.id, advertiser_id: advertiser.id).call
        month_config.reload
        expect(month_config.advertiser.id).to be(advertiser.id)
      end
    end
  end
end
