require 'rails_helper'

RSpec.describe Calendar::Advertiser::Update do
  let(:image) { Rack::Test::UploadedFile.new(Rails.root.join('spec', 'support', 'images', 'image2.png'), "image/png") }

  describe 'call' do
    context 'update' do
      subject { create(:advertiser) }

      context 'when image was not uploaded' do
        it 'shouldn\'t change right advert' do
          Calendar::Advertiser::Update.new(id: subject.id, right_advert: nil).call
          subject.reload
          expect(subject.right_advert).not_to be(nil)
        end

        it 'shouldn\'t change left_advert_1 advert' do
          Calendar::Advertiser::Update.new(id: subject.id, left_advert_1: nil).call
          subject.reload
          expect(subject.left_advert_1).not_to be(nil)
        end

        it 'shouldn\'t change left advert 2 advert' do
          Calendar::Advertiser::Update.new(id: subject.id, left_advert_2: nil).call
          subject.reload
          expect(subject.left_advert_2).not_to be(nil)
        end

        it 'shouldn\'t change left_advert_3 advert' do
          Calendar::Advertiser::Update.new(id: subject.id, left_advert_3: nil).call
          subject.reload
          expect(subject.left_advert_3).not_to be(nil)
        end

        it 'shouldn\'t change cover_advert advert' do
          Calendar::Advertiser::Update.new(id: subject.id, cover_advert: nil).call
          subject.reload
          expect(subject.cover_advert).not_to be(nil)
        end
      end

      context 'when image was uploaded' do
        it 'should change right advert' do
          old_path = subject.right_advert.path
          Calendar::Advertiser::Update.new(id: subject.id, right_advert: image).call
          subject.reload
          expect(subject.right_advert.path).not_to be(old_path)
        end

        it 'should change left_advert_1 advert' do
          old_path = subject.left_advert_1.path
          Calendar::Advertiser::Update.new(id: subject.id, left_advert_1: image).call
          subject.reload
          expect(subject.left_advert_1.path).not_to be(old_path)
        end

        it 'should change left advert 2 advert' do
          old_path = subject.left_advert_2.path
          Calendar::Advertiser::Update.new(id: subject.id, left_advert_2: image).call
          subject.reload
          expect(subject.left_advert_2.path).not_to be(old_path)
        end

        it 'should change left_advert_3 advert' do
          old_path = subject.left_advert_3.path
          Calendar::Advertiser::Update.new(id: subject.id, left_advert_3: image).call
          subject.reload
          expect(subject.left_advert_3.path).not_to be(old_path)
        end

        it 'should change cover_advert advert' do
          old_path = subject.cover_advert.path
          Calendar::Advertiser::Update.new(id: subject.id, cover_advert: image).call
          subject.reload
          expect(subject.cover_advert.path).not_to be(old_path)
        end
      end
    end
  end
end
