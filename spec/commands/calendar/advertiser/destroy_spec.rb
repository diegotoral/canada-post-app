require 'rails_helper'

RSpec.describe Calendar::Advertiser::Destroy do
  let(:month) { create(:month_config) }

  describe 'call' do
    context 'destroy' do
      subject { create(:advertiser) }
      it 'should raise an exception if advertiser is used' do
        month.update(advertiser: subject)
        expect{ Calendar::Advertiser::Destroy.new(subject.id).call }.to raise_error(Calendar::Advertiser::Destroy::AdvertiserIsConnectedToMonth)
      end

      it 'should delete advertiser' do
        subject
        expect{ Calendar::Advertiser::Destroy.new(subject.id).call }.to change { Calendar::Advertiser.count }.by(-1)
      end
    end
  end
end
