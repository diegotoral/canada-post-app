require 'rails_helper'

RSpec.describe Calendar::Advertiser::Create do
  let(:image) { Rack::Test::UploadedFile.new(Rails.root.join('spec', 'support', 'images', 'image2.png'), "image/png") }
  let(:valid_params) { {name: Faker::Company.name, cover_advert: image, left_advert_1: image, left_advert_2: image,
                  left_advert_3: image, right_advert: image, grid: 0} }

  describe 'call' do
    context 'create' do
      it 'should create advertiser with all fields' do
        advertiser = Calendar::Advertiser::Create.new(valid_params).call
        expect(advertiser.name).not_to eq(nil)
        expect(advertiser.cover_advert).not_to eq(nil)
        expect(advertiser.left_advert_1).not_to eq(nil)
        expect(advertiser.left_advert_2).not_to eq(nil)
        expect(advertiser.left_advert_3).not_to eq(nil)
        expect(advertiser.grid).not_to eq(nil)
        expect(advertiser.right_advert).not_to eq(nil)
      end

      it 'should validate name' do
        expect{Calendar::Advertiser::Create.new(valid_params.reject{|key| key == :name}).call}.to raise_error(ActiveModel::ValidationError)
      end

      it 'should validate grid' do
        expect{Calendar::Advertiser::Create.new(valid_params.reject{|key| key == :grid}).call}.to raise_error(ActiveModel::ValidationError)
      end

      it 'should validate cover_advert' do
        expect{Calendar::Advertiser::Create.new(valid_params.reject{|key| key == :cover_advert}).call}.to raise_error(ActiveModel::ValidationError)
      end

      it 'should validate left_advert_1' do
        expect{Calendar::Advertiser::Create.new(valid_params.reject{|key| key == :left_advert_1}).call}.to raise_error(ActiveModel::ValidationError)
      end

      it 'should validate left_advert_2 with 1:2 grid' do
        params = valid_params.reject{|key| key == :left_advert_2}.merge({grid: Calendar::Advertiser.grids['1:2']})
        expect{Calendar::Advertiser::Create.new(params).call}.to raise_error(ActiveModel::ValidationError)
      end

      it 'should validate left_advert_2 with 2:1 grid' do
        params = valid_params.reject{|key| key == :left_advert_2}.merge({grid: Calendar::Advertiser.grids['2:1']})
        expect{Calendar::Advertiser::Create.new(params).call}.to raise_error(ActiveModel::ValidationError)
      end

      it 'should validate left_advert_2 with 1:1:1 grid' do
        params = valid_params.reject{|key| key == :left_advert_2}.merge({grid: Calendar::Advertiser.grids['1:1:1']})
        expect{Calendar::Advertiser::Create.new(params).call}.to raise_error(ActiveModel::ValidationError)
      end

      it 'should validate left_advert_3 with 1:1:1 grid' do
        params = valid_params.reject{|key| key == :left_advert_3}.merge({grid: Calendar::Advertiser.grids['1:1:1']})
        expect{Calendar::Advertiser::Create.new(params).call}.to raise_error(ActiveModel::ValidationError)
      end
    end
  end
end
