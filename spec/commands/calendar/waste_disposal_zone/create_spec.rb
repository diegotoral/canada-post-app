require 'rails_helper'

RSpec.describe Calendar::WasteDisposalZone::Create do
  let!(:region) { create(:region) }
  let(:command) { Calendar::WasteDisposalZone::Create.new(params) }

  describe 'call' do
    subject { command.call }

    context 'no params given' do
      let(:params) { {} }

      it { expect{subject}.to raise_error(ActiveRecord::RecordNotFound) }
    end

    context 'valid params given' do
      context 'zone with such name does not exist' do
        let(:params) { { waste_types: %w(garbage), region_id: region.id, coordinates: [[[10.0, 11.0], [25, 30]]] } }

        it { expect{subject}.not_to raise_error }
        it { expect(subject).to be_a(Calendar::WasteDisposalZone) }
        it { expect(subject.coordinates).to eq([[[10.0, 11.0], [25, 30]]]) }
        it { expect(subject.region_config).to eq(region) }
      end

      context 'zone with such name exist' do
        let!(:zone) { create(:waste_disposal_zone, region_config: region, name: 'test') }
        let(:params) { { waste_types: %w(garbage), name: 'test', region_id: region.id, coordinates: [[[10.0, 11.0], [25, 30]]] } }

        it { expect{subject}.not_to raise_error }
        it { expect(subject).to eq(zone) }
        it { expect(subject.coordinates).to eq([[[10.0, 11.0], [25, 30]]]) }
        it { expect(subject.region_config).to eq(region) }
      end
    end

    context 'invalid params given' do
      context 'coordinates' do
        context 'argument is not a list' do
          let(:params) { { waste_types: %w(garbage), coordinates: :something } }

          it { expect{subject}.to raise_error(Calendar::WasteDisposalZone::Create::WrongCoordinatesFormat).with_message('argument is not a proper list') }
        end

        context 'argument is not a list of arrays' do
          let(:params) { { waste_types: %w(garbage), coordinates: [:something] } }

          it { expect{subject}.to raise_error(Calendar::WasteDisposalZone::Create::WrongCoordinatesFormat).with_message('argument is not a proper list') }
        end

        context 'values are not numbers' do
          let(:params) { { waste_types: %w(garbage), coordinates: [[[:a, :b]]] } }

          it { expect{subject}.to raise_error(Calendar::WasteDisposalZone::Create::WrongCoordinatesFormat).with_message('values are not numbers') }
        end

        context 'longitute out of range' do
          let(:params) { { waste_types: %w(garbage), coordinates: [[[200,45]]] } }

          it { expect{subject}.to raise_error(Calendar::WasteDisposalZone::Create::WrongCoordinatesFormat).with_message('values out of range') }
        end

        context 'latitude out of range' do
          let(:params) { { waste_types: %w(garbage), coordinates: [[[45,-100]]] } }

          it { expect{subject}.to raise_error(Calendar::WasteDisposalZone::Create::WrongCoordinatesFormat).with_message('values out of range') }
        end
      end

      context 'region' do
        context 'region does not exist' do
          let(:params) { { region_id: 0, coordinates: [[[10.0, 11.0], [25, 30]]] } }

          it { expect{subject}.to raise_error(ActiveRecord::RecordNotFound) }
        end
      end

      context 'waste_types' do
        context 'waste types duplicated' do
          let(:params) { { region_id: region.id, waste_types: %w(garbage garbage), coordinates: [[[45,45]]] } }
          it { expect{subject}.to raise_error(Calendar::WasteDisposalZone::Create::WrongWasteTypes).with_message('waste types are duplicated') }
        end

        context 'wrong waste types' do
          let(:params) { { region_id: region.id, waste_types: %w(garbage aa), coordinates: [[[45,45]]] } }
          it { expect{subject}.to raise_error(Calendar::WasteDisposalZone::Create::WrongWasteTypes).with_message("wrong waste types. Allowed: #{Calendar::WastePickup::WASTE_TYPES.to_sentence}") }
        end
      end
    end
  end
end
