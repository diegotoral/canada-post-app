require 'rails_helper'

RSpec.describe Calendar::PostalCodeCover::Destroy do
  let(:region) { create(:region) }
  let!(:postal_code_cover) { create(:postal_code_cover, region_config: region) }

  let(:command) { Calendar::PostalCodeCover::Destroy.new(params) }

  describe 'call' do
    let(:params) { postal_code_cover.id }

    context 'valid params' do
      it 'should create postal code cover' do
        expect{command.call}.to change{Calendar::PostalCodeCover.count}.by(-1)
      end
    end
  end
end
