require 'rails_helper'

RSpec.describe Calendar::PostalCodeCover::Update do
  let(:region) { create(:region) }
  let!(:postal_code_cover) { create(:postal_code_cover, region_config: region) }

  let(:command) { Calendar::PostalCodeCover::Update.new({ name: 'nn' }, postal_code_cover.id) }

  describe 'call' do

    context 'valid params' do
      it 'should store region config' do
        command.call
        expect(postal_code_cover.reload.name).to eq('nn')
      end
    end
  end
end
