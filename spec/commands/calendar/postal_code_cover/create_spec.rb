require 'rails_helper'

RSpec.describe Calendar::PostalCodeCover::Create do
  let(:region) { create(:region) }

  let(:command) { Calendar::PostalCodeCover::Create.new(params) }

  describe 'call' do
    let(:params) { attributes_for(:postal_code_cover).merge(region_config_id: region.id) }

    context 'valid params' do
      it 'should create postal code cover' do
        expect{command.call}.to change{Calendar::PostalCodeCover.count}.by(1)
      end

      it 'should store region config' do
        expect(command.call.region_config_id).to be(region.id)
      end

      it 'should store front' do
        expect(command.call.front.url).not_to be(nil)
      end

      it 'should store fr_front' do
        expect(command.call.fr_front.url).not_to be(nil)
      end
    end
  end
end
