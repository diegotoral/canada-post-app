require 'rails_helper'

RSpec.describe Calendar::AdvertiserEvent::Create do
  let(:advertiser) { create(:advertiser) }
  let(:valid_params) { {color: 'red', name: Faker::Company.name, start_date: DateTime.now, end_date: DateTime.now + 2.days, advertiser_id: advertiser.id } }

  describe 'call' do
    context 'create' do
      before(:each) do
        allow_any_instance_of(Calendar::AdvertiserEvent::Create).to receive(:update_region_status).and_return(nil)
      end

      it 'should create event with all fields' do
        advertiser = Calendar::AdvertiserEvent::Create.new(valid_params).call
        expect(advertiser.name).not_to eq(nil)
        expect(advertiser.start_date).not_to eq(nil)
        expect(advertiser.end_date).not_to eq(nil)
        expect(advertiser.advertiser).not_to eq(nil)
      end

      it 'should validate name' do
        expect{Calendar::AdvertiserEvent::Create.new(valid_params.reject{|key| key == :name}).call}.to raise_error(ActiveModel::ValidationError)
      end

      it 'should validate start_date' do
        expect{Calendar::AdvertiserEvent::Create.new(valid_params.reject{|key| key == :start_date}).call}.to raise_error(ActiveModel::ValidationError)
      end

      it 'should validate end_date' do
        expect{Calendar::AdvertiserEvent::Create.new(valid_params.reject{|key| key == :end_date}).call}.to raise_error(ActiveModel::ValidationError)
      end

      it 'should validate advertiser_id' do
        expect{Calendar::AdvertiserEvent::Create.new(valid_params.reject{|key| key == :advertiser_id}).call}.to raise_error(ActiveModel::ValidationError)
      end
    end

    context 'region config status' do
      let(:full_month_config) { create(:full_month_config, advertiser_id: advertiser.id) }

      before(:each) do
        allow_any_instance_of(Calendar::RegionConfigValidationStatusService).to receive(:call).and_raise('RegionConfigValidationStatus')
      end

      it 'shouldn\'t call Calendar::RegionConfigValidationStatusService service' do
        expect{Calendar::AdvertiserEvent::Create.new(valid_params).call}.not_to raise_error
      end

      it 'should call Calendar::RegionConfigValidationStatusService service' do
        full_month_config
        expect{Calendar::AdvertiserEvent::Create.new(valid_params).call}.to raise_error('RegionConfigValidationStatus')
      end
    end
  end
end
