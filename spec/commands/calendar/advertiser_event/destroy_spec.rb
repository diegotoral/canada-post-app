require 'rails_helper'

RSpec.describe Calendar::AdvertiserEvent::Destroy do
  let(:event) { create(:advertiser_event) }

  describe 'call' do
    context 'destroy' do
      before(:each) do
        allow_any_instance_of(Calendar::RegionConfigValidationStatusService).to receive(:call).and_raise('RegionConfigValidationStatus')
      end

      it 'should delete advertiser' do
        event
        expect{ Calendar::AdvertiserEvent::Destroy.new(event.id).call }.to change { Calendar::AdvertiserEvent.count }.by(-1)
      end
    end

    context 'region config status' do
      let(:full_month_config) { create(:full_month_config, advertiser_id: event.advertiser_id) }

      before(:each) do
        allow_any_instance_of(Calendar::RegionConfigValidationStatusService).to receive(:call).and_raise('RegionConfigValidationStatus')
      end

      it 'shouldn\'t call Calendar::RegionConfigValidationStatusService service' do
        expect{Calendar::AdvertiserEvent::Destroy.new(event.id).call}.not_to raise_error
      end

      it 'should call Calendar::RegionConfigValidationStatusService service' do
        full_month_config
        expect{Calendar::AdvertiserEvent::Destroy.new(event.id).call}.to raise_error('RegionConfigValidationStatus')
      end
    end
  end
end
