require 'rails_helper'

RSpec.describe Calendar::AdvertiserEvent::Update do
  let(:event) { create(:advertiser_event) }

  describe 'call' do
    context 'update' do
      before(:each) do
        allow_any_instance_of(Calendar::AdvertiserEvent::Create).to receive(:update_region_status).and_return(nil)
      end

      it 'should change name field' do
        old_field = event.name
        Calendar::AdvertiserEvent::Update.new(color: 'red', id: event.id, name: 'XYZ', start_date: DateTime.now - 2.year, end_date: DateTime.now - 1.year).call
        event.reload
        expect(event.name).not_to be(old_field)
      end

      it 'should change start_date field' do
        old_field = event.start_date
        Calendar::AdvertiserEvent::Update.new(color: 'red', id: event.id, name: 'XYZ', start_date: DateTime.now - 2.year, end_date: DateTime.now - 1.year).call
        event.reload
        expect(event.start_date).not_to be(old_field)
      end

      it 'should change end_date field' do
        old_field = event.end_date
        Calendar::AdvertiserEvent::Update.new(color: 'red', id: event.id, name: 'XYZ', start_date: DateTime.now - 2.year, end_date: DateTime.now - 1.year).call
        event.reload
        expect(event.end_date).not_to be(old_field)
      end
    end

    context 'region config status' do
      let(:full_month_config) { create(:full_month_config, advertiser_id: event.advertiser_id) }
      let(:valid_params) { { color: 'red', id: event.id, name: 'XYZ', start_date: DateTime.now - 2.year, end_date: DateTime.now - 1.year } }

      before(:each) do
        allow_any_instance_of(Calendar::RegionConfigValidationStatusService).to receive(:call).and_raise('RegionConfigValidationStatus')
      end

      it 'shouldn\'t call Calendar::RegionConfigValidationStatusService service' do
        expect{Calendar::AdvertiserEvent::Update.new(valid_params).call}.not_to raise_error
      end

      it 'should call Calendar::RegionConfigValidationStatusService service' do
        full_month_config
        expect{Calendar::AdvertiserEvent::Update.new(valid_params).call}.to raise_error('RegionConfigValidationStatus')
      end
    end
  end
end
