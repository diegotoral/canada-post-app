require 'rails_helper'

RSpec.describe Invitation::Create do
  let!(:admin) { create(:admin) }

  describe 'call' do
    context 'admin exists' do
      it 'should raise an error' do
        expect{ Invitation::Create.new(email: admin.email).call }.to raise_error(Invitation::Create::AdminAlreadyExists)
      end

      it 'should downcase email param do find if already exists' do
        expect{ Invitation::Create.new(email: admin.email.upcase).call }.to raise_error(Invitation::Create::AdminAlreadyExists)
      end
    end

    context 'admin does not exist' do
      it 'should send an email with invitation token and create an invitation object' do
        expect{ Invitation::Create.new(email: Faker::Internet.email).call }.to change{ ActionMailer::Base.deliveries.count }.by(1)
        expect(Invitation.count).to eq(1)
      end

      context 'validation' do
        it 'should verify email presence' do
          expect{ Invitation::Create.new(email: '').call }.to raise_error(ActiveModel::ValidationError)
        end
      end
    end
  end
end
