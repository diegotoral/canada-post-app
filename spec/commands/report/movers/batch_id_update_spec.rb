require 'rails_helper'

RSpec.describe Report::Movers::BatchIdUpdate do

  describe 'call' do
    context 'update' do
      subject { FactoryGirl.create(:report_movers) }

      it 'should add job ids to array' do
        Report::Movers::BatchIdUpdate.new(report: subject, batch_id: "231ac").call
        subject.reload
        expect(subject.batch_id).to eq("231ac")
      end
    end
  end
end
