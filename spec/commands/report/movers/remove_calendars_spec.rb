require 'rails_helper'

RSpec.describe Report::Movers::RemoveCalendars do
  let(:report_movers) { create(:report_movers) }
  let(:calendars) { create(:report_calendar, report_movers: report_movers) }

  describe 'call' do
    it 'shouldn\'t call Report::MoversCalendar::Destroy' do
      allow_any_instance_of(Report::MoversCalendar::Destroy).to receive(:call).and_raise('MoversCalendarDestroy')
      expect{Report::Movers::RemoveCalendars.new(report_movers).call}.not_to raise_error
    end

    it 'should call Report::MoversCalendar::Destroy' do
      calendars
      allow_any_instance_of(Report::MoversCalendar::Destroy).to receive(:call).and_raise('MoversCalendarDestroy')
      expect{Report::Movers::RemoveCalendars.new(report_movers).call}.to raise_error('MoversCalendarDestroy')
    end
  end
end
