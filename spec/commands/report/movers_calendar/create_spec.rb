require 'rails_helper'

RSpec.describe Report::MoversCalendar::Create do
  let(:report) { create(:report_movers) }
  let(:valid_params) { { report_id: report.id, path: 'path/to/calendar' } }

  describe 'call' do
    context 'create' do
      it 'should create report movers calendar with all fields' do
        calendar = Report::MoversCalendar::Create.new(valid_params).call
        expect(calendar.report_movers).to eq(report)
        expect(calendar.path).to eq(valid_params[:path])
      end
    end
  end
end
