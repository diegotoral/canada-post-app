require 'rails_helper'

RSpec.describe Report::MoversError::Create do
  let(:report) { create(:report_movers) }
  let(:valid_params) { { report_id: report.id, row: 'field', message: ['error'], type: :location_invalid } }

  describe 'call' do
    subject {Report::MoversError::Create.new(valid_params).call}

    it 'should assign fields correctly' do
      expect(subject.error_type).to eq('location_invalid')
      expect(subject.row).to eq('field')
      expect(subject.message).to eq('error')
    end
  end
end
