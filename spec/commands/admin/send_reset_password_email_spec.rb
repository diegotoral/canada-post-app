require 'rails_helper'

RSpec.describe Admin::SendResetPasswordEmail do
  let!(:admin) { create(:admin) }

  describe 'call' do
    context 'admin exists' do
      it 'should send an email' do
        expect{ Admin::SendResetPasswordEmail.new(admin.id).call }.to change{ ActionMailer::Base.deliveries.count }.by(1)
      end
    end

    context 'admin does not exist' do
      it 'should raise not found error' do
        expect{ Admin::SendResetPasswordEmail.new('non-existing-id').call }.to raise_error(ActiveRecord::RecordNotFound)
      end
    end
  end
end
