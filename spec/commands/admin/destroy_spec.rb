require 'rails_helper'

RSpec.describe Admin::Destroy do
  let!(:admin) { create(:admin) }

  describe 'call' do
    context ' admin exists' do
      it 'should delete a record' do
        expect{ Admin::Destroy.new(admin.id).call }.to change { Admin.count }.by(-1)
      end
    end

    context 'admin does not exist' do
      it 'should raise not found error' do
        expect{ Admin::Destroy.new('non-existing-id').call }.to raise_error(ActiveRecord::RecordNotFound)
      end
    end
  end
end
