require 'rails_helper'

RSpec.describe Admin::Create do
  let(:invitation) { create(:invitation) }

  let(:confirmed_invitation) { create(:invitation) }
  let!(:admin) { create(:admin, email: confirmed_invitation.email) }

  describe 'call' do
    context 'token is correct' do
      it 'should create a record' do
        expect{ Admin::Create.new(
          {
            token: invitation.token,
            admin: {
              password: 'password',
              password_confirmation: 'password'
            }
          })
          .call
        }.to change { Admin.count }.by(1)
      end

      context 'validation' do
        it 'should validate email uniqueness' do
          expect{ Admin::Create.new(
            {
              token: confirmed_invitation.token,
              admin: {
                password: 'password',
                password_confirmation: 'password'
              }
            })
            .call
          }.to raise_error(Admin::Create::AdminAlreadyExists)
        end
        it 'should validate password length' do
          expect{ Admin::Create.new(
            {
              token: invitation.token,
              admin: {
                password: 'short',
                password_confirmation: 'short'
              }
            })
            .call
          }.to raise_error(ActiveModel::ValidationError)
        end

        it 'should validate password presence' do
          expect{ Admin::Create.new(
            {
              token: invitation.token,
              admin: {
                password: '',
                password_confirmation: ''
              }
            })
            .call
          }.to raise_error(ActiveModel::ValidationError)
        end

        it 'should validate password match' do
          expect{ Admin::Create.new(
            {
              token: invitation.token,
              admin: {
                password: 'password',
                password_confirmation: 'different'
              }
            })
            .call
          }.to raise_error(ActiveModel::ValidationError)
        end
      end
    end

    context 'token is wrong' do
      it 'should raise not found error' do
        expect{ Admin::Create.new(
          {
            token: 'non-existing-token',
            admin: {
              password: 'password',
              password_confirmation: 'different'
            }
          })
          .call
        }.to raise_error(ActiveRecord::RecordNotFound)
      end
    end
  end
end
