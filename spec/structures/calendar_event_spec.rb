require 'rails_helper'

RSpec.describe CalendarEventStructure do
  let(:calendar_event) {FactoryGirl.build(:calendar_event)}

  describe 'call' do
    context 'validation' do
      context 'date format' do
        it 'should be numbers only' do
          calendar_event.date = "wrong_format"
          expect{ calendar_event.call }.to raise_error(WrongDateFormat)
        end

        it 'should have exactly 8 numbers' do
          calendar_event.date = 201612121
          expect{ calendar_event.call }.to raise_error(WrongDateFormat)
        end

        it 'should be YYYYMMDD (month check)' do
          calendar_event.date = 20161301
          expect{ calendar_event.call }.to raise_error(WrongDateFormat)
        end

        it 'should be YYYYMMDD (day check)' do
          calendar_event.date = 20161132
          expect{ calendar_event.call }.to raise_error(WrongDateFormat)
        end
      end
    end
  end

end
