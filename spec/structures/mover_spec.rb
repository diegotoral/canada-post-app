require 'rails_helper'

RSpec.describe MoverStructure do
  let!(:zone) { create(:waste_disposal_zone) }
  let(:mover) { FactoryGirl.build(:mover) }
  before { allow_any_instance_of(Calendar::Locations::GeocodeService).to receive(:call).and_return([43.751, -79.507]) }

  describe 'call' do
    context 'validation' do
      it 'shouldn\'t raise error - record valid' do
        expect{ mover.call }.not_to raise_error
      end

      it 'should validate record_id presence' do
        mover.record_id = ''
        expect{ mover.call }.to raise_error(ActiveModel::ValidationError)
      end

      it 'should validate start_date presence' do
        mover.start_date = ''
        expect{ mover.call }.to raise_error(WrongDateFormat)
      end

      it 'should validate language presence' do
        mover.language = ''
        expect{ mover.call }.to raise_error(ActiveModel::ValidationError)
      end

      it 'should validate last_name presence' do
        mover.last_name = ''
        expect{ mover.call }.to raise_error(ActiveModel::ValidationError)
      end

      it 'should validate address_line_1 presence' do
        mover.address_line_1 = ''
        expect{ mover.call }.to raise_error(ActiveModel::ValidationError)
      end

      it 'should validate city presence' do
        mover.city = ''
        expect{ mover.call }.to raise_error(ActiveModel::ValidationError)
      end

      it 'should validate province presence' do
        mover.province = ''
        expect{ mover.call }.to raise_error(ActiveModel::ValidationError)
      end

      it 'should validate postal_code presence' do
        mover.postal_code = ''
        expect{ mover.call }.to raise_error(ActiveModel::ValidationError)
      end

      it 'should validate multi_unit_building presence' do
        mover.multi_unit_building = ''
        expect{ mover.call }.to raise_error(ActiveModel::ValidationError)
      end

      context 'start_date format' do
        it 'should be numbers only' do
          mover.start_date = "wrong_format"
          expect{ mover.call }.to raise_error(WrongDateFormat)
        end

        it 'should have exactly 8 numbers' do
          mover.start_date = 201612121
          expect{ mover.call }.to raise_error(WrongDateFormat)
        end

        it 'should be YYYYMMDD (month check)' do
          mover.start_date = 20161301
          expect{ mover.call }.to raise_error(WrongDateFormat)
        end

        it 'should be YYYYMMDD (day check)' do
          mover.start_date = 20161132
          expect{ mover.call }.to raise_error(WrongDateFormat)
        end
      end

      context 'language format' do
        it 'shouldn\'t allow other values than E or F' do
          mover.language = "G"
          expect{ mover.call }.to raise_error(ActiveModel::ValidationError)
        end
      end

      context 'length' do
        before(:each) do
          @some_string = (0..50).map{|number| number}.join
        end

        it 'record_id max 10' do
          mover.record_id = 12345678901
          expect{ mover.call }.to raise_error(ActiveModel::ValidationError)
        end

        it 'first_name max 30' do
          mover.first_name = @some_string.first(31)
          expect{ mover.call }.to raise_error(ActiveModel::ValidationError)
        end

        it 'last_name max 40' do
          mover.last_name = @some_string.first(41)
          expect{ mover.call }.to raise_error(ActiveModel::ValidationError)
        end

        it 'address_line_1 max 40' do
          mover.address_line_1 = @some_string.first(41)
          expect{ mover.call }.to raise_error(ActiveModel::ValidationError)
        end

        it 'address_line_2 max 40' do
          mover.address_line_2 = @some_string.first(41)
          expect{ mover.call }.to raise_error(ActiveModel::ValidationError)
        end

        it 'city max 30' do
          mover.city = @some_string.first(31)
          expect{ mover.call }.to raise_error(ActiveModel::ValidationError)
        end

        it 'province max 2' do
          mover.province = @some_string.first(3)
          expect{ mover.call }.to raise_error(ActiveModel::ValidationError)
        end

        it 'multi_unit_building max 1' do
          mover.multi_unit_building = @some_string.first(2)
          expect{ mover.call }.to raise_error(ActiveModel::ValidationError)
        end
      end

      context 'postal_code formater' do
        it 'raise error not enough letters' do
          mover.postal_code = "X"
          expect{ mover.call }.to raise_error(ActiveModel::ValidationError)
        end

        it 'adds space' do
          mover.postal_code = "XXXXXX"
          expect{ mover.call }.not_to raise_error
          expect(mover.postal_code).to eq("XXX XXX")
        end

        it 'does nothing - valid' do
          mover.postal_code = "XXX XXX"
          expect{ mover.call }.not_to raise_error
          expect(mover.postal_code).to eq("XXX XXX")
        end
      end
    end
  end

  describe 'pre-assignments' do
    subject { mover.call }

    it { expect(mover.near).to eq([mover.city,mover.province].join(',')) }
    it { expect(mover.address).to eq([mover.address_line_1,mover.province,mover.postal_code].join(' ')) }
    it { expect(mover.anniversary_date).to eq(1.year.since.to_date) }
    it { expect(mover.french?).to eq(true) }
    it { expect{subject.tap{|mover| mover.geocode}}.to change{mover.location}.to([43.751, -79.507]) }
    it { expect{subject.tap{|mover| mover.geocode; mover.find_zones}}.to change{mover.zones}.to([zone]) }
  end
end
