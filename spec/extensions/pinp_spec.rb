require 'rails_helper'
require 'extensions/pinp'

RSpec.describe PINP do
  describe PINP::Edge do
    let(:edge) { PINP::Edge.new(start_point, end_point) }

    describe 'intersect?' do
      subject { edge.send(:intersect?, point) }

      context 'true' do
        let(:start_point) { PINP::Point.new(2,2) }
        let(:end_point)   { PINP::Point.new(1,1) }
        let(:point)       { PINP::Point.new(1.5,0) }

        it { is_expected.to eq(true) }
      end

      context 'false (x out of range)' do
        let(:start_point) { PINP::Point.new(2,2) }
        let(:end_point)   { PINP::Point.new(1,1) }
        let(:point)       { PINP::Point.new(4,0) }

        it { is_expected.to eq(false) }
      end

      context 'false (ray does not intersect with line)' do
        let(:start_point) { PINP::Point.new(2,2) }
        let(:end_point)   { PINP::Point.new(1,1) }
        let(:point)       { PINP::Point.new(0,4) }

        it { is_expected.to eq(false) }
      end

      context 'false (parallel ray)' do
        let(:start_point) { PINP::Point.new(0,2) }
        let(:end_point)   { PINP::Point.new(0,1) }
        let(:point)       { PINP::Point.new(0,4) }

        it { is_expected.to eq(false) }
      end
    end

    describe 'rise' do
      subject { edge.send(:rise) }
      let(:start_point) { PINP::Point.new(0, 3) }
      let(:end_point)   { PINP::Point.new(0, 5) }

      it { is_expected.to eq(2) }
    end

    describe 'run' do
      subject { edge.send(:run) }
      let(:start_point) { PINP::Point.new(3, 0) }
      let(:end_point)   { PINP::Point.new(5, 0) }

      it { is_expected.to eq(2) }
    end
  end

  describe PINP::Polygon do
    let(:polygon) { PINP::Polygon.new(points) }
    let(:points) { coordinates.map { |coords| PINP::Point.new(*coords) } }

    describe 'edges' do
      subject { polygon.edges }
      let(:coordinates) { [[0,0],[0,1],[1,1],[1,0]] }

      it { is_expected.to eq([
        PINP::Edge.new(PINP::Point.new(0,0), PINP::Point.new(0,1)),
        PINP::Edge.new(PINP::Point.new(0,1), PINP::Point.new(1,1)),
        PINP::Edge.new(PINP::Point.new(1,1), PINP::Point.new(1,0)),
        PINP::Edge.new(PINP::Point.new(1,0), PINP::Point.new(0,0))
      ]) }
    end

    describe 'include?' do
      let(:point1) { PINP::Point.new(2,2) }
      let(:point2) { PINP::Point.new(6,2) }
      let(:point3) { PINP::Point.new(6,6) }
      let(:point4) { PINP::Point.new(2,6) }
      let(:point5) { PINP::Point.new(4.5,4.5) }

      context 'rectangle' do
        context 'all points included' do
          let(:coordinates) { [[1,1],[7,1],[7,7],[1,7]] }

          it { expect(polygon.include?(point1)).to eq(true) }
          it { expect(polygon.include?(point2)).to eq(true) }
          it { expect(polygon.include?(point3)).to eq(true) }
          it { expect(polygon.include?(point4)).to eq(true) }
        end

        context 'some points on the left vertical edge' do
          let(:coordinates) { [[2,1],[7,1],[7,7],[2,7]] }

          it { expect(polygon.include?(point2)).to eq(true) }
          it { expect(polygon.include?(point3)).to eq(true) }
        end

        context 'some points on the right vertical edge' do
          let(:coordinates) { [[1,1],[6,1],[6,7],[1,7]] }

          it { expect(polygon.include?(point1)).to eq(true) }
          it { expect(polygon.include?(point4)).to eq(true) }
        end

        context 'some points on the bottom horizontal edge' do
          let(:coordinates) { [[1,2],[7,2],[7,7],[1,7]] }

          it { expect(polygon.include?(point3)).to eq(true) }
          it { expect(polygon.include?(point4)).to eq(true) }
        end

        context 'some points on the top horizontal edge' do
          let(:coordinates) { [[1,1],[7,1],[7,6],[1,6]] }

          it { expect(polygon.include?(point1)).to eq(true) }
          it { expect(polygon.include?(point2)).to eq(true) }
        end

        context 'some points included (left side)' do
          let(:coordinates) { [[1,1],[5,1],[5,7],[1,7]] }

          it { expect(polygon.include?(point1)).to eq(true) }
          it { expect(polygon.include?(point2)).to eq(false) }
          it { expect(polygon.include?(point3)).to eq(false) }
          it { expect(polygon.include?(point4)).to eq(true) }
        end

        context 'some points included (right side)' do
          let(:coordinates) { [[3,1],[7,1],[7,7],[3,7]] }

          it { expect(polygon.include?(point1)).to eq(false) }
          it { expect(polygon.include?(point2)).to eq(true) }
          it { expect(polygon.include?(point3)).to eq(true) }
          it { expect(polygon.include?(point4)).to eq(false) }
        end

        context 'some points included (top side)' do
          let(:coordinates) { [[1,3],[7,3],[7,7],[1,7]] }

          it { expect(polygon.include?(point1)).to eq(false) }
          it { expect(polygon.include?(point2)).to eq(false) }
          it { expect(polygon.include?(point3)).to eq(true) }
          it { expect(polygon.include?(point4)).to eq(true) }
        end

        context 'some points included (bottom side)' do
          let(:coordinates) { [[1,1],[7,1],[7,5],[1,5]] }

          it { expect(polygon.include?(point1)).to eq(true) }
          it { expect(polygon.include?(point2)).to eq(true) }
          it { expect(polygon.include?(point3)).to eq(false) }
          it { expect(polygon.include?(point4)).to eq(false) }
        end
      end

      context 'triangle' do
        context 'some points included' do
          let(:coordinates) { [[-2,2],[7,7],[2,-2]] }

          it { expect(polygon.include?(point1)).to eq(true) }
          it { expect(polygon.include?(point2)).to eq(false) }
          it { expect(polygon.include?(point3)).to eq(true) }
          it { expect(polygon.include?(point4)).to eq(false) }
        end

        context 'some points included' do
          let(:coordinates) { [[-1,7],[7,7],[7,-1]] }

          it { expect(polygon.include?(point1)).to eq(false) }
          it { expect(polygon.include?(point2)).to eq(true) }
          it { expect(polygon.include?(point3)).to eq(true) }
          it { expect(polygon.include?(point4)).to eq(true) }
        end

        context 'some points included' do
          let(:coordinates) { [[0,8],[6,0],[8,2]] }

          it { expect(polygon.include?(point1)).to eq(false) }
          it { expect(polygon.include?(point2)).to eq(true) }
          it { expect(polygon.include?(point3)).to eq(false) }
          it { expect(polygon.include?(point4)).to eq(true) }
        end
      end

      context 'irregular polygon' do
        context 'some points included' do
          let(:coordinates) { [[0,7],[8,7],[4,4.5],[8,3],[6,0],[0,4]] }

          it { expect(polygon.include?(point1)).to eq(false) }
          it { expect(polygon.include?(point2)).to eq(true) }
          it { expect(polygon.include?(point3)).to eq(true) }
          it { expect(polygon.include?(point4)).to eq(true) }
          it { expect(polygon.include?(point5)).to eq(false) }
        end
      end
    end
  end
end
