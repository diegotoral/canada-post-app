FactoryGirl.define do
  factory :advertiser_event, class: Calendar::AdvertiserEvent do
    name Faker::Company.name
    start_date DateTime.now
    end_date DateTime.now + 1.day
    color 'red'
    advertiser_id { FactoryGirl.create(:advertiser_no_images).id }
  end

  factory :full_advertiser_event, class: Calendar::AdvertiserEvent do
    color 'red'
    name Faker::Company.name
    start_date DateTime.now
    end_date DateTime.now + 1.day
    advertiser_id { FactoryGirl.create(:advertiser).id }
  end
end
