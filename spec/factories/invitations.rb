FactoryGirl.define do
  factory :invitation do
    email { Faker::Internet.email }
    token { Devise.friendly_token(40) }
  end
end
