FactoryGirl.define do
  factory :report_calendar, class: Report::MoversCalendar do
    path 'some/path'
  end
end
