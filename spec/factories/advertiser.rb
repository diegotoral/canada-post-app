image = Rack::Test::UploadedFile.new(Rails.root.join('spec', 'support', 'images', 'image.png'), "image/png")

FactoryGirl.define do
  factory :advertiser, class: Calendar::Advertiser do
    name Faker::Company.name
    cover_advert image
    right_advert image
    left_advert_1 image
    left_advert_2 image
    left_advert_3 image
    grid 0
  end

  factory :advertiser_no_images, class: Calendar::Advertiser do
    name Faker::Company.name
    grid 1
  end
end
