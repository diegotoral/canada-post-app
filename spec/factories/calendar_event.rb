FactoryGirl.define do
  factory :calendar_event, class: CalendarEventStructure do
    name 'Name'
    type 'waste'
    date DateTime.now
  end
end
