FactoryGirl.define do
  factory :report_movers, class: Report::Movers do
    file_name 'file.txt'
    parsed_count 1
  end
end
