image = Rack::Test::UploadedFile.new(Rails.root.join('spec', 'support', 'images', 'image.png'), "image/png")

FactoryGirl.define do
  factory :postal_code_cover, class: Calendar::PostalCodeCover do
    name 'ChinaTown'
    front image
    fr_front image
    codes ['MMM']
  end
end
