FactoryGirl.define do
  factory :mover, class: MoverStructure do
    record_id "123"
    start_date DateTime.now.strftime('%Y%m%d')
    language "F"
    first_name { Faker::Name.first_name }
    last_name { Faker::Name.last_name }
    address_line_1 { Faker::Address.street_name }
    address_line_2 { Faker::Address.street_name }
    city { Faker::Address.city }
    province { Faker::Address.country_code }
    postal_code "TST456"
    multi_unit_building "N"
  end

  factory :mover_with_coordinates, class: MoverStructure do
    record_id "123"
    start_date DateTime.now.strftime('%Y%m%d')
    language "F"
    first_name { Faker::Name.first_name }
    last_name { Faker::Name.last_name }
    address_line_1 { Faker::Address.street_name }
    address_line_2 { Faker::Address.street_name }
    city { Faker::Address.city }
    province { Faker::Address.country_code }
    postal_code "TST456"
    multi_unit_building "N"
    lat 40
    long 40
  end
end
