image = Rack::Test::UploadedFile.new(Rails.root.join('spec', 'support', 'images', 'image.png'), "image/png")

FactoryGirl.define do
  factory :month_config, class: Calendar::Month do
    month_number 1
    region_config {create(:region_no_images)}
  end

  factory :full_month_config, class: Calendar::Month do
    month_number 1
    region_config {create(:region)}
  end
end
