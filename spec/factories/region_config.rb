image = Rack::Test::UploadedFile.new(Rails.root.join('spec', 'support', 'images', 'image.png'), "image/png")

FactoryGirl.define do
  factory :region, class: Calendar::RegionConfig do
    region 'Toronto'
    front image
    signature image
    front_left image
    last_page image
    anniversary image
    neighbourhood image
    stamp image
    codes ['TST']
  end

  factory :region_no_images, class: Calendar::RegionConfig do
    region 'Toronto'
    codes ['TST']
  end
end
