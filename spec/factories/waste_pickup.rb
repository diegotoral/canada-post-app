FactoryGirl.define do
  factory :waste_pickup, class: Calendar::WastePickup do
    waste_disposal_zone { create(:waste_disposal_zone) }
    date DateTime.now
    green_bin true
  end
end
