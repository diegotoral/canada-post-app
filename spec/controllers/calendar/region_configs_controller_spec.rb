require 'rails_helper'

RSpec.describe Calendar::RegionConfigsController, type: :controller do
  let(:admin) { create(:admin) }
  let(:region) { create(:region_no_images) }

  describe 'GET #index' do
    it 'should redirect not logged in admin' do
      expect(get :index).to redirect_to(new_admin_session_path)
    end

    it 'should allow only logged in admin' do
      sign_in(admin)
      expect(get :index).to have_http_status(200)
    end
  end

  describe 'GET #edit' do
    it 'should redirect not logged in admin' do
      expect(get :edit, params: { id: region.id } ).to redirect_to(new_admin_session_path)
    end

    it 'should allow only logged in admin' do
      sign_in(admin)
      expect(get :edit, params: { id: region.id } ).to have_http_status(200)
    end
  end

  describe 'PUT #update' do
    it 'should redirect not logged in admin' do
      expect(put :update, params: { id: region.id, calendar_region_config: {} }).to redirect_to(new_admin_session_path)
    end

    it 'should allow only logged in admin' do
      sign_in(admin)
      expect(put :update, params: { id: region.id, calendar_region_config: { front: nil } }).to redirect_to(calendar_region_configs_path)
    end
  end
end
