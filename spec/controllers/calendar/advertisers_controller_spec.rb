require 'rails_helper'

RSpec.describe Calendar::AdvertisersController, type: :controller do
  let(:admin) { create(:admin) }
  let(:advertiser) { create(:advertiser_no_images) }

  describe 'GET #index' do
    it 'should redirect not logged in admin' do
      expect(get :index).to redirect_to(new_admin_session_path)
    end

    it 'should allow only logged in admin' do
      sign_in(admin)
      expect(get :index).to have_http_status(200)
    end
  end

  describe 'GET #edit' do
    it 'should redirect not logged in admin' do
      expect(get :edit, params: { id: advertiser.id } ).to redirect_to(new_admin_session_path)
    end

    it 'should allow only logged in admin' do
      sign_in(admin)
      expect(get :edit, params: { id: advertiser.id } ).to have_http_status(200)
    end
  end

  describe 'PUT #update' do
    it 'should redirect not logged in admin' do
      expect(put :update, params: { id: advertiser.id, calendar_advertiser: {} }).to redirect_to(new_admin_session_path)
    end

    it 'should allow only logged in admin' do
      sign_in(admin)
      expect(put :update, params: { id: advertiser.id, calendar_advertiser: { name: nil } }).to redirect_to(calendar_advertisers_path)
    end
  end

  describe 'PUT #destroy' do
    it 'should redirect not logged in admin' do
      expect(put :destroy, params: { id: advertiser.id }).to redirect_to(new_admin_session_path)
    end

    it 'should allow only logged in admin' do
      sign_in(admin)
      expect(put :destroy, params: { id: advertiser.id }).to redirect_to(calendar_advertisers_path)
    end

    it 'should redirect due to wrong id' do
      sign_in(admin)
      expect(put :destroy, params: { id: 0 }).to have_http_status(302)
    end
  end
end
