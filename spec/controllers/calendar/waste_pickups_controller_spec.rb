require 'rails_helper'

RSpec.describe Calendar::WastePickupsController, type: :controller do
  let(:admin) { create(:admin) }
  let(:zone) { create(:waste_disposal_zone) }
  let(:region) { zone.region_config }
  let(:waste_day_schedule) { { 'garbage' => '1', 'recycling' => '0', 'yard_waste' => '0', 'green_bin' => '0', 'christmas_tree' => '0' } }
  let(:waste_week_schedule) { { '0' => waste_day_schedule, '1' => waste_day_schedule, '2' => waste_day_schedule, '3' => waste_day_schedule, '4' => waste_day_schedule, '5' => waste_day_schedule, '6' => waste_day_schedule } }
  let(:options) { { 'date_from' => '01-01-2017', 'date_to' => '31-01-2017',
                    'waste_pickups_week_1' => waste_week_schedule,
                    'waste_pickups_week_2' => waste_week_schedule }.symbolize_keys }
  let(:proper_params) { { region_config_id: region.id, waste_disposal_zone_id: zone.id, calendar_waste_disposal_zone: { year: '2017', month: '1', waste_pickups: { '1' => { 'garbage' => '0', 'recycling' => '1' } } } } }
  let(:proper_pattern_params) { { region_config_id: region.id, waste_disposal_zone_id: zone.id, calendar_waste_disposal_zone: options } }

  describe 'GET #index' do
    it 'should redirect not logged in admin' do
      expect(get :index, params: { region_config_id: region.id, waste_disposal_zone_id: zone.id }).to redirect_to(new_admin_session_path)
    end

    it 'should allow only logged in admin' do
      sign_in(admin)
      expect(get :index, params: { region_config_id: region.id, waste_disposal_zone_id: zone.id }).to have_http_status(200)
    end
  end

  describe 'POST #save' do
    it 'should redirect not logged in admin' do
      expect(post :save, params: proper_params ).to redirect_to(new_admin_session_path)
    end

    it 'should allow only logged in admin' do
      sign_in(admin)
      expect(post :save, params: proper_params ).to have_http_status(302)
    end
  end

  describe 'POST #save_week_pattern' do
    it 'should redirect not logged in admin' do
      expect(post :save_week_pattern, params: proper_pattern_params ).to redirect_to(new_admin_session_path)
    end

    it 'should allow only logged in admin' do
      sign_in(admin)
      expect(post :save_week_pattern, params: proper_pattern_params ).to have_http_status(302)
    end
  end
end
