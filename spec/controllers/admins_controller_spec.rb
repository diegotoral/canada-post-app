require 'rails_helper'

RSpec.describe AdminsController, type: :controller do
  describe 'GET #admin_list' do
    let(:admin) { create(:admin) }

    it 'should redirect not logged in admin' do
      expect(get :admin_list).to redirect_to(new_admin_session_path)
    end

    it 'should allow only logged in admin' do
      sign_in(admin)
      expect(get :admin_list).to have_http_status(200)
    end
  end

  describe 'DELETE #remove_admin' do
    let(:admin) { create(:admin) }

    before :each do
      sign_in(admin)
    end

    context 'admin exists' do
      it 'should have redirect to admin list' do
        expect(delete :remove_admin, params: { id: admin.id }).to redirect_to(admin_list_path)
      end
    end

    context 'admin does not exist' do
      it 'should redirect to admin path' do
        expect(delete :remove_admin, params: { id: 'non-existing-id' }).to redirect_to(admin_list_path)
        expect(flash[:danger]).to match(/Admin not found */)
      end
    end
  end

  describe 'POST #reset_password' do
    let(:admin) { create(:admin) }

    before :each do
      sign_in(admin)
    end

    context 'admin exists' do
      it 'should have redirect to admin list' do
        expect(delete :reset_password, params: { id: admin.id }).to redirect_to(admin_list_path)
      end
    end

    context 'admin does not exist' do
      it 'should redirect to admin path with 404 status' do
        expect(delete :reset_password, params: { id: 'non-existing-id' }).to redirect_to(admin_list_path)
        expect(flash[:danger]).to match(/Admin not found */)
      end
    end
  end

  describe 'GET #new_admin' do

    context 'wrong token' do
      it 'should redirect if the token if wrong' do
        expect(get :new_admin, params: { token: 'wrong-token' }).to redirect_to(new_admin_session_path)
        expect(flash[:danger]).to match(/Token invalid */)
      end
    end

    context 'correct token' do
      let(:invitation) { create(:invitation) }

      it 'should not redirect if the token is right' do
        expect(get :new_admin, params: { token: invitation.token }).to have_http_status(200)
      end

    end
  end

  describe 'POST #create_admin' do
    let(:invitation) { create(:invitation) }
    let(:valid_params) { { token: invitation.token, admin: { password: 'password', password_confirmation: 'password' } } }
    let(:invalid_password) { { token: invitation.token, admin: { password: '', password_confirmation: 'password' } } }
    let(:invalid_token) { { token: 'wrong-token', admin: { password: 'password', password_confirmation: 'password' } } }

    context 'valid parameters' do
      it 'should redirect to login form' do
        expect(post :create_admin, params: valid_params ).to redirect_to(new_admin_session_path)
      end
    end

    context 'invalid parameters' do
      it 'should redirect to form back again' do
        expect(post :create_admin, params: invalid_password).to redirect_to(new_admin_path(token: invitation.token))
      end

      it 'should redirect to form back again if invitation not found' do
        expect(post :create_admin, params: invalid_token).to redirect_to(new_admin_session_path)
      end
    end
  end
end
