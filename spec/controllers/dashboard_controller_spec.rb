require 'rails_helper'

RSpec.describe DashboardController, type: :controller do
  describe 'GET #home' do
    let(:admin) { create(:admin) }

    it 'should redirect not logged in admin' do
      expect(get :home).to redirect_to(new_admin_session_path)
    end

    it 'should allow only logged in admin' do
      sign_in(admin)
      expect(get :home).to have_http_status(200)
    end
  end
end
