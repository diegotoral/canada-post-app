require 'rails_helper'

RSpec.describe InvitationsController, type: :controller do
  describe 'GET #invite_admin' do
    let(:admin) { create(:admin) }

    it 'should redirect not logged in admin' do
      expect(get :invite_admin).to redirect_to(new_admin_session_path)
    end

    it 'should allow only logged in admin' do
      sign_in(admin)
      expect(get :invite_admin).to have_http_status(200)
    end
  end

  describe 'POST #send_admin_invitation' do
    let(:admin) { create(:admin) }

    it 'should redirect not logged in admin' do
      expect(post :send_admin_invitation, params: { admin: { email: Faker::Internet.email } }).to redirect_to(new_admin_session_path)
    end

    context 'while admin is logged in' do

      before :each do
        sign_in(admin)
      end

      it 'should redirect to admin list and send an email' do
        expect{ post :send_admin_invitation, params: { admin: { email: Faker::Internet.email } } }.to change{ ActionMailer::Base.deliveries.count }.by(1)
      end

      it 'should rescue an error when admin already exists' do
        expect(post :send_admin_invitation, params: { admin: { email: admin.email } } ).to redirect_to(invite_admin_path)
        expect(flash[:danger]).to match(/Admin already exists */)
      end

      it 'should rescue an error when email is empty' do
        expect(post :send_admin_invitation, params: { admin: { email: '' } } ).to redirect_to(invite_admin_path)
        expect(flash[:danger]).to match(/Email can't be blank */)
      end
    end
  end
end
