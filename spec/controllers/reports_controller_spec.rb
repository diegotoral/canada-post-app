require 'rails_helper'

RSpec.describe ReportsController, type: :controller do
  let(:admin) { create(:admin) }
  let(:report) { create(:movers_report) }

  describe 'GET #movers_reports' do
    it 'should redirect not logged in admin' do
      expect(get :movers_reports).to redirect_to(new_admin_session_path)
    end

    it 'should allow only logged in admin' do
      sign_in(admin)
      expect(get :movers_reports).to have_http_status(200)
    end
  end
end
