require 'rails_helper'

RSpec.describe CalendarGenerateWorker do
  let(:report) { create(:report_movers) }
  let(:mover) { build(:mover_with_coordinates) }
  let(:admin) { create(:admin) }
  let(:zone) { create(:waste_disposal_zone) }

  describe 'call' do
    before(:each) do
      allow_any_instance_of(PdfGenerator::GenerateService).to receive(:call).and_return(nil)
      allow_any_instance_of(PdfGenerator::GenerateService).to receive(:region).and_return(nil)
      allow_any_instance_of(PdfGenerator::GenerateService).to receive(:file_path).and_return('path')
      allow_any_instance_of(PdfGenerator::HeadlessChromeService).to receive(:call).and_return(nil)
      allow_any_instance_of(PdfGenerator::HeadlessChromeService).to receive(:region).and_return(nil)
      allow_any_instance_of(PdfGenerator::HeadlessChromeService).to receive(:file_path).and_return('path')
      allow_any_instance_of(CalendarGenerateWorker).to receive(:batch).and_return(double(bid: '123abc'))
      allow_any_instance_of(Calendar::Locations::GeocodeService).to receive(:call).and_return([-79.507,43.751])
      allow(Report::Movers).to receive(:find).and_return(report)
    end

    context 'whole flow' do
      it 'should change status to in_progress' do
        allow(Sidekiq).to receive(:redis).and_return(true)
        report.update(parsed_count: 2)
        CalendarGenerateWorker.new.perform(mover.to_hash, report.id)
        expect(report.status).to eq('pending')
      end

      it 'should update paths' do
        allow(Sidekiq).to receive(:redis).and_return(false)
        CalendarGenerateWorker.new.perform(mover.to_hash, report.id)
        expect(report.calendars.count).to eq(1)
      end
    end
  end
end
