const CDP = require('chrome-remote-interface');
const fs = require('fs');
const url_path = process.argv[2]
const output_file_path = process.argv[3]
const chrome_port = parseInt(process.argv[4])

async function load() {
  try{
    const tab = await CDP.New({port: chrome_port});
    const client = await CDP({tab});
    const {Network, Page, Runtime} = client;
    await Promise.all([Network.enable(), Page.enable()]);
    return new Promise((resolve, reject) => {
      Page.navigate({url: url_path});
      Page.loadEventFired(() => {
        Runtime.evaluate({
          expression: `
            new Promise((fulfill, reject) => {
              function check_status(counter) {
                if ($('.leaflet-tile').length == $('.leaflet-tile-loaded').length) {
                  fulfill(true)
                } else {
                  if(counter >= 10){
                    reject(false)
                  } else {
                    window.setTimeout(check_status, 1000, counter + 1);
                  }
                }
              }
              check_status(0);
            })`,
          awaitPromise: true
        }).then(response => {
          if(response.result.type == 'boolean' && response.result.value == true){
            setTimeout(resolve, 2000, {client: client, tab: tab})
          } else {
            return CDP.Close({port: chrome_port, id: tab.id}).then(() => {
              return process.exit(1);
            });
          }
        });
      });
    });
  } catch(e) {
    await CDP.Close({port: chrome_port, id: tab.id});
    process.exit(1)
  }
}

async function getPdf() {
  const {client, tab} = await load();
  const {Page} = client;
  try{
    pdf = await promiseTimeout(60000, Page).catch(e => {
      throw("TimedOut");
    });
    await CDP.Close({port: chrome_port, id: tab.id});
    return pdf;
  } catch(e) {
    await CDP.Close({port: chrome_port, id: tab.id});
    process.exit(2)
  }
}

function promiseTimeout(timeout, Page){
  return Promise.race([
    Page.printToPDF({pageRanges: '1,3,5,7,9,11,13,15,17,19,21,23,25,27,29,31', scale: 1.3335349259, paperWidth: 52.091208043, paperHeight: 77.094987904, marginTop: 0, marginLeft: 0, marginBottm: 0, marginRight: 0, printBackground: true}),
    new Promise(function(resolve, reject) {
      setTimeout(function() {
        reject('Timed out');
      }, timeout);
    })
  ]);
}

function createDirPath(whole_path){
  var splitted_path = whole_path.split("/")
  var dir_path = splitted_path[1] + "/" + splitted_path[2]
  checkDirectory("./" + splitted_path[1])
  checkDirectory("./" + dir_path)
}

function checkDirectory(directory) {
  fs.mkdir(directory, function(e){})
}

getPdf().then(response => {
  createDirPath(output_file_path)
  buffer = new Buffer(response.data, 'base64');
  fs.writeFileSync('.' + output_file_path, buffer, 'base64', function(err) {
    if (err) {
      process.exit(1)
      console.log(err);
    }
  });
  process.exit(0);
});
