# README Canada Post

## Running the server locally
Because we use sidekiq it is easy to start sidekiq along with the server itself by typing
```shell
foreman start
```

## Ruby version
* Ruby: 2.3.1
* Rails: 5.0.0

## Gems
To install gems you need to get bundler.
```shell
gem install bundler
```
After this you can use bundler to install rest of gems by running command:
```shell
bundle install
```

If you do not have rails installed run:
```shell
gem install rails
```
## Database
To create database, firstly copy database configuration file:
```shell
cp config/database.yml.example config/database.yml
```
You can edit that file to change default settings. Then create database:
```shell
rake db:create
```
Run migrations to create tables in DB:
```shell
rake db:migrate
```

## Node
Project is using also nodeJS. Go into `/headless-chrome` and run
```shell
npm install
```
Make sure that you have `node >= 7.6.0`.

## Others
Install chrome browser in version >= 63.0.
To run chrome browser in headless mode run:
```shell
chrome-canary --headless --remote-debugging-port=9222 --disable-gpu &
```
More information:
https://developers.google.com/web/updates/2017/04/headless-chrome

## Default data
If you want some default data to use, run seed command:
```shell
rake db:seed
```
It will create admin account:
* email: **admin@admin.com**
* password: **lolopolo**

Now you can access your app through webbrowser by connecting to URL
```shell
localhost:5000
```
## Tests
To run tests simply run command:
```shell
rspec
```
