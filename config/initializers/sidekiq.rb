CALENDAR_GENERATION_LOGGER ||= Logger.new("#{Rails.root}/log/calendar_generation.log")

Sidekiq.configure_server do |config|
  config.redis = { url: 'redis://127.0.0.1:6379/0', namespace: "#{Rails.application.secrets.sidekiq_namespace}_#{ Rails.env }" }
end
Sidekiq.configure_client do |config|
  config.redis = { url: 'redis://127.0.0.1:6379/0', namespace: "#{Rails.application.secrets.sidekiq_namespace}_#{ Rails.env }" }
end
