BASE_URL = Rails.application.secrets.base_url
PHANTOMJS_BASE_URL = Rails.application.secrets.phantomjs_base_url
MAPBOX_TOKEN = Rails.application.secrets.mapbox_token
MAPBOX_STYLE_URL = Rails.application.secrets.mapbox_style_url
GOOGLE_TOKEN = Rails.application.secrets.google_token
CHROME_PORT = Rails.application.secrets.chrome_port

ADVERT_DISTANCE = 5000
Date::DATE_FORMATS.merge!({default: '%d/%m/%Y'})
