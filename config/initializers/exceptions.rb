module Parser
  class HeadersException < StandardError; end
  class NoValidRecords < StandardError; end
  class InvalidFileFormatException < StandardError; end
  class EmptyCsvException < StandardError; end
  class WrongFormatedCsvException < StandardError; end
  class InvalidByteSequenceException < StandardError; end
end

module Calendar
  class FileNotFound < StandardError; end
  class Advertiser::Destroy::AdvertiserIsConnectedToMonth < StandardError; end
  class Location::WrongCategory < StandardError; end
end

class WrongDateFormat < StandardError; end
