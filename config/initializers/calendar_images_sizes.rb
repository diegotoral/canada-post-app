CALENDAR_IMAGE_SIZE = {
  front: '3383x2577',
  signature: '490x230',
  front_left: '1205x2577',
  last_page: '3383x2577',
  cover_advert: '3383x2577',
  left_advert_grid: '1450x480',
  right_advert: '1450x480',
  anniversary: '3383x2577',
  stamp: '',
  neighbourhood: '3383x1260',
  tiny_address_logo: '480x200',
  logo: '960x200'
}
