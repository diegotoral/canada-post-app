lock '3.6.1'

set :repo_url,     'git@gitlab.binarapps.com:wojtekhoch/canada-post.git'
#set :stages,       %w(staging)

set :linked_files, %w(config/database.yml config/unicorn.rb config/sidekiq.yml)
set :linked_dirs,  %w(headless-chrome/node_modules log vendor/bundle tmp/sockets tmp/pids tmp/cache public/uploads public/pdfs calendars)

set :keep_releases, 3

# set :rvm_type, :system
set :rvm_map_bins, %w(rake gem bundle ruby rails sidekiq sidekiqctl)
set :whenever_identifier, ->{ "#{fetch(:application)}_#{fetch(:stage)}" }
