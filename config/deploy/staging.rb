role :app,        %w(staging01.binarapps.com)
role :web,        %w(staging01.binarapps.com)
role :db,         %w(staging01.binarapps.com), primary: true
set :application, 'canada-post-staging.binarapps.com'

server 'staging01.binarapps.com', user: fetch(:application), roles: %w(web app db), primary: true

set :full_app_name, 'canada-post-staging.binarapps.com'
set :rails_env,   'production'
set :branch,      'staging'
set :deploy_to,   "/home/#{fetch(:full_app_name)}/www/"

set :rvm_ruby_version, '2.3.1@canada-post-staging'

namespace :deploy do
  desc 'Restart application'
  task :stop do
    on roles(:app), in: :sequence, wait: 10 do
      execute 'systemctl --user stop unicorn.service'
      execute 'systemctl --user stop sidekiq.service'
    end
  end

  task :start do
    on roles(:app), in: :sequence, wait: 10 do
      execute 'systemctl --user start unicorn.service'
      execute 'systemctl --user start sidekiq.service'
    end
  end

  after :publishing, :stop
  after :stop, :start

  after :restart, :clear_cache do
    on roles(:web), in: :groups, limit: 3, wait: 10 do
    end
  end
end
