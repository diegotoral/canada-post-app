role :app,        %w(cpcolourinnovations.com)
role :web,        %w(cpcolourinnovations.com)
role :db,         %w(cpcolourinnovations.com), primary: true
set :application, 'canada-post-app'

server 'cpcolourinnovations.com', user: 'canada-post-app', port: 8022, roles: %w(web app db), primary: true

set :full_app_name, 'canada-post-app'
set :rails_env,   'production'
set :branch,      'staging'
set :deploy_to,   "/home/#{fetch(:full_app_name)}/www/"
set :linked_files, %w(config/database.yml config/unicorn.rb config/sidekiq.yml config/secrets.yml)

set :rvm_ruby_version, '2.3.1@canada-post'

namespace :deploy do
  desc 'Restart application'
  task :stop do
    on roles(:app), in: :sequence, wait: 10 do
      execute 'sudo systemctl stop canada-post-app.service'
      execute 'sudo systemctl stop canada-post-app-sidekiq.service'
    end
  end

  task :start do
    on roles(:app), in: :sequence, wait: 10 do
      execute 'sudo systemctl start canada-post-app.service'
      execute 'sudo systemctl start canada-post-app-sidekiq.service'
    end
  end

  after :publishing, :stop
  after :stop, :start

  after :restart, :clear_cache do
    on roles(:web), in: :groups, limit: 3, wait: 10 do
    end
  end
end

