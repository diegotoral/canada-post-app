role :app,        %w(72.51.8.122)
role :web,        %w(72.51.8.122)
role :db,         %w(72.51.8.122), primary: true
set :application, 'canada-post-app'

server '72.51.8.122', user: fetch(:application), roles: %w(web app db), primary: true

set :full_app_name, 'canada-post-app'
set :rails_env,   'production'
set :branch,      'master'
set :deploy_to,   "/home/#{fetch(:full_app_name)}/www/"
set :linked_files, %w(config/database.yml config/unicorn.rb config/sidekiq.yml config/secrets.yml)

set :rvm_ruby_version, '2.3.1@canada-post'

namespace :deploy do
  desc 'Restart application'
  task :stop do
    on roles(:app), in: :sequence, wait: 10 do
      execute 'sudo unicornctl stop'
    end
  end

  task :start do
    on roles(:app), in: :sequence, wait: 10 do
      execute 'sudo unicornctl start'
    end
  end

  after :publishing, :stop
  after :stop, :start

  after :restart, :clear_cache do
    on roles(:web), in: :groups, limit: 3, wait: 10 do
    end
  end
end

