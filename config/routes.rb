require 'sidekiq/web'

Rails.application.routes.draw do
  root to: 'dashboard#home'
  authenticate :admin do
    mount Sidekiq::Web => '/sidekiq'
  end

  devise_for :admins

  get '/home', to: 'dashboard#home'

  # admins_controller
  get '/admin_list', to: 'admins#admin_list'
  get '/new_admin', to: 'admins#new_admin'
  post '/create_admin', to: 'admins#create_admin', as: 'create_admin'
  delete '/remove_admin/:id', to: 'admins#remove_admin', as: 'remove_admin'
  post '/reset_password/:id', to: 'admins#reset_password', as: 'reset_password'

  # invitations_controller
  get '/invite_admin', to: 'invitations#invite_admin'
  post '/send_admin_invitation', to: 'invitations#send_admin_invitation', as: 'send_admin_invitation'

  # movers_controller
  resources :movers, only: [:new]
  post 'upload_movers', to: 'movers#upload_file', as: 'upload_movers'

  # reports controller
  get 'movers_reports', to: 'reports#movers_reports', as: 'movers_reports'
  get '/csv_errors/:id', to: 'reports#csv_errors', as: 'csv_errors'
  get '/phantom_calendar/:id', to: 'reports#phantom_calendar', as: 'phantom_calendar'
  post 'cancel_movers_generator/:id', to: 'reports#cancel_movers_generator', as: 'cancel_movers_generator'
  get 'reports/:id/calendars', to: 'reports#calendars_index', as: 'calendars_index'
  get 'reports/:id/zip_calendars', to: 'reports#zip_calendars', as: 'zip_calendars'

  get '/calendars/*filepath' => 'calendars#show', as: 'calendar_show'
  namespace :calendar do
    # advertisers controller
    resources :advertisers do
      resources :advertiser_events
    end

    resources :category_filters, only: [:index, :update]

    resources :postal_code_covers, only: [:new, :create, :edit, :update, :destroy]

    # region configs controller
    resources :region_configs do
      collection do
        post :upload_pois
        post :upload_zones
      end

      resources :waste_disposal_zones, only: [:index] do
        resources :waste_pickups, only: [:index] do
          collection do
            post :save
            post :save_week_pattern
          end
        end
      end
    end
    post '/refresh_calendar/:id', to: 'region_configs#refresh_calendar', as: 'refresh_calendar'

    # month controller
    resources :months, only: [:edit, :update]
  end

end
